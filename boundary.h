#ifndef BOUNDARY_H
#define BOUNDARY_H

#include <QObject>
#include <QJsonObject>
#include <QFile>
#include <QTextStream>

#include "plant_stand.h"
#include "soilnode.h"
#include "soilprofile.h"
#include "wsnode.h"

/**********************************//**
*@brief The Boundary class implements all interactions with external
* conditions, e.g. weather & management.
*
*All interactions with external components, e.g. weather and management are treated by the
* Boundary class. Weather is read in from an external text file and redistributed to the relevant components
* (plant stand & soil profile).
*
* ***********************************/
class Boundary : public QObject
{
    Q_OBJECT

public:
    explicit Boundary(QObject *parent = nullptr);
    Boundary(QObject *parent, const QJsonObject &theInit);
    void configure(const QJsonObject &theInit);
    double albedo(void);
    double lt_tempVal;

public slots:
    void doTimeStep(double step);
    void setTranspi(double transpiro);
signals:
    void precipitation(double);
    void etp(double);
    void temperature(double);
    void par(double);
    void eva(double);
    void photo(double);
private:

    QFile weatherFile;
    QTextStream inLines;
    double rainVal,evaVal, tempVal, transpi, wdate,photoperiod,radiation, prec_per;
    int st_number;
};

#include "bodiumrunner.h"
#endif // BOUNDARY_H
