#ifndef BODIUMRUNNER_H
#define BODIUMRUNNER_H

#include <QObject>

#include "boundary.h"
#include "soilnode.h"
#include "soilprofile.h"
#include "plant_stand.h"
#include "timex.h"
#ifdef WITH_GUI
    #include "bodiumwindow.h"
#endif
#ifdef MAKE_LIB
    #define SHARED_LIB_EXPORT Q_DECL_EXPORT
#else
    #define SHARED_LIB_EXPORT Q_DECL_IMPORT
#endif

#ifdef MAKE_LIB
class SHARED_LIB_EXPORT bodiumrunner: public QObject
#else
class bodiumrunner: public QObject
#endif
{
    Q_OBJECT
public:
    bodiumrunner();
    void run(void);
    void run_py(void);
    void configAll(void);
    Boundary theBoundary;
    Plant_stand thePlantStand;
    SoilProfile theProfile;
    timeX myTimer;
    QList<QJsonObject> manageList;
    bool var_orgfert=true;
    bool jsoned=false;
    double node_no=1;
    bool landtrans=false;
    QString config_name=NULL;
#ifdef WITH_GUI
    BodiumWindow BodWindow;
#endif


signals:
    void finished(int);

private slots:
    void setJsonEd(void);

private:
    QString path_out, r_path, var_json, minus;
    bool fill_complist=true;
    bool manage=false;
    QList<QString> compListR;

};

#endif // BODIUMRUNNER_H
