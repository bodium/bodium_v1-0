#include "bodiumrunner.h"
#ifdef WITH_GUI
#include "ui_bodiumwindow.h"
#endif
#include <stdio.h>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QTextStream>
#include <QList>
#include <iostream>
#include <cstdio>
#include <QProcess>
#include <qdir.h>
#include <QCommandLineParser>
extern QCommandLineParser optionParser;

bodiumrunner::bodiumrunner() :
     theBoundary(this), thePlantStand(this),
    theProfile(this), myTimer(this)
{
    connect(&theBoundary,SIGNAL(etp(double)), &thePlantStand, SLOT(setETP(double)));
    connect(&theBoundary,SIGNAL(temperature(double)), &thePlantStand, SLOT(setTemp(double)));
    connect(&thePlantStand,SIGNAL(transpi(double)), &theBoundary, SLOT(setTranspi(double)));
    connect(&theBoundary,SIGNAL(eva(double)), &theProfile, SLOT(setEva(double)));
    connect(&theBoundary,SIGNAL(precipitation(double)), &theProfile, SLOT(setRain(double)));
    connect(&theBoundary,SIGNAL(par(double)), &thePlantStand, SLOT(setPAR(double)));
    connect(&theBoundary,SIGNAL(photo(double)), &thePlantStand, SLOT(setPhotoperiod(double)));
    connect(&theBoundary,SIGNAL(temperature(double)), &theProfile, SLOT(setTemp(double)));


    connect(&myTimer, SIGNAL(timeStepped(double)), &theBoundary, SLOT(doTimeStep(double)));

    connect(&myTimer, SIGNAL(timeStepped(double)), &theProfile, SLOT(doTimeStep(double)));

    connect(&myTimer, SIGNAL(timeStepped(double)), &thePlantStand, SLOT(doTimeStep(double)));
    connect(&myTimer, SIGNAL(finished(int)), this, SIGNAL(finished(int)));

    connect(&thePlantStand, SIGNAL(rtl_fac(void)), &theProfile, SLOT(rootLengthFactor(void)));

    connect(&thePlantStand, SIGNAL(rootdemand(double)), &theProfile, SLOT(rootRequest(double)));
    connect(&thePlantStand, SIGNAL(seed(double)), &theProfile, SLOT(doSeed(double)));
    connect(&theProfile, SIGNAL(demandN(double)), &thePlantStand, SLOT(updateN(double)));

    connect(&myTimer, SIGNAL(write(void)), &theProfile, SLOT(doOutput(void)));

    connect(&thePlantStand, SIGNAL(setfirstroot(double)), &theProfile, SLOT(doSeed(double)));

    connect(&thePlantStand, SIGNAL(ferti(double,double, double)), &theProfile, SLOT(fertilize(double,double,double)));
    connect(&thePlantStand, SIGNAL(manure(double,double)), &theProfile, SLOT(organic_fert(double,double)));
    connect(&thePlantStand, SIGNAL(straw(double,double,double)), &theProfile, SLOT(straw_fert(double,double,double)));

    qDebug("Pre-config");

    configAll();

    qDebug("Post-config");      //qDebug statt printf, weil letzteres in die Outputdatei schreibt

#ifdef WITH_GUI
    connect(BodWindow.paramwindow,SIGNAL(saveJsonEd()),this,SLOT(setJsonEd()));
    connect(BodWindow.ui->runPushButton, SIGNAL(clicked(bool)), &myTimer, SLOT(doTimeSteps(void)));
    connect(&myTimer, SIGNAL(finished(int)), BodWindow.ui->finishBar, SLOT(setValue(int)));
    BodWindow.initCompList(compListR,node_no+1);
    BodWindow.show();
#endif

    double bvb=9;

}

void bodiumrunner::run(void)
{
    myTimer.doTimeSteps();

}

void bodiumrunner::run_py(void){
    myTimer.step_py();
}

void bodiumrunner::configAll(void){
    SoilNode* actNode, *prevNode = nullptr;
    Component* actComponent;


    //so sollte das als relativer Pfad funktionieren, solange der Projektordner auf der gleichen Ebene ist wie der Build Ordner
    QDir tmpCurrDir = QDir::current();
    QString jsonname;
    jsonname = tmpCurrDir.path()+"/config.json";
    const QStringList args =optionParser.positionalArguments();
    if(args.length()>0)
        jsonname = args.at(0);

    if(landtrans & (config_name.size()>0)) jsonname=tmpCurrDir.path()+config_name;

    QFile loadFile(jsonname);

    if (!loadFile.open(QIODevice::ReadOnly)) {
            qWarning("Couldn't open config file.");

        }

    QByteArray saveData = loadFile.readAll();
    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    myTimer.init(loadDoc.object()["timeX"].toObject());
    if(loadDoc.object().contains("Output")){
        path_out = loadDoc.object()["Output"].toString()+"_"+QString::number(1);
        freopen(qPrintable(path_out+".csv"),"w",stdout);
    }

    r_path=loadDoc.object()["R_path"].toString();
    manage=loadDoc.object()["UseManFile"].toBool();

    if(manage){
        QString manage_path=loadDoc.object()["ManFile"].toString();

        QFile loadmanFile(manage_path);

        if (!loadmanFile.open(QIODevice::ReadOnly)) {
                qWarning("Couldn't open management file.");

            }

        QByteArray manData = loadmanFile.readAll();
        QJsonDocument loadmanDoc(QJsonDocument::fromJson(manData));
        QJsonArray manInit=loadmanDoc.object()["Events"].toArray();

        manageList.clear();
        foreach (const QJsonValue & value, manInit) {
            manageList.append(value.toObject());
            }
    }

    theBoundary.configure(loadDoc.object());

    thePlantStand.configure(loadDoc.object());

    theProfile.setNodes(loadDoc.object());
    connect(&thePlantStand, SIGNAL(till_event(double)), &theProfile, SLOT(tillage(double)));
    std::cout<<"dat_time\t Photosynthese\t Plant_biomass\t Plant_biomass_SO\t Plant_nitrogen\t Plant_resp\t Root_depth\t Dev_stage\t Nitro_roots\tWaterstress\tTranspiration\tLeachate\tNout";

    QString component_header, sum_header;

    foreach(actNode, theProfile.theNodes){
        Process* actProcess;
        connect(&myTimer, SIGNAL(timeStepped(double)), actNode, SLOT(doTimeStep(double)));
        connect(&myTimer, SIGNAL(update(void)), actNode, SLOT(doUpdate(void)));
        connect(&thePlantStand, SIGNAL(rootharv(double,double)), actNode->getComponent("FOM"), SLOT(root_harv(double,double)));
        connect(&thePlantStand, SIGNAL(rootharv(double,double)), actNode->getComponent("Root"), SLOT(removeVol(double,double)));

        if(prevNode!= nullptr){
            connect(prevNode->getComponent("Root"), SIGNAL(seed(double)), actNode->getComponent("Root"), SLOT(getSeed(double)));
            connect(prevNode->getComponent("Root"), SIGNAL(rts_dp(bool)), actNode, SLOT(setRTSdepth(bool)));}
        prevNode = actNode;
        foreach(actProcess, actNode->theProcesses){
            actProcess->configure(loadDoc.object());
        }

        foreach(actComponent, actNode->theComponents){
            actComponent->configure(loadDoc.object());
            if(fill_complist){
            component_header.append("\t"+actComponent->getName()+"_val\t"+actComponent->getName()+"_vol\t"+actComponent->getName()+"_mass");
            sum_header.append("\t"+actComponent->getName()+"_sumass");
            compListR.append(actComponent->getName());
            }
        }
        fill_complist=false;

    }

    //new method to sort output based on user defined depth intervals
        QList<double> borders, intervals,depths;
        QJsonObject depthInit = loadDoc.object()["Out_depth"].toObject();
        QJsonArray jsonArray = QJsonArray(depthInit["borders"].toArray());

        foreach (const QJsonValue & value, jsonArray) {
            borders.append(value["value"].toDouble());
            }
        jsonArray = QJsonArray(depthInit["intervals"].toArray());

        foreach (const QJsonValue & value, jsonArray) {
            intervals.append(value["value"].toDouble());
            }

        if(intervals.isEmpty() && borders.isEmpty()){depths={0.1,0.2,0.3,0.4,0.6,0.8,1.2,1.5};}
        else{
            double border;
            if(intervals.isEmpty()){
                foreach(border,borders){
                    depths.append(border/100);
                }
            }
            else{
                if(borders.isEmpty()){borders={0,150};}
                int border_count=0,interval_count=0;
                double cur_depth,next_border;

                cur_depth=borders.at(border_count);
                next_border=borders.at(border_count+1);

                while(cur_depth<next_border){
                    cur_depth+=intervals.at(border_count);
                    depths.append(cur_depth/100);
                    if((cur_depth+intervals.at(border_count))>=next_border){
                        border_count++;
                        next_border=(border_count==(borders.size()-1))?0:borders.at(border_count+1);
                        interval_count=(border_count>(intervals.size()-1))?interval_count:border_count;
                    }
                }

            }
        }

        theProfile.setOutdepths(depths);
        theProfile.setSumOut(depthInit["sum"].toDouble()/100);
        node_no=depths.length();

        std::cout<<sum_header.toLatin1().data();
        std::cout<<"\ttotal_sumass\tAWC\tfilter";
        double depth;
        foreach(depth, depths){
            std::cout<<"\tout_depth";
            std::cout<<component_header.toLatin1().data();
            std::cout<<"\ttotal_mass\ttotal_vol\tnode_depth\tloading\ttemp\ttopval\tnitrate_out\tCO2_em\tNAOM_cn\tAOM_cn\tSSOM_cn\twater_reduc\tFB_ratio";

        }

        //to write also LandTrans node out, independent of user wishes
        std::cout<<"\tout_depth";
        std::cout<<component_header.toLatin1().data();
        std::cout<<"\ttotal_mass\ttotal_vol\tnode_depth\tloading\ttemp\ttopval\tnitrate_out\tCO2_em\tNAOM_cn\tAOM_cn\tSSOM_cn\twater_reduc\tFB_ratio";


    theProfile.init();

}

void bodiumrunner::setJsonEd(){
    jsoned=true;
    configAll();
}
