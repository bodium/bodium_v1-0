#include "waterProfile.h"
extern double minTimeStep;
waterProfile::waterProfile(QObject *parent) : QObject(parent)
{

}

void waterProfile::init(const QJsonObject &theInit)
{

    int sub;
    QJsonArray jsonArray = QJsonArray(theInit["wsnodes"].toArray());
    
    foreach (const QJsonValue & value, jsonArray) {
        if(!value["SubNodes"].isUndefined())
            sub = value["SubNodes"].toInt();
        else
            sub = 1;
        double thick = value["Thickness"].toDouble()/sub;
        for(int i = 0;i<sub;i++){
            theNodes.append(new wsnode(value.toObject(), this));
            //theNodes.last()->setNodeThick(thick);

        }


    }
    WaterCell* theOutlet = new WaterCell(theNodes.last());
    theOutlet->init(1.7E208, 1e-3,1,0,0,0);
    theOutlet->setName("Outlet");
    theOutlet->setBoundary(true);
    theNodes.last()->cells.append(theOutlet);

    wsnode *actNode, *oldNode=NULL;
    bool isFirst = true;
    foreach(actNode, theNodes){
        actNode->addInternalCond(theConducts);
        if(isFirst)
            isFirst = false;
        else
            actNode->addexternalCond(oldNode, theConducts);
        oldNode = actNode;
    }
    boundary = new wsnode(this);
    boundary->initBoundary();

    rainy = new WaterCell(boundary);
    evapy = new WaterCell(boundary);
    boundary->cells.append(rainy);
    boundary->cells.append(evapy);
    rainy->init(1000, 1e-3, 1, 100);
    evapy->init(0.0, 1e-12, 1, -1e7);
    rainy->setName("Rain");
    evapy->setName("Evi");
    actNode = theNodes.first();


    conductor *newCond =new conductor(actNode);
    newCond->source = actNode->matrixCell;
    newCond->target = evapy;
    newCond->setPotTarget(evapy->psi_d());
    newCond->setDistSource(actNode->nodeThick()/2);
    newCond->setDistTarget(0.1);
    newCond->setCondTarget(1e-12);
    newCond->setCondSource(actNode->conduct());
    newCond->setTempCondTarget(10);
    connect(boundary, SIGNAL(tempChanged(double)), newCond, SLOT(setTempTarget(double)));
    connect(actNode, SIGNAL(tempChanged(double)), newCond, SLOT(setTempSource(double)));
    connect(actNode, SIGNAL(tempConductChanged(double)),newCond, SLOT(setTempCondSource(double)));
    connect(this,SIGNAL(stepTime(double)),newCond, SLOT(conductEnergy(double)));


    theConducts.append(newCond);
    connect(actNode->matrixCell,SIGNAL(psiChanged(double)),newCond,SLOT(setPotSource(double)));
    connect(actNode, SIGNAL(conductChanged(double)), newCond, SLOT(setCondSource(double)));
    connect(evapy, SIGNAL(horConductivityChanged(double)), newCond,SLOT(setCondTarget(double)));

    actNode->addSourceCond(newCond);
    actNode->setTopout(newCond);
    newCond =new conductor(actNode);
    newCond->target = actNode->matrixCell;
    newCond->source = rainy;
    newCond->setPotSource(rainy->psi_s());
    newCond->setDistSource(0.001);
    newCond->setDistTarget(actNode->nodeThick()/2);
    newCond->setCondSource(1e-2);
    newCond->setCondTarget(actNode->conduct());
    newCond->setGradientZ(9.81e3); // Pa/m
    newCond->setTempCondSource(10);
    theConducts.append(newCond);
    actNode->addTargetCond(newCond);
    actNode->setTopin(newCond);

    connect(actNode->matrixCell,SIGNAL(psiChanged(double)),newCond,SLOT(setPotTarget(double)));
    connect(actNode, SIGNAL(conductChanged(double)), newCond, SLOT(setCondTarget(double)));
    connect(rainy, SIGNAL(horConductivityChanged(double)), newCond,SLOT(setCondSource(double)));
    connect(boundary, SIGNAL(tempChanged(double)), newCond, SLOT(setTempSource(double)));
    connect(actNode, SIGNAL(tempChanged(double)), newCond, SLOT(setTempTarget(double)));
    connect(actNode, SIGNAL(tempConductChanged(double)),newCond, SLOT(setTempCondTarget(double)));
    connect(this,SIGNAL(stepTime(double)),newCond, SLOT(conductEnergy(double)));







    //connect(rainy, SIGNAL(goneEmpty(bool)), evapy, SLOT(emitNotFull(bool)));
    for(QList<conductor*>::iterator actCond = theConducts.begin(); actCond != theConducts.end(); actCond++){

        (*actCond)->checkEnds();



    }
    foreach(actNode, theNodes){
        connect(actNode, SIGNAL(psiChanged(double)),this, SLOT(matrixChanges(double)));
        actNode->conduct();
        //actNode->evalDTheta();
    }
}

void waterProfile::resetChangeRates()
{
    wsnode* actNode;
    WaterCell* theCell;
    conductor* theCond;
    foreach(actNode, theNodes){
        foreach(theCell, actNode->cells){
           theCell->setZeroChange();
        }
        actNode->matrixCell->setZeroChange();
    }
    evapy->setZeroChange();
    rainy->setZeroChange();
    //foreach(theCond, theConducts){
      //  if(theCond->isItActive()&&!theCond->source->empty()&&!theCond->target->full()){
        //    theCond->source->addToChange(-theCond->conduct());
          //  theCond->target->addToChange(theCond->conduct());


        //}
       // theCond->unSwitch();
       // theCond->checkEnds();
    //}

}

void waterProfile::doTimestep(double timestep)
{
    double maxTime;
    wsnode* actNode;
    WaterCell* theCell;
    //conductor* theCond;
    //foreach(theCond, theConducts){
    //    theCond->checkSwitch();
    //}
    while((maxTime = timestep)){
        printf("\r%lf", maxTime);
        fflush(stdout);
        do{
            matrixChanged = false;
            QList<wsnode*>::iterator iter;
            for(iter = theNodes.begin();iter != theNodes.end();iter++){
                (*iter)->findZero();
            }
            QList<wsnode*>::reverse_iterator riter;
            for(riter = theNodes.rbegin();riter != theNodes.rend();riter++){
                (*riter)->findZero();
            }
        }while(matrixChanged);

        foreach(actNode, theNodes){
            foreach(theCell, actNode->cells){
                double actMax = theCell->linearTime();
                maxTime = actMax<maxTime?actMax:maxTime;
                if(maxTime <= minTimeStep)
                    break;
            }
            actNode->tempConduct();
        }
        double actMax = evapy->linearTime();
        maxTime = actMax<maxTime?actMax:maxTime;
        actMax = rainy->linearTime();
        maxTime = actMax<maxTime?actMax:maxTime;
        foreach(actNode, theNodes){
            foreach(theCell, actNode->cells){
                theCell->doTimeStep(maxTime);
            }
        }
        evapy->doTimeStep(maxTime);
        rainy->doTimeStep(maxTime);
        emit stepTime(maxTime);
        //foreach(theCond, theConducts){
        //    theCond->checkSwitch();
        //}

        timestep -= maxTime;
    }
}

QString waterProfile::getVals()
{
    wsnode* actNode;
    double massBal;
    QString ret(""), loc;
    loc = "%1: %2, %3, %4, %5, %6\n";
    massBal =0;
    foreach(actNode, theNodes){

        ret.append(loc.arg(actNode->name()).arg(actNode->theta()).arg(actNode->psi()).arg(actNode->conduct()).arg(actNode->topval()).arg(actNode->temperature()));
        massBal += actNode->theta()* actNode->nodeThick();
    }
    ret.append(loc.arg("Boundary").arg(rainy->theta()).arg(evapy->capacity()));
    ret.append(loc.arg("BoundaryResidual").arg(residualRain).arg(residualEva));
    ret.append(loc.arg("Mass balance").arg(massBal*1000));
    return ret;
}

double waterProfile::getChangeSum()
{
    wsnode* actNode;
    WaterCell* theCell;
    double retval = 0;
    foreach(actNode, theNodes){
        foreach(theCell, actNode->cells){
            retval += theCell->deltaTheta();
        }
    }
    return retval;
}

void waterProfile::setEvaRain(double rain, double evapo, double ttemp)
{
    residualRain = rainy->theta();
    residualEva = evapy->capacity();
    rainy->setTheta(rain);
    evapy->setCapacity(evapo);
    evapy->setTheta(0);
    boundary->setTemperature(ttemp);
    //evapy->emitNotFull(false);
    //rainy->emitNotEmpty(true);
}

void waterProfile::StructChange(const QJsonObject &theInit)
{
    wsnode* actNode;
    foreach(actNode, theNodes){
        if(actNode->name() == theInit["Horizon"].toString()){
            actNode->addMacro(theInit["Macro+"].toDouble());
        }
    }

}
