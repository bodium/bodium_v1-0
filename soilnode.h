#ifndef SOILNODE_H
#define SOILNODE_H
#include <QObject>
#include <QtCore/QDateTime>
#include <iostream>
#include "watercell.h"
#include <math.h>
#define ROOTDENS 800
#define DAY 86400
#define lambda_mrf1 1200 //matrix density at full growth
#define lambda_mrf2 1800 //matrix density at zero growth
#define lambda_seedday 75 // date of annual seeding
#define lambda_harvest 210 // date of annual harvest
#define lambda_osom .005/DAY
#define lambda_somo1 0.05
#define lambda_somo2 .1/DAY
#define lambda_op 1/DAY
#define lambda_pop .5/DAY
#define ETA .55 //synthesis coefficient for FOM, value between .5 and .6, depends on crop, derived from candy demo database
#define k_FOM .56/DAY //rate coefficient for FOM turnover, depends on source. at present we have root as FOM input
#define k_S .0009/DAY //rate coefficient for C-Flux from AOM to SSOM, derived from candy demo database (k_stab)
#define k_A .00032/DAY //rate coefficient for C-Flux from SSOM to AOM, derived from candy demo database (k_akt)
#define k_M .00556/DAY //rate coefficient for C mineralization AOM, derived from candy demo database (k_aos?)
#define hkap .16 //heat capacity of dry soil [J cm-3 K-1], derived from candy demo database
#define amplitude 3.9 //amplitude [C], derived from candy demo database
#define pha 240 //phase shift of sinus/cosinus phase [d], derived from candy demo database
#define ltem 10 //mean annual air temperature [C], derived from candy demo database

/**
 * @brief Base class for all components
 *
 * Includes different functions all components have in common.
 *
 */
class Component : public QObject
{
    Q_OBJECT
public:
    explicit Component(QObject *parent = nullptr);
    virtual QString getName(void){return "Generic component object";}
    virtual QString getUnit(void){return "[-]";}
    virtual double getValue(void){return theValue;}
    virtual double getVolume(void){return 0;}
    virtual double getMass(void){return theMass;}
    virtual void setMass(double mass){theMass=mass;}
    virtual bool getTillMix(void){return till_mix;}

    virtual void setValue(double newValue){theValue = newValue; theUpdateValue = 0;}
    /**
     * @brief merge merge this with other component.
     * @param other similar component to be merged with
     *
     * If two nodes are merged each component has to be merged. The basic definition just adds their value.
     */
    virtual void merge(Component other);
    /**
     * @brief split create similar component and split this into two
     * @param splitter proportion to be split off
     * @return new component to use in new node
     */
    virtual Component *split(double splitter);
    bool till_mix=false;


signals:
    double valueChanged(double value);

public slots:
    /**
     * @brief Updates value of the component and writes current value to output file
     */
    virtual void doUpdate(void){
        theValue+=theUpdateValue;
        theUpdateValue = 0;
        //std::cout<<"\t"<<getValue()<<"\t"<<getVolume()<<"\t"<<getMass();
    }
    /**
     * @brief Adds specific value to the component value
     * @param incr value to be added
     */
    virtual void addUpdate(double incr){theUpdateValue+=incr;}
    virtual void dropUpdate(void){theUpdateValue = 0;}
    /**
     * @brief Configuration of component parameters
     * @param theConfig Config file
     */
    virtual void configure(const QJsonObject &theConfig){}




protected:
    double theValue=0, theUpdateValue=0, theMass=0;
};

/**
 * @brief Base class for all processes
 *
 */
class Process : public QObject
{
    Q_OBJECT
public:
    explicit Process(QObject *parent = 0);
    QList<Component*> Components;
    virtual QString getName(void){return "Generic component object";}

public slots:
    /**
     * @brief Basic function where process is ongoing
     */
    virtual void evaluate(double timeStep){}
    /**
     * @brief Configuration of component parameters
     * @param theConfig Config file
     */
    virtual void configure(const QJsonObject &theConfig){}
    //void init_params(void){}


};
/**
 * @brief Description of each soil node
 */
class SoilNode : public QObject
{
    Q_OBJECT
public:
    explicit SoilNode(QObject *parent = nullptr, SoilNode *topnode =nullptr);
    SoilNode(const QJsonObject &theConfig, QObject *parent = nullptr, SoilNode *topnode =nullptr);

    QList<Component*> theComponents;
    QList<Process*> theProcesses;
    double getMass(void);
    double getVolume(void);
    Component* getComponent(QString theName);
    Process* getProcess(QString theName);
    void setFactorRTL(double newFactor){factorRTL=newFactor;}
    double factorRTL=0;
    bool rts_depth=0;
    double depth=0;
    double thick=0;
    double loading=0;
    double water_up=0;
    double BAT=0;
    void setDepth(double newDepth){depth=newDepth;}
    void setLoad(double newLoad){loading=newLoad;}
    void setWaterup(double newWaterup);
    double getDepth(void){return depth;}
    double getLoad(void){return loading;}
    double getWaterup(void){return water_up;}
    double setCO2(double new_co2){return co2_em=new_co2;}
    double getCO2(void){return co2_em;}
    float getNitrateOut(void){return nitrate_out;}
    double nitrate_in=0;
    double nitrate_out=0;
    double co2_em=0;
    double microbes_waterreduc(double psi_m);
    double cn_total=10;
    double active_ratio=0.2;
    void setTopNode(SoilNode *nodee){topNode=nodee;}
    SoilNode *getTopNode(void){return topNode;}
    /**
     * @brief split splits up node in two.
     * @param splitter amount to be split off.
     * @return new node to be inserted below existing one
     */
    //SoilNode split(double splitter);
    /**
     * @brief merge merge two nodes into one
     * @param other Node to be merged with this.
     */
    void merge(SoilNode other){}

signals:
    void seed(double);
    void waterup_changed(double);

public slots:
    void doTimeStep(double timeStep);
    void doUpdate(void);
    void setRTSdepth(bool rts_dp);//for marking that the node is the deepest with roots for calculating rooting depth
    void tilleffect(void);
private:
    SoilNode *topNode;

};

class SoilProfile;

/**
 * @brief matrix of each soil node
 *
 * includes volume and mass of SoilNode and thus defines bulk density
 */

class Matrix : public Component
{
    Q_OBJECT
public:
    explicit Matrix(QObject *parent = nullptr):Component(parent){
        theMass = updateMass = theVolume = updateVolume = 0;
    }
    QString getName(void){return "Matrix";}
    virtual double getVolume(void){return theVolume;}
    virtual double getMass(void){return theMass;}

public slots:
    void setVolume(double newVolume){theVolume = newVolume;}
    void setMass(double newMass){theMass = newMass; updateMass = 0;}
    void addVolume(double newVolume){updateVolume += newVolume;}
    void addMass(double newMass){updateMass += newMass;}
    void doUpdate(void);


private:
    double theMass, updateMass, theVolume, updateVolume;


};

/**
 * @brief Macropores affected by Root and BioX
 */

class Macropore : public Component
{
    Q_OBJECT
public:
    explicit Macropore(QObject *parent = 0):Component(parent){
        theVolume = updateVolume = 0;
    }

    virtual double getVolume(void){return theVolume;}

    QString getName(void){return "Macropore";}
public slots:
    void setVolume(double newVolume){theVolume = newVolume;}
    void addVolume(double newVolume){updateVolume += newVolume;}
    void doUpdate(void);
private:
    double theVolume, updateVolume;


};

/**
 * @brief Root system affected by Macropore and Plant_stand
 *
 * Is preferentially growing into macropores and at some point exchanging nutrients (exudates out, nitrogen in)
 * Grotwh is described in RootGrowth
 */

class Root : public Component
{
    Q_OBJECT

public:
    explicit Root(QObject *parent = 0):Component(parent){

    }

    QString getName(void){return "Root";}
    double getMass(void);
    virtual double getVolume(void){return theVolume;}
    void setVolume(double newVolume){theVolume=newVolume;updateVolume=0;}
    void upVolume(double newVolume){theVolume += newVolume;updateVolume=0;}
    void addVolume(double newVolume){updateVolume += newVolume;}

signals:
    void seed(double);
    void rts_dp(bool);

public slots:
    void doUpdate(void);
    void getSeed(double);
    virtual void configure(const QJsonObject &theConfig);
    void removeVol(double,double);

private:
    bool doSeed = true;
    double theVolume, updateVolume;
};

/**
 * @brief Representation of faunal organisms (currently earthworms)
 */

class BioX : public Component
{
    Q_OBJECT
public:
    explicit BioX(QObject *parent = 0):Component(parent){
        setValue(0.01);
    }

    QString getName(void){return "BioX";}
public slots:
    void doUpdate(void);
    virtual void configure(const QJsonObject &theConfig);
    void disturbance(double NO3_fert,double NH4_fert);

private:
    double dist_rate=0.5;
    bool dist_fert=false;

};
/**
 * @brief Fresh organic matter pool with specific C/N Ratio
 *
 * Depends on input source e.g FOM from litter fall, FOM from dead roots etc.
 */

class FomPool : public Component
{
    Q_OBJECT
public:
    explicit FomPool(QObject *parent = 0):Component(parent){}
    FomPool(const QJsonObject &theConfig, QObject *parent = 0);

   QString getName(void){return pool;}
   double getCNratio(void){return cn_ratio;}
   double getEta(void){return eta;}
   double getRate(void){return k_fom;}
   void setCNratio(double newcn,double newini){cn_ratio=((getCNratio()*getValue())+(newcn*newini))/(getValue()+newini);}
   double cn_ratio=12;

public slots:
    void doUpdate(void);
    virtual void addUpdate(double incr){theUpdateValue+=incr;}

private:
    double eta, k_fom, ini;
    QString pool;

};

/**
 * @brief Total fresh organic matter (sum of FOM pools)
 */
class Fom : public Component
{
    Q_OBJECT
public:
    explicit Fom(QObject *parent = 0):Component(parent){}
    QList<FomPool*> theFomPools;
    QString getName(void){return "FOM";}
public slots:
    void doUpdate(void);
    virtual void configure(const QJsonObject &theConfig);
    void root_harv(double,double);
};

/**
 * @brief Active organic matter (AOM) including microorganisms
 */

class Aom : public Component
{
    Q_OBJECT
public:
    explicit Aom(QObject *parent = 0):Component(parent){}

    QString getName(void){return "AOM";}
    double getCNratio(void){return cn_ratio;}
    void setCNratio(double newcn,double newini){cn_ratio_temp=((cn_ratio_temp*value_temp)+(newcn*newini))/(value_temp+newini); value_temp+=newini;}
    double getValuetemp(void){return theUpdateValue;}
    void setCNratio_fin(){cn_ratio=cn_ratio_temp;}

public slots:
    void doUpdate(void);
    virtual void configure(const QJsonObject &theConfig);

private:
    double cn_ratio=10,value_temp=0, cn_ratio_temp=10;

};

class NAom : public Component
{
    Q_OBJECT
public:
    explicit NAom(QObject *parent = 0):Component(parent){}

    QString getName(void){return "NAOM";}
    double getCNratio(void){return cn_ratio;}
    void setCNratio(double newcn,double newini){cn_ratio_temp=((cn_ratio_temp*value_temp)+(newcn*newini))/(value_temp+newini);value_temp+=newini;}
    void setCNratio_fin(){cn_ratio=cn_ratio_temp;}

public slots:
    void doUpdate(void);
    virtual void configure(const QJsonObject &theConfig);

private:
    double cn_ratio=20,value_temp=0, cn_ratio_temp=20;

};

/**
 * @brief Stabilized soil organic matter (SSOM)
 */

class Ssom : public Component
{
    Q_OBJECT
public:
    explicit Ssom(QObject *parent = 0):Component(parent){}

    QString getName(void){return "SSOM";}
    double getCNratio(void){return cn_ratio;}
    void setCNratio(double newcn,double newini){cn_ratio=((getCNratio()*getValue())+(newcn*newini))/(getValue()+newini);}
    double till_destabilize=0.005;

public slots:
    void doUpdate(void);
    virtual void configure(const QJsonObject &theConfig);

private:
    double cn_ratio=8;

};
/**
 * @brief Active microorganisms - currently bacteria, community represented as one species
 */

class Microbes : public Component
{
    Q_OBJECT
public:
    explicit Microbes(QObject *parent = 0):Component(parent){}

    QString getName(void){return "Microbes";}
    double getCNratio(void){return cn_ratio;}
    double getCNratio_temp(void){return cn_ratio_temp;}
    double getCredu(void){return C_redu;}
    void setCredu(double newcredu){C_redu=newcredu;}
    double getFBratio(void){return fb_ratio;}
    void setFBratio(double newfb){fb_ratio=newfb;}

    void setCNratio_temp(double newcn){cn_ratio_temp=newcn;}
    void setCNratio(double newcn){cn_ratio=newcn;}
    double till_death=0.3; double till_inactive=0.3;

public slots:
    void doUpdate(void) override;
    virtual void configure(const QJsonObject &theConfig);

private:
    double cn_ratio=8, cn_ratio_temp=8,C_redu=0,fb_ratio=1;
};

/**
 * @brief Inactive microorganisms - currently bacteria, community represented as one species
 */

class SleepMicrobes : public Component
{
    Q_OBJECT
public:
    explicit SleepMicrobes(QObject *parent = 0):Component(parent){}

    QString getName(void){return "SleepMicrobes";}
    double getCNratio(void){return cn_ratio;}

public slots:
    void doUpdate(void);
    virtual void configure(const QJsonObject &theConfig);

private:
    double cn_ratio=8;
};
/**
 * @brief Total mineral nitrogen
 *
 * Pool of total mineral nitrogen, internally splitted into NH4 and NO3 for calculation in process nitro_cycle
 * Input internal through SOM described in Nitro_Cycle and external through fertilizers
 */

class MinNitro : public Component
{
    Q_OBJECT
public:
    explicit MinNitro(QObject *parent = nullptr):Component(parent){}

    QString getName(void){return "MinNitro";}
    double getNH4(void){return NH4;}
    double getNO3(void){return NO3;}
    void setNH4(double newNH4){NH4=newNH4; NH4_temp=0;}
    void setNO3(double newNO3){NO3=newNO3; NO3_temp=0;}
    void setNH4_solid(double newNH4){NH4_solid=newNH4;}
    void setNO3_solid(double newNO3){NO3_solid=newNO3;}
    double getNH4_solid(void){return NH4_solid;}
    double getNO3_solid(void){return NO3_solid;}
    void addNH4(double newNH4){NH4_temp+=newNH4;}
    void addNO3(double newNO3){NO3_temp+=newNO3;}
    double getNH4t(void){return NH4_temp;}
    double getNO3t(void){return NO3_temp;}
    double getValue(void){return NO3+NH4;}

public slots:

    virtual void configure(const QJsonObject &theConfig);
    void doUpdate(void);

private:
    double NH4=0;
    double NO3=0;
    double NH4_temp=0;
    double NO3_temp=0;
    double NH4_solid=0;
    double NO3_solid=0;
};


class RootDemand : public Component
{
    Q_OBJECT
public:
    explicit RootDemand(QObject *parent = 0):Component(parent){}

    QString getName(void){return "RootDemand";}

};
/*****************************************************************//**
 * @brief The Waterpore class links the water, temperature and air dynamics to the soil nodes.
 * 
 * The Wat
 */
class Waterpore : public Component
{
    Q_OBJECT
public:
    explicit Waterpore(QObject *parent = nullptr):Component(parent){}
    //void evaluate(double timestep);
    QString getName(void) override {return "Waterpore";}
    double getValue() override {return psi();}
    double getMass() override {return watercontent();}
    double getVolume() override;

    double theta(void);
    double poreDensity(void);
    double psi(void);
    double temperature(void);
    double watercontent(void);
    double topval(void);
    void addSubNode(wsnode *actNode){theSubNodes.append(actNode);}
    double getMacropores(double boundary=1e-4);
    double airContent(void);
    double awc(void);
    /*************************************************************//**
     * Diffusivity of air defined by \f$\theta\f$, total porosity and macropores.
     *
     * Based on \cite moldrup+04 the relative gas diffusion coefficient
     * \f$  D_p/D_0 \f$ is calculated with \f$D_p\f$ the coefficient in soil and \f$D_0\f$
     * the coefficient in air (0.176 cm²/s -> 1.76e-5m²/s). It is calculated as
     * \f{eqnarray} \frac{D_p}{D_0} & = & \Phi^2(\epsilon/\Phi)^X \\
     * X &=& \log_{10}((2\epsilon_{100}^3 + 0.04 \epsilon_{100})/\Phi^2)/\log_{10}(\epsilon_{100}/\Phi) \f}
     *
     * \f$\epsilon\f$ is current air content, \f$\epsilon_{100}\f$ air content at 100 cm water column.
     * \f$\Phi\f$ defines soil's total porosity.
     *
     *
     * ***************************************************************/
    double getAirDiffusivity(void);
    QList<wsnode*> theSubNodes;

private:
    int subNum;
    bool isConfigured = false;

public slots:
    virtual void configure(const QJsonObject &theConfig) override;
    void doUpdate(void) override;
    void rootUp(double);
    void stressMe(double);

};
/**
 * @brief The Oxygen class provides amount and concentration of oxygen in a soil node
 *
 * Oxygen is considered here in the gas phase only. Concentrations solved in water are small,
 * and diffusivity in water also is small.
 * Mass is in kg/m² per node, concentration in kg/m³. Volume is 0 as it resides within the gas phase.
 * The value of concentration in each node can be initialised with "Oxygen": concVal, in the JSON file.
 */
class Oxygen : public Component
{
    Q_OBJECT
public:
    explicit Oxygen(QObject *parent = nullptr);
    QString getName(void) override {return "Oxygen";}
    double concentration(void){return concOxy;} /**< concentration of oxygen in the gas phase (kg/m³) */
    double getValue(void) override {return concentration();}
    double getMass(void) override;
    void addToOxy(double change){tempOxy+=change;}
    void consume(double mass){ mass /=thePore->airContent();
                                 tempOxy+=mass;}/**< consumation of oxygen in kg */
    double getTempOxy(void){return tempOxy;}
    void setValue(double oxyconc) override {concOxy=oxyconc;}

public slots:
    virtual void configure(const QJsonObject &theConfig) override;
    void doUpdate(void) override;

private:
    double concOxy, tempOxy;
    Waterpore *thePore;


};
/**
 * @brief The structCompaction class performs daily structural change due to load.
 */
class structCompaction : public Process
{
    Q_OBJECT

public:
    explicit structCompaction(QObject *parent=0);
    void evaluate(double timeStep);
    QString getName(void){return "structCompaction";}

private:
    Waterpore *theWater;
    SoilNode *theNode;

};
/**
 * @brief The oxyTransport class diffuses oxygen at each time step.
 *
 * Oxygen is diffusing through the soil profile proportional to the concentration gradient in
 * the gas phase. The gradient is multiplied by the harmonic mean of the diffusion coefficient
 * of the neighbouring nodes. Thus, two nodes are to be considered: the current node and the top node.
 * Top node for the uppermost node is the atmosphere with a fixed O² concentration of
 * 0.21*0.032 kg/mol / 22.414e-3m³/mol = 0,29981262 kg/m³ (0.3 is used).
 */
class oxyTransport : public Process
{
    Q_OBJECT

public:
    explicit oxyTransport(QObject *parent=0);
    void evaluate(double timeStep);
    QString getName(void){return "OxyTransport";}

private:
    Oxygen *theOxy, *theUpperOxy;
    Waterpore *theWater, *theUpperWater;
    SoilNode *theUpperNode, *theNode;

};

/**
 * @brief Description of root growth
 *
 * Depends on demand for roots, bulk density of Matrix and existing Macropore structure
 */

class RootGrowth : public Process
{
    Q_OBJECT
public:
    explicit RootGrowth(QObject *parent = 0);
    void evaluate(double timeStep);
    QString getName(void){return "RootGrowth";}

private:
    Root* theRoot;
    Macropore* thePore;
    Matrix* theMatrix;
    Fom* theFom;
    RootDemand* theDemand;
    MinNitro* theMinNitro;
    Waterpore* theWaterpore;
    Aom* theAom;
    bool rts_dp;
    double r_mot=0.005;        //decay rate, Yang et al 2004

};

/**
 * @brief Feeding and breeding of organisms
 */

class Feed_Breed : public Process
{
    Q_OBJECT
public:
    explicit Feed_Breed(QObject *parent = 0);
    void evaluate(double timeStep);
private:
    Macropore* thePore;
    Matrix* theMatrix;
    BioX* theBioX;
    Fom* theFom;
    Waterpore* theWaterpore;
    double l_osom = lambda_osom;
    double l_somo1 = lambda_somo1;
    double l_somo2 = lambda_somo2;
    double l_op = lambda_op;
    double l_pop = lambda_pop;
    bool process_act=false;


public slots:
    virtual void configure(const QJsonObject &theConfig);

    
};

class Som_Turn : public Process
{
    Q_OBJECT
public:
    explicit Som_Turn (QObject *parent = 0);
    void evaluate(double timeStep);
private:
    Fom* theFom;
    Aom* theAom;
    Ssom* theSsom;
    Matrix* theMatrix;
    Waterpore* theWaterpore;
    Microbes* theMicrobes;
    SleepMicrobes* theSleepMicrobes;
    NAom* theNAom;
    double eta = ETA;
    double k_m = k_M;
    double k_s = k_S;
    double k_a = k_A;
    double k_fom = k_FOM;
    double k_ntoa=1e-6;
    double stab_am=0.9;
    double r_death=0.01; //motility rate bacteria/AOM
    double FAT=0;
    double pock_vol=0;

public slots:
    virtual void configure(const QJsonObject &theConfig);
};

/*! \brief Mineral nitrogen cycle
 *
 * Processes involved in mineral part of nitrogen cycle including interaction with organic matter (mineralization, immobilization).
 * Total nitrogen amount \f$N\,[kg\,m^{-2}]\f$ is the sum of the ammonium amount \f$N_{NH4}\,[kg\,m^{-2}]\f$ and the nitrate amount \f$N_{NO3}\,[kg\,m^{-2}]\f$ in the system
 * \f[N=N_{NH4}+N_{NO3}\f]
 *
 */

class Nitro_Cycle : public Process
{
    Q_OBJECT
public:
    explicit Nitro_Cycle (QObject *parent = nullptr);
    void evaluate(double timeStep);
private:
    Fom* theFom;
    Aom* theAom;
    MinNitro* theMinNitro;
    Matrix* theMatrix;
    Waterpore* theWaterpore;
    Microbes* theMicrobes;   
    SleepMicrobes* theSleepMicrobes;
    NAom* theNAom;
    double N2=0;
    double k_nitri=0;
    double k_denitri=0;
    double Kconst_nitri=0;
    double dissolve_rate=0.01;
    double n_atmo=0;

public slots:
   virtual void configure(const QJsonObject &theConfig);
};

/*! \brief Dynamics of active and inactive microbes
 *
 *
 */

class Microbe_Dyn : public Process
{
    Q_OBJECT
public:
    explicit Microbe_Dyn (QObject *parent = nullptr);
    void evaluate(double timeStep);
    QString getName(void){return "Microbe_Dyn";}

    double necro_am=0.9;

private:
    Fom* theFom;
    Aom* theAom;
    Microbes* theMicrobes;
    SleepMicrobes* theSleepMicrobes;
    Matrix* theMatrix;
    Waterpore* theWaterpore;
    NAom* theNAom;
    MinNitro* theMinNitro;
    Oxygen* theOxygen;
    double growth_r=0.0036;           //growth rate in h-1
    double maint_r=0.0003;          //maintenance rate in h-1, value from Saras Diss ?
    double death_r=0.002;           //death as fixed proportion?
    double capacity=0.9;            //capacity as proportion of waterfilled pore space
    double kmm=5;                    //michaelis menten constant, kg m-3
    double drought_thre=0.2;        //threshold for water reduction function under which active microbes go inactive
    double drought_per=0.1;         //amount going inactive
    double sporolater=0;
    double spor_am=0.2;
    double CUE=0.6;
    double active_th=0.1;
    double active_p=0.6;
    double temp_thre=0.2;
    double oxyConRate=2.3;              //kg o2 / kg Biomasse
    double cn_ratio_b=6,cn_ratio_f=14;
    double km_ox=0.01;

public slots:
   virtual void configure(const QJsonObject &theConfig);
};


#endif // SOILNODE_H
