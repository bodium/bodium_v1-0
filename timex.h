#ifndef TIMEX_H
#define TIMEX_H
#include <QDateTime>
#include "soilnode.h"

/**
 * @brief Class for handling time issues
 */
class timeX : public QObject
{
    Q_OBJECT
public:
    explicit timeX(QObject *parent = 0, double timeStep = DAY);
    timeX(const QJsonObject &theConf, QObject *parent);
    void init(const QJsonObject &theConf);
    QDateTime startModelTime;
    QDateTime endModelTime;
    QDateTime currentModelTime;
    double total_secs;
    double currentTimeStepSize;

public slots:
    void doTimeSteps(void);
    void step_py(void);

signals:
    double timeStepped(double);
    double update(void);
    int finished(int);
    void nodedel(void);
    void write(void);
};

#endif // TIMEX_H
