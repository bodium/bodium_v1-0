#include "wsnode.h"
#include "watercell.h"
#include "conductor.h"
#include "soilprofile.h"
#include "soilnode.h"
#include <math.h>
#include <QRandomGenerator>
#include <QJsonObject>
wsnode::wsnode(QObject *parent) : QObject(parent)
{

}   
wsnode::wsnode(const QJsonObject &theInit, QObject *parent) : QObject(parent)
{

    init(theInit);
}
void wsnode::initBoundary(void)
{
    matrixCell = new WaterCell(this);
    matrixCell->setName("Boundary");
    matrixCell->setRadius(1.0);
    nodeThickness = 0.01;
    isBoundary = true;

}
void wsnode::init(const QJsonObject &init)
{
    WaterCell *theCell;
    numCells = init["NumCells"].toInt();
    horName = init["Name"].toString();
    double capo = (init["Theta_s"].toDouble() - init["Theta_r"].toDouble());
    double alpha = init["Alpha"].toDouble();
    double vGN = init["N"].toDouble();
    lKsat = init["Ksat"].toDouble();
    double airEntry = -10;
    airEntry = init["airEntry"].isUndefined()?airEntry:init["airEntry"].toDouble();
    double PotSorb = init["Pot_sorb"].toDouble();
    double tau = init["Tau"].toDouble();
    double immo = init["Theta_r"].toDouble();
    double makro = init["Macropore_Volume"].toDouble();
    innerDist = init["InnerDist"].toDouble();
    bool initPsi = false;
    initPsi = init["initPsi"].isUndefined()?initPsi:init["initPsi"].toBool();
    nodeThickness = init["Matrix_Weight"].toDouble()/2650/(1-init["Theta_s"].toDouble());
    if(!init["SubNodes"].isUndefined())
        nodeThickness /= init["SubNodes"].toDouble();
    if(!init["Temperature"].isUndefined())
        lTemp = init["Temperature"].toDouble();
    else {
        lTemp = 8.5;
    }
    double lowIncomp = -1e-5;
    lowIncomp = init["lowIncomp"].isUndefined()?lowIncomp:init["lowIncomp"].toDouble();
    double highCompSpread = 1e-1;
    highCompSpread = init["highCompSpread"].isUndefined()?highCompSpread:init["highCompSpread"].toDouble();
    double Sato = init["Saturation"].toDouble()*nodeThickness*1000;
    double actSat, Sat=0, desorb_psi;
    lMass = init["Matrix_Weight"].toDouble();
    matrixCell = new WaterCell(this);
    matrixCell->setName(QString("_matrix").prepend(horName));
    matrixCell->setRadius(-1);
    matrixCell->setMatrix();
    matrixCell->setTheta(0);
    matrixCell->setPsi(0.0);
    if(immo>0){
        theCell = new WaterCell(this);
        theCell->init(immo*nodeThickness*1000, 1e-15,1,-1e10,-1e10,immo*nodeThickness*1000);
        theCell->setName(QString("_immo").prepend(horName));
        cells.append(theCell);
    }
    if(makro>0){
        theCell = new WaterCell(this);
        theCell->init(makro*nodeThickness*1000, 1e-5/8 * makro,1,-290,-290,0);//Lf: ny_w =10-3 kg/m/s; r = 10-4; Anzahl/m² = 1/πr² -> r²/8Ny_w *makro
        theCell->setName(QString("_makro").prepend(horName));
        cells.append(theCell);
        connect(this,SIGNAL(stressOccurred(double)),theCell, SLOT(stressMe(double)));
    }
    if(!initPsi){
        capo/=numCells;

        while(Sat < 0.99999){
            //while((desorb_psi < 1.0) && (oldpsi - desorb_psi > nodeThickness/2)){
            //   factor *=2;
            //   desorb_psi = pow((pow(Sat + 0.5/(numCells * factor), -vGN/(vGN-1))-1), 1/vGN)/alpha;
            //}
            Sat += 0.5/(numCells);
            desorb_psi = -pow((pow(Sat, -vGN/(vGN-1))-1), 1/vGN)/alpha;

            if(Sato >0){
                if(Sato > capo){
                    actSat = capo;
                } else {
                    actSat = Sato;
                }
            }else{
                actSat = 0;
            }
            Sato -= actSat;
            theCell = new WaterCell(this);

            double condu;
            if(Sat +0.5/(numCells)>0.999)
                condu = lKsat;
            else
                condu = lKsat * pow(Sat+ 0.5/(numCells), tau) * pow( 1-pow(1-pow(Sat+ 0.5/(numCells ), vGN/(vGN-1)), 1-1/vGN),2);
            condu -= lKsat * pow(Sat - 0.5/(numCells ), tau) * pow( 1-pow(1-pow(Sat - 0.5/(numCells), vGN/(vGN-1)), 1-1/vGN),2);
            theCell->init(capo*nodeThickness*1000, condu, 1, desorb_psi, desorb_psi + PotSorb, actSat*nodeThickness*1000);
            theCell->setName(QString("_%1").prepend(horName).arg(desorb_psi));
            cells.append(theCell);
            connect(this,SIGNAL(stressOccurred(double)),theCell, SLOT(stressMe(double)));

            Sat += 0.5/(numCells);

        }
    }else{
        desorb_psi = 1e9;
        double oldSat= 0,newSat = 0,oldPsi =1e9, expo=9;
        while(desorb_psi >= -airEntry - 0.00005){
            expo -= (9-log10(-airEntry))/numCells;
            desorb_psi = pow(10,expo);
            double spread = desorb_psi < -lowIncomp?(desorb_psi + lowIncomp)/(lowIncomp-airEntry)*highCompSpread:0;
            newSat = pow(1+pow(alpha*desorb_psi, vGN),1/vGN-1);
            double capLoc = (newSat-oldSat)*capo;
            if(Sato >0){
                if(Sato > capLoc){
                    actSat = capLoc;
                } else {
                    actSat = Sato;
                }
            }else{
                actSat = 0;
            }
            Sato -= actSat;
            theCell = new WaterCell(this);
            spread *= capLoc;

            double condu;
            condu = lKsat * pow(newSat, tau) * pow( 1-pow(1-pow(newSat, vGN/(vGN-1)), 1-1/vGN),2);
            condu -= lKsat * pow(oldSat, tau) * pow( 1-pow(1-pow(oldSat, vGN/(vGN-1)), 1-1/vGN),2);
            theCell->init(capLoc*nodeThickness*1000, condu, 1, -(desorb_psi + oldPsi)/2, -(desorb_psi + oldPsi)/2+ PotSorb, actSat*nodeThickness*1000,(capLoc-spread)*nodeThickness*1000,(capLoc+spread)*nodeThickness*1000);
            theCell->setName(QString("_%1").prepend(horName).arg((desorb_psi +oldPsi)/2));
            cells.append(theCell);
            connect(this,SIGNAL(stressOccurred(double)),theCell, SLOT(stressMe(double)));

            oldSat = newSat;
            oldPsi=desorb_psi;
        }
    }

    }

    void wsnode::addSourceCond(conductor* condi)
    {
        condSourceList.append(condi);
    }
    void wsnode::addTargetCond(conductor* condi)
    {
        condTargetList.append(condi);
    }
    void wsnode::addInternalCond(QList<conductor*> &condList)
    {
        WaterCell *sourceCell, *targetCell;
        targetCell = matrixCell;
        foreach(sourceCell, cells){
            conductor *newCond = new conductor(this);
            newCond->source = sourceCell;
            newCond->target = targetCell;
            newCond->setPotSource(sourceCell->psi_d());
            newCond->setPotTarget(targetCell->psi_s());
            newCond->setGradientZ(0);
            newCond->setCondSource(sourceCell->horConduct());
            newCond->setCondTarget(targetCell->horConduct());
            newCond->setKsat_target(targetCell->horConduct());
            newCond->setDistSource(innerDist/2);
            newCond->setDistTarget(innerDist/2);
            connect(targetCell,SIGNAL(psiChanged(double)),newCond,SLOT(setPotTarget(double)));
            connect(this, SIGNAL(horConductChanged(double)), newCond,SLOT(setCondTarget(double)));
            connect(sourceCell, SIGNAL(goneEmpty(bool)), newCond, SLOT(sourceEmpty(bool)));
            condList.append(newCond);
            condTargetList.append(newCond);
            newCond = new conductor(this);
            newCond->setGradientZ(0);
            newCond->target = sourceCell;
            newCond->source = targetCell;
            newCond->setPotSource(targetCell->psi_d());
            newCond->setPotTarget(sourceCell->psi_s());
            condList.append(newCond);
            condSourceList.append(newCond);
            newCond->setCondSource(targetCell->horConduct());
            newCond->setCondTarget(sourceCell->horConduct());
            newCond->setKsat_target(sourceCell->horConduct());
            newCond->setDistSource(innerDist/2);
            newCond->setDistTarget(innerDist/2);
            connect(targetCell,SIGNAL(psiChanged(double)),newCond,SLOT(setPotSource(double)));
            connect(this, SIGNAL(horConductChanged(double)), newCond,SLOT(setCondSource(double)));
            connect(targetCell, SIGNAL(goneFull(bool)), newCond, SLOT(targetFull(bool)));
            //connect(sourceCell, SIGNAL(goneEmpty(bool)), newCond, SLOT(noteSwitch(bool)));






        }
        emit horConductChanged(horConduct());


    }
    void wsnode::addexternalCond(wsnode *upper, QList<conductor *> &condList)
    {
        double halfthick = nodeThickness/2;
        double halfupper = upper->nodeThick()/2;
        conductor *newcond;
        newcond = new conductor(this);
        newcond->target = matrixCell;
        newcond->source = upper->matrixCell;
        newcond->setTempTarget(lTemp);
        newcond->setTempSource(upper->temperature());
        newcond->setGradientZ(-9.81e3); // Pa/m
        newcond->setKsat_target(ksat());
        condList.append(newcond);
        condTargetList.append(newcond);
        upper->addSourceCond(newcond);
        topin = newcond;
        connect(matrixCell,SIGNAL(psiChanged(double)),newcond,SLOT(setPotTarget(double)));
        connect(upper->matrixCell,SIGNAL(psiChanged(double)),newcond,SLOT(setPotSource(double)));
        connect(this,SIGNAL(tempChanged(double)),newcond,SLOT(setTempTarget(double)));
        connect(upper,SIGNAL(tempChanged(double)),newcond,SLOT(setTempSource(double)));
        connect(this, SIGNAL(conductChanged(double)), newcond, SLOT(setCondTarget(double)));
        connect(upper, SIGNAL(conductChanged(double)), newcond, SLOT(setCondSource(double)));
        connect(this, SIGNAL(tempConductChanged(double)), newcond, SLOT(setTempCondTarget(double)));
        connect(upper, SIGNAL(tempConductChanged(double)), newcond, SLOT(setTempCondSource(double)));
        //   connect(static_cast<waterProfile*>(parent()),SIGNAL(stepTime(double)),newcond, SLOT(conductEnergy(double)));
        connect(static_cast<SoilProfile*>(parent()->parent()->parent()),SIGNAL(stepTime(double)),newcond, SLOT(conductEnergy(double)));
        newcond->setDistSource(halfupper);
        newcond->setDistTarget(halfthick);
        newcond = new conductor(this);
        newcond->source = matrixCell;
        newcond->target = upper->matrixCell;
        newcond->setKsat_target(upper->ksat());
        newcond->setTempSource(lTemp);
        newcond->setTempTarget(upper->temperature());
        newcond->setGradientZ(9.81e3); //Pa/m
        condList.append(newcond);
        condSourceList.append(newcond);
        upper->addTargetCond(newcond);
        connect(matrixCell,SIGNAL(psiChanged(double)),newcond,SLOT(setPotSource(double)));
        connect(upper->matrixCell,SIGNAL(psiChanged(double)),newcond,SLOT(setPotTarget(double)));
        connect(this,SIGNAL(tempChanged(double)),newcond,SLOT(setTempSource(double)));
        connect(upper,SIGNAL(tempChanged(double)),newcond,SLOT(setTempTarget(double)));
        //   connect(static_cast<waterProfile*>(parent()),SIGNAL(stepTime(double)),newcond, SLOT(conductEnergy(double)));
        connect(static_cast<SoilProfile*>(parent()->parent()->parent()),SIGNAL(stepTime(double)),newcond, SLOT(conductEnergy(double)));
        newcond->setDistTarget(halfupper);
        newcond->setDistSource(halfthick);
        connect(this, SIGNAL(conductChanged(double)), newcond, SLOT(setCondSource(double)));
        connect(upper, SIGNAL(conductChanged(double)), newcond, SLOT(setCondTarget(double)));
        connect(this, SIGNAL(tempConductChanged(double)), newcond, SLOT(setTempCondSource(double)));
        connect(upper, SIGNAL(tempConductChanged(double)), newcond, SLOT(setTempCondTarget(double)));
        topout = newcond;




    }

#define C1 0.55
#define C2 0.8
#define C3 3.07
#define C4 0.13
#define C5 4
#define F1 13.05
#define F2 1.06

    double wsnode::heatCapacity()
    {
        if(isBoundary)
            return 100000;
        return 2060 * icecontent() + 4200 * watercontent() + 850 * lMass;

    }
    double wsnode::tempConduct()
    {
        double lambda, theta_t, theta_i;
        if(isBoundary)
            return 10;
        theta_i = icecontent()/1000/nodeThickness;
        theta_t = theta();
        theta_t += theta_i + F1*pow(theta_i, F2+1);
        lambda = C1 + C2* theta_t - (C1-C4)*exp(-C3*pow(theta_t, C5));
        //if(fabs(lambda-oldLambda)> 1e-6){
        emit tempConductChanged(lambda);
        oldLambda = lambda;
        //}
        if(lambda<0)
            printf("Scheissekalt?");
        return lambda;

    }
    double wsnode::theta()
    {
        WaterCell *actcell;
        double content= 0;
        foreach (actcell, cells) {
            if(!actcell->boundary())
                content += actcell->theta();
        }
        return content/1000/nodeThickness;//actcell->theta is kg, transfer to dimless!
    }
    double wsnode::poreDensity()
    {
        WaterCell *actcell;
        double content= 0;
        foreach (actcell, cells) {
            if(!actcell->boundary())
                content += actcell->capacity();
        }
        return content/1000/nodeThickness;//actcell->theta is kg, transfer to dimless!
    }
    double wsnode::watercontent()
    {
        WaterCell *actcell;
        double content= 0;
        foreach (actcell, cells) {
            if(!actcell->boundary())
                content += actcell->theta();
        }
        return content;//actcell->theta is kg!
    }

    double wsnode::icecontent()
    {
        WaterCell *actcell;
        double content= 0;
        foreach (actcell, cells) {
            content += actcell->ice();
        }
        return content;//actcell->theta is kg!
    }

#define minConduct 1e-20
    double wsnode::conduct()
    {
        WaterCell *actcell;
        double conduct= minConduct;
        foreach (actcell, cells) {
            conduct += actcell->conduct();
        }
        if(conduct>1e-17){
            if(fabs(oldConduct/conduct-1)>0.05){
                oldConduct= conduct;
                emit conductChanged(conduct);
                //emit horConductChanged(conduct);
            }
        }else{oldConduct= conduct;}
        return conduct;
    }

    double wsnode::horConduct()
    {
        WaterCell *actcell;
        double conduct= 0;
        foreach (actcell, cells) {
            conduct += actcell->horConduct();
        }
        if(conduct>1e-20){
            if(fabs(oldConduct/conduct-1)>0.05){
                oldHorConduct= conduct;
                emit horConductChanged(conduct);
            }
        }else{oldHorConduct= conduct;}
        return conduct;
    }
    double wsnode::psi()
    {
        return matrixCell->psi_d();
    }
    void wsnode::evalDTheta()
    {
        double dTheta = 0;
        conductor* theConduct;
        foreach(theConduct, condSourceList){
            //if(theConduct->isItActive()){
            dTheta -= theConduct->conduct();
            //}
        }
        foreach(theConduct, condTargetList){
            //if(theConduct->isItActive()){
            dTheta += theConduct->conduct();
            //}
        }





    }

    //double wsnode::topval()
    //{return topin->conduct() - topout->conduct();}
#define maxErr 1e-13 //error of water content change per soil node
    void wsnode::findZero()
    {
        double psi[2], val[2];
        double lastPsi, oldPsi,lastval;
        int i=1;
        evalDTheta();
        psi[0] = oldPsi = lastPsi = matrixCell->psi_d();
        val[0] = matrixCell->deltaTheta();//+matrixCell->theta()/DAY;
        if(fabs(val[0]) < maxErr)
            return;
        matrixCell->setPsi(lastPsi + 0.1);
        evalDTheta();
        psi[1] = lastPsi = matrixCell->psi_d();
        val[1] = matrixCell->deltaTheta();//+matrixCell->theta()/DAY;
        /************************************
      * https://ganymed.math.uni-heidelberg.de/~lehre/SS12/numerik0/2-nullstellen.pdf Definition 2.24
      * Sekantenverfahren, stabilisiert:
      * Given a steady function on \f$ [a_0, b_0] := [a,b] \f$ and \f$ f(a)f(b) < 0\f$.
      * The latter is achieved only after an initial guess in our implementation.
      * Approximation of \f$ f(x_{n+1} = 0 \f$ is done by \f[
      * x_{n+1} := a_n - \frac{f(a_n)(b_n-a_n)}{f(b_n) - f(a_n)}
      * \f]
      * Next interval is defined by \f[
      * [a_{n+1], b_{n+1}] = \left\{\begin{array}{ll}
      * [a_n, x_{n+1}]& | f(a_n)f(x_{n+1} < 0\\
      * [x_{n+1}, b_n]&| f(x_{n+1}f(b_n) < 0\\
      * [x_{n+1}, x_{n+1}] &| f(x_{n+1}) = 0
      * \right.
      * \f]
      *
      ***********************************************************************/
        while(fabs(val[i]) > maxErr){
            i^=1;
            //if(fabs(val[i]-val[i^1])<maxErr||fabs(psi[i]-psi[i^1])<0.01)
            //   lastPsi += QRandomGenerator::global()->bounded(10.0)-5;
            //else
            if(fabs(psi[0]-psi[1])<1e-5)
                break;
            else{
                //if(fabs(val[i]/val[i^1])>1000||fabs(val[i^1]/val[i])>1000)
                //    lastPsi = (psi[i]+psi[i^1])/2;
                //else
                    lastPsi = (psi[i^1] - psi[i])/(val[i] - val[i^1])*val[i]+psi[i];
                if(fabs((lastPsi - psi[i])/(psi[i]-psi[i^1]))<1e-3||fabs((lastPsi - psi[i^1])/(psi[i]-psi[i^1]))<1e-3)
                    lastPsi = (psi[i]+psi[i^1])/2;
            }
            //if(lastPsi<-1e8)
            //  printf("too dry!");
            matrixCell->setPsi(lastPsi);
            evalDTheta();
            lastval = matrixCell->deltaTheta();
            //lastval += matrixCell->theta()/DAY;
            if(lastval*val[i]<0)
                i^=1;
            psi[i] = lastPsi;
            val[i] = lastval;

        }
        if(lastPsi< -1e10)
            printf("Dös is a Kaas!");

        //if(fabs(oldPsi-lastPsi)>1e-1)
        if(fabs((oldPsi-lastPsi)/lastPsi)>1e-1&&fabs(oldPsi-lastPsi)>1)   //relativer Fehler
            emit psiChanged(lastPsi);



    }
    void wsnode::addMacro(double macro)
    {
        cells.last()->setCapacity(cells.last()->capacity()+ macro * nodeThickness*1000);
    }

    void wsnode::addHeat(double energy)
    {
        if(!isBoundary){
            lTemp += energy/heatCapacity();
            if(lTemp <-30)
                printf("Hare's in the pepper!");
            emit tempChanged(lTemp);
        }
    }

    double wsnode::getMacropores(double boundary)

    {
        double pore=0;
        WaterCell*actcell;
        foreach(actcell,cells){
            if(actcell->getRadius()>boundary&& !actcell->boundary())
                pore+=actcell->capacity();
        }
        return(pore);
    }

    double wsnode::topval()
    {
        return topsum;
    }

    void wsnode::topbal(double timestep)
    {
        topsum += topin->conduct()*timestep;
        topsum -= topout->conduct()*timestep;
    }
    double wsnode::topcur(double timestep)
    {
        return topin->conduct()*timestep - topout->conduct()*timestep;
    }


    void wsnode::setRootUp(double rootup_l)
    {

        matrixCell->addToChange(-rootup_l+ rootUp);
        rootUp = rootup_l;
    }
double wsnode::waterCapa(double potential)
{
    double pore=0;
    WaterCell*actcell;
    foreach(actcell,cells){
        if(actcell->psi_d()<potential)
            pore+=actcell->capacity();
    }
    return(pore);

}
