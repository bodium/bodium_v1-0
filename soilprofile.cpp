#include "soilprofile.h"
#include "soilnode.h"
#include "plant_stand.h"
#include "bodiumrunner.h"
#ifdef WITH_GUI
#include <QMessageBox>
#endif
#include <QDir>
#include <QJsonArray>
#include <QJsonObject>
#include <QString>
#include <QJsonDocument>
#include "wsnode.h"
#include "conductor.h"
#include "watercell.h"
//#include <qtconcurrentmap.h>


//Function declarations for Soil profile

SoilProfile::SoilProfile(QObject *parent) : QObject(parent), theUpper(this)
{
    //theNodes.append(new SoilNode(this));
}

void SoilProfile::setNodes(const QJsonObject &jsonNodes)
{
    theNodes.clear();
    theWNodes.clear();
    theConducts.clear();
    SoilNode *currNode, *topNode = nullptr;

    QDir tmpCurrDir = QDir::current();

    QString profile_path=jsonNodes["Profile"].toString();

    QFile proFile(profile_path);
    if (!proFile.open(QIODevice::ReadOnly)) {
            qWarning("Couldn't open proFile.");
#ifdef WITH_GUI
            QMessageBox msgBox;
            msgBox.setText("ProFile not found, please check path");
            msgBox.exec();
#endif
        }

    QByteArray profData = proFile.readAll();
    QJsonDocument proFileDoc(QJsonDocument::fromJson(profData));

    groundwater=jsonNodes["Groundwater"].toBool();
    explicit_microbes=jsonNodes["Microbes"].toBool();
    if(jsonNodes.contains("waterOutFile")){
        waterOut = true;
        theWaterFile.setFileName(jsonNodes["waterOutFile"].toString());
        theWaterFile.open(QIODevice::WriteOnly);
        outWaterStream.setDevice(&theWaterFile);

    }

    QJsonArray jsonArray = proFileDoc["Profile"].toArray();

    foreach (const QJsonValue & value, jsonArray) {
        currNode = new SoilNode(value.toObject(), this, topNode);

        topNode = currNode;
        theNodes.append(currNode);

    }

    rootdepth=0;           //m
    sum_factor_rtl=0;
    lTrans_act=0;           //m s-1 actual transpiration, is calculated in calcWuptake; needed in plant stand to calculate water stress
    h2=0;
    rootVol=0;
}

void SoilProfile::deleteNodes(){
    SoilNode *actNode;

    foreach(actNode, theNodes){
        disconnect(actNode, nullptr, nullptr, nullptr);
        actNode->~SoilNode();
    }

    wsnode *wactNode = this->findChild<wsnode *>();
    wactNode->~wsnode();
}

double SoilProfile::actRoot()
{
    double sumRoot = 0;
    SoilNode *actNode;
    foreach(actNode, theNodes){
        sumRoot += actNode->getComponent("Root")->getValue();
    }
    return(sumRoot);
}

void SoilProfile::rootRequest(double request)
{
    SoilNode *actNode;
    foreach(actNode, theNodes){
        actNode->getComponent("RootDemand")->setValue(request);
    }

}

void SoilProfile::setDepthLoad(void){
    SoilNode *actNode;
    double depth=0, load=0;
    foreach(actNode, theNodes){
        actNode->setDepth(depth);
        depth+=actNode->getVolume();
        actNode->setLoad(load);
        load+=actNode->getMass();
    }
}

void SoilProfile::setRootFrac(void){
    SoilNode *actNode;
    rootVol=0;
    double rootVal=0;
    foreach(actNode,theNodes){
        rootVol+=actNode->getComponent("Root")->getVolume();
        rootVal+=actNode->getComponent("Root")->getValue();
    }
    static_cast<bodiumrunner*>(parent())->thePlantStand.setRootBio(rootVal);
}

/**
 * The uptake rate of nitrogen \f$N_{up}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$ consists of the uptake rate via convection \f$N_{up}^{conv}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$ and the uptake rate via diffusion \f$N_{up}^{diff}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$, in case the nitrogen demand of the plant is not yet fullfilled through the uptake via convection (see below)
 * \f[N_{up}=N_{up}^{conv}+N_{up}^{diff}\f]
 * This uptake rate is restricted by the maximum or the potential nitrogen uptake rate \f$N_{up}^{pot}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$, which is the sum of the nitrogen demand \f$N_{dem,x}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$ of the different plant organs \f$x\f$
 * \f[N_{dem,x}=B_{x}\,(N_{x}^{opt}-N_{x}^{cur})\f]
 * with \f$x=\{lvs,stm,rts\}\f$ for leaves, stem and roots; \f$B_{x}\,[kg\,m^{-2}]\f$ their biomass, \f$N_{x}^{opt}\,[kg_{N}\,kg^{-1}]\f$ the optimum nitrogen amount, and \f$N_{x}^{cur}\,[kg_{N}\,kg^{-1}]\f$ the current nitrogen amount.\n
 * The uptake rate via convection is calculated over the rooted soil profile with
 * \f[N_{up}^{conv}=\int_{0}^{z_{max}}W_{up}\,(N_{NH_{4}}+N_{NO_{3}})dz\f]
 * with \f$z_{max}\,[m]\f$ the rooting depth, \f$W_{up}\,[mm\,s^{-1}]\f$ the water uptake rate, \f$N_{NH_{4}}\,[kg_{N}\,m^{-3}]\f$ the concentration of ammonium in soil and \f$N_{NO_{3}}\,[kg_{N}\,m^{-3}]\f$ the concentration of nitrate.\n
 */

void SoilProfile::calcNuptake(double timeStep){
    SoilNode *actNode;
    MinNitro* theMinNitro;
    Root* theRoot;
    Waterpore* theWaterpore;
    double n_demand=static_cast<bodiumrunner*>(parent())->thePlantStand.nitro_demand[0];
    double mean_dia=static_cast<bodiumrunner*>(parent())->thePlantStand.mean_root_dia;

    double total_n=0,n_conv=0, n_diff=0;
    double water_up_n=0;
    foreach(actNode, theNodes){
        theRoot=static_cast<Root*>(actNode->getComponent("Root"));
        if(theRoot->getVolume()>0){
            double n_conv_node=0;
            water_up_n=actNode->getWaterup();
            theMinNitro=static_cast<MinNitro*>(actNode->getComponent("MinNitro"));
            theWaterpore=static_cast<Waterpore*>(actNode->getComponent("Waterpore"));
            double nh4conc = theMinNitro->getNH4()/theWaterpore->watercontent();
            double no3conc =theMinNitro->getNO3()/theWaterpore->watercontent();
            nh4conc = nh4conc > 0.043? 0.043:nh4conc;
            no3conc = no3conc > 0.043? 0.043:no3conc;

            n_conv_node=water_up_n*(nh4conc+no3conc);

            n_conv+=n_conv_node;

        }
    }
    double n_lim=static_cast<bodiumrunner*>(parent())->thePlantStand.n_lim;

    foreach(actNode, theNodes){
        theRoot=static_cast<Root*>(actNode->getComponent("Root"));
        if(theRoot->getVolume()>0){
            double n_conv_node=0;
            water_up_n=actNode->getWaterup();
            theMinNitro=static_cast<MinNitro*>(actNode->getComponent("MinNitro"));
            theWaterpore=static_cast<Waterpore*>(actNode->getComponent("Waterpore"));
            double nh4conc = theMinNitro->getNH4()/theWaterpore->watercontent();
            double no3conc =theMinNitro->getNO3()/theWaterpore->watercontent();
            nh4conc = nh4conc > 0.043? 0.043:nh4conc;
            no3conc = no3conc > 0.043? 0.043:no3conc;

            n_conv_node=water_up_n*(nh4conc+no3conc);

            if(n_conv>n_lim){
                double new_nup=(n_conv_node/n_conv)*n_lim;
                double ratio_nh4=nh4conc/(nh4conc+no3conc);
                ratio_nh4=(isnan(ratio_nh4))?0:ratio_nh4;
                theMinNitro->addNH4(-(new_nup*ratio_nh4));
                theMinNitro->addNO3(-(new_nup*(1-ratio_nh4)));
            }
            else{
                theMinNitro->addNH4(-(water_up_n*nh4conc));
                theMinNitro->addNO3(-(water_up_n*no3conc));
            }

        }
    }

     n_conv=(n_conv>n_lim)?n_lim:n_conv;

    /**
     * If \f$N_{up}^{conv}<N_{up}^{pot}\f$, nitrogen is additionally uptaken via diffusion, where the potential diffusion \f$N_{up}^{pot,diff}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$ is set to the difference between nitrogen demand and nitrogen uptake via convection
     * \f[N_{up}^{pot,diff}=N_{up}^{pot}-N_{up}^{conv}\f]
     *The maximum diffusive uptake \f$N_{up}^{max,diff}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$  depends on root surface and nitrogen concentration
     * \f[N_{up}^{max,diff}=\int_{0}^{z_{max}}\,2\,sur_{rts}\,D(\theta)\,\frac{\theta\,(N_{NH_{4}}+N_{NO_{3}})}{l_{d}}dz\f]
     * with \f$sur_{rts}\,[m^{2}]\f$ the root surface, \f$D(\theta)\,[m^{2}\,s^{-1}]\f$ the diffusion coefficient of water in soil, \f$\theta\f$ the soil water content and \f$l_d\,[m]\f$ the characteristic diffusion length between root surface and soil water, set to \f$10E-5\f$.
     * The root surface is calculated based on the root volume \f$Vol_{rts}\,[m^{3}]\f$ and the mean radius \f$r_{rts}\,[m]\f$ of the roots
     * \f[sur_{rts}=2\,\frac{Vol_{rts}}{r_{rts}}\f]
     * The actual diffusive uptake rate \f$N_{up}^{diff}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$ depends then on the difference between maximum and potential diffusive uptake rate with
     * \f[N_{up}^{diff}=\begin{cases}
            N_{up}^{max,diff} & \text{if $N_{up}^{max,diff}\leq N_{up}^{pot,diff}$} \\
            N_{up}^{pot,diff} & \text{if $N_{up}^{max,diff}>N_{up}^{pot,diff}$}
             \end{cases}
     * \f]
     */

     n_demand=(n_demand>n_lim)?n_lim:n_demand;

    if(n_conv<n_demand){

        double diff_coeff=5E-7;                    // diff_coeff = D(theta)/l_d in m/s, D is 5e-11 m²/s,
        double n_diff_pot=n_demand-n_conv;
        double totalNH4=0;
        double totalNO3=0;
        bool usepot;


        foreach(actNode, theNodes){
            theRoot=static_cast<Root*>(actNode->getComponent("Root"));
            if(theRoot->getVolume()>0){
                double n_diff_node=0;
                theMinNitro=static_cast<MinNitro*>(actNode->getComponent("MinNitro")); //kg/Node
                theWaterpore=static_cast<Waterpore*>(actNode->getComponent("Waterpore"));
                double surface=2*(theRoot->getVolume()/(mean_dia/2)); // pi r² -> 2pi r, m²/Node
                n_diff_node = surface * diff_coeff * timeStep*((theMinNitro->getNH4()+theMinNitro->getNH4t()+theMinNitro->getNO3()+theMinNitro->getNO3t())/theWaterpore->watercontent());

                n_diff+=n_diff_node;

                totalNH4+=theMinNitro->getNH4()+theMinNitro->getNH4t();
                totalNO3+=theMinNitro->getNO3()+theMinNitro->getNO3t();
            }
        }

        usepot=(n_diff<=n_diff_pot)?false:true;

        foreach(actNode, theNodes){
            theRoot=static_cast<Root*>(actNode->getComponent("Root"));
            if(theRoot->getVolume()>0){
                double n_diff_node=0;
                theMinNitro=static_cast<MinNitro*>(actNode->getComponent("MinNitro"));
                theWaterpore=static_cast<Waterpore*>(actNode->getComponent("Waterpore"));
                double surface=2*(theRoot->getVolume()/(mean_dia/2));
                n_diff_node = surface * diff_coeff * timeStep*((theMinNitro->getNH4()+theMinNitro->getNH4t()+theMinNitro->getNO3()+theMinNitro->getNO3t())/theWaterpore->watercontent());

              double ratio_NH4=(theMinNitro->getValue()>0)?(theMinNitro->getNH4()+theMinNitro->getNH4t())/(theMinNitro->getNH4()+theMinNitro->getNH4t()+theMinNitro->getNO3()+theMinNitro->getNO3t()):0;

                n_diff_node=(usepot)?n_diff_pot*(n_diff_node/n_diff):n_diff_node;

                theMinNitro->addNH4(-(n_diff_node*ratio_NH4));
                theMinNitro->addNO3(-(n_diff_node*(1-ratio_NH4)));
            }
        }
        n_diff=(usepot)?n_diff_pot:n_diff;
    }

    total_n=n_conv+n_diff;
    if(total_n>0){
    emit demandN(total_n);}
}

//sum Volume over whole profile / all nodes for specific component
double SoilProfile::sumVolume(QString thename){
    double sum_value=0;
    SoilNode *actNode;
    Component *rightMatch;

    foreach(actNode, theNodes){
        rightMatch=actNode->getComponent(thename);
        sum_value+=rightMatch->getVolume();
    }

    return sum_value;
}
   /**
    *The water reduction function \f$f_ {\psi}\f$ is calculated as
    * \f[f_ {\psi}=\begin{cases}
           0 & \text{if $\psi_{M,i}<h_3$} \\
           \frac{\psi_{M,i}-h_3}{h_{2,t}-h_3}  & \text{if $h_3 \leq \psi_{M,i}<h_{2,t}$} \\
           1 & \text{if $h_{2,t} \leq \psi_{M,i}<h_1$} \\
           \frac{\psi_{M,i}-h_0}{h_1-h_0}  & \text{if $h_1 \leq \psi_{M,i}<h_0$} \\
           0 & \text{if $h_0 \leq \psi_{M,i}$}
            \end{cases}
    * \f]
    * with \f$h_3\,[Pa]\f$ the matric potential below which water uptake ceases (usually equal to permanent wilting point), \f$h_1\,[Pa]\f$ the matric potential below which water uptake is optimal, \f$h_0\,[Pa]\f$ the matric potential above which water uptake ceases due to lack of aeration, \f$h_{2,t}\,[Pa]\f$ the matric potential below which water uptake is not optimal but has not completely stopped yet.
    * The latter depends on the potential transpiration \f$TR_{pot}\,[m\,s^{-1}]\f$
    * \f[h_{2,t}= \begin{cases}
           h_{2L} & \text{if $TR_{pot}<TR_{potL}$} \\
           h_{2L}+\frac{TR_{potH}-TR_{pot}}{TR_{potH}-TR_{potL}}\,(h_{2H}-h_{2L})  & \text{if $TR_{potL}\leq TR_{pot} \leq TR_{potH}$} \\
           h_{2H} & \text{if $TR_{potH}<TR_{pot}$}
            \end{cases}
    * \f]
    * with \f$h_{2L}\,[Pa]\f$ the matric potential at low transpiration demand, \f$h_{2H}\,[Pa]\f$ the matric potential at high transpiration demand, \f$TR_{potL}\,[m\,s^{-1}]\f$ the lower transpiration rate threshold and \f$TR_{potH}\,[m\,s^{-1}]\f$ the higher transpiration rate threshold.\n
    */

double SoilProfile::waterReduc(double psi){
    double h2=getH2();
    double stress=(psi<h3)?0:(psi<h2)?((psi-h3)/(h2-h3)):(psi<h1)?1:(psi<h0)?((psi-h0)/(h1-h0)):0;
    return stress;
}

/**
 * The total water uptake \f$W_{up}\,[mm\,s^{-1}]\f$ is the sum over the water uptake rate\f$W_{up,i}\,[mm\,s^{-1}]\f$ within all rooted soil nodes \f$i\f$
 * \f[W_{up,i}=\alpha_{i}\,W_{up,i}^{max}\f]
 * with \f$f_ {\psi}\f$ a water reduction function (see \link waterReduc \endlink), and \f$W_{up,i}^{max}[mm\,s^{-1}]\f$ the maximum water uptake within a soil node, which depends on the potential water uptake distribution \f$\beta_i\f$ and the potential transpiration rate
 * \f[W_{up,i}^{max}=\beta_i\,TR_{pot}\f]
 * with \f$\beta_i\f$ is assumed to be proportional to the root density distribution and can be estimated by normalisation
 * \f[\beta_i=\frac{d_{rts,L}}{\int_0^{z_{rts}}d_{rts,L}\,dz}\f]
 * with \f$d_{rts,L}\,[m\,m^{-3}]\f$ the root length density, and \f$z_{rts}\,[m]\f$ the depth of the rooting system.\n
 * The actual transpiration rate \f$TR_{act}\,[m\,s^{-1}]\f$ is then
 * \f[TR_{act}=TR_{pot}\,\int_{i=1}^{i=z_{rts}}\,f_ {\psi}\,\beta_i\f]
 *
 *  Update 26.01.2021
 * \f$\psi\f$ is calculated as weighted by root distribution arithmetic mean of all depths, then stress is calculated and transpiration is determined and distributed. Distribution is according to
 * \f[(\psi_m - \psi_{root} V_{root}/T_{akt} = 1/K_{rad}\f] with the assumption that the root matric potential can be approximated by the wilting point.
 */

void SoilProfile::calcWuptake(double trans_pot){
    SoilNode *actNode;
    Root* theRoot;
    Waterpore* theWaterpore;
    double water_up_tot=0;
    double root_density=sumVolume("Root");
    lTrans_act=0;
    double psi_t =0;
    double localfact =0;

    foreach(actNode, theNodes){

        theRoot=static_cast<Root*>(actNode->getComponent("Root"));
        theWaterpore=static_cast<Waterpore*>(actNode->getComponent("Waterpore"));

        if(theRoot->getVolume()>0){
            double psiloc=theWaterpore->psi();
            psi_t += psiloc*theRoot->getVolume();
            localfact += psiloc<h3?0:(psiloc - h3) *theRoot->getVolume();

            }
        }
    if(root_density > 0)
        psi_t /= root_density;
    water_up_tot = trans_pot*waterReduc(psi_t);
    if(localfact >0 && water_up_tot >0)
        localfact /=water_up_tot;
    foreach(actNode, theNodes){
        theRoot=static_cast<Root*>(actNode->getComponent("Root"));
        theWaterpore=static_cast<Waterpore*>(actNode->getComponent("Waterpore"));
        if((theRoot->getVolume()>0)&&theWaterpore->psi() > h3 && water_up_tot>0){
            actNode->setWaterup(((theWaterpore->psi() - h3) *theRoot->getVolume()/localfact));                          //kg m-2 d-1
        }else{
            actNode->setWaterup(0);
        }


    }
    lTrans_act=water_up_tot;

}

void SoilProfile::setTransAct(double transe)
{
    SoilNode *actNode;
    double porewise = transe/theNodes.count();
    foreach(actNode, theNodes){
        actNode->setWaterup(porewise);                          //kg m-2 d-1
    }
    lTrans_act = transe;
}
/**
 * Calculation of root length factor over the whole profile, is then used to calculate root length growth within each node
 */
void SoilProfile::rootLengthFactor(){
    SoilNode *actNode;
    Waterpore* theWaterpore;
    MinNitro* theMinNitro;
    double reduc_water=0, reduc_N=0, reduc_T=0;
    double sum_factor_l=0;
    double availN=availableN(rootdepth);
    double max_depth=static_cast<bodiumrunner*>(parent())->thePlantStand.pen_depth;

    reduc_N=1-(exp(-0.15*availN)*10000);
    reduc_N=(reduc_N<=0.01)?0.01:reduc_N;

   /**
    * Within each node, the root length factor \f$f_{RLF}\,[m]\f$ is calculated with
    * \f[f_{RLF}=f_{RLFW}\,\Delta\,z\,min{f_{\theta,rts};f_{N,rts}}\f]
    * with \f$f_{RLFW}\f$ the weighting factor
    * \f[f_{RLFW}=exp(-4(z-0.5\,\Delta\,z)/z_{rts,max}))\f]
    * with \f$z\,[m]\f$ the depth of the node, \f$\Delta\,z\,[m]\f$ the thickness of the node, and \f$z_{rts,max}\,[m]\f$ the maximal rooting depth; and \f$f_{\psi,rts}\f$ the redcution function of water, calculated based on the matric potential (see \link waterReduc() \endlink), and \f$f_{N,rts}\f$ the reduction function of nitrogen
    * \f[f_{N,rts}=min(0.01;1-(exp(-0.15\,N_{min})))\f]
    * with \f$N_{min}\,[kg\, m^{-2}]\f$ the summed amount of mineral nitrogen in all rooted soil nodes.
    * In the deepest rooted soil node the root length factor is corrected by the rooted proportion \f$z_{rts}\,[m]\f$
    * \f[f_{RLF}=f_{RLF}\,(1-(z-0.5\,\Delta\,z-z_{rts})/\Delta\,z)\f]
    */

    foreach(actNode, theNodes){
        double depth=actNode->getDepth();

        if(rootdepth>depth){
            theWaterpore=static_cast<Waterpore*>(actNode->getComponent("Waterpore"));
            theMinNitro=static_cast<MinNitro*>(actNode->getComponent("MinNitro"));

            double temp=theWaterpore->temperature();
            double psi_t=theWaterpore->psi();
            double thick=actNode->getVolume();

            reduc_water=waterReduc(psi_t);

            reduc_T=static_cast<bodiumrunner*>(parent())->thePlantStand.tempFunc_Optima(temp,-4,25,35);

            double factor_length=0, fct_l=0;    //root length growth factor (m), and weighting factor (-)

            fct_l=exp(-4*((depth+thick)-0.5*thick)/max_depth);
            factor_length=fct_l*thick*((reduc_water<reduc_N)?reduc_water:reduc_N);

            if(rootdepth<(depth+thick)){
                factor_length=factor_length*((rootdepth-depth)/thick);

            }

            actNode->setFactorRTL(factor_length);
            sum_factor_l+=factor_length;
        }

    }
    setSumFtRtl(sum_factor_l);
}

double SoilProfile::availableN(double rootingdepth){
    SoilNode *actNode;
    MinNitro* theMinNitro;
    double availN_tot=0;

    foreach(actNode, theNodes){
        theMinNitro=static_cast<MinNitro*>(actNode->getComponent("MinNitro"));

        double depth=actNode->getDepth();

        if(rootingdepth>depth){
            availN_tot=(isnan(theMinNitro->getValue()))?availN_tot:availN_tot+theMinNitro->getValue();
        }
    }
    return availN_tot;
}


void SoilProfile::calcNtransport(double timeStep){
    SoilNode *actNode, *prevNode = nullptr;
    Waterpore* theWaterpore, *thePrevWaterpore;
    MinNitro* theMinNitro, *thePrevMinNitro;
    Matrix* theMatrix, *thePrevMatrix;

    foreach(actNode, theNodes){


        if(actNode->getDepth()==0){
            actNode->nitrate_in=0;

        }
        else{
            theWaterpore=static_cast<Waterpore*>(actNode->getComponent("Waterpore"));
            theMinNitro=static_cast<MinNitro*>(actNode->getComponent("MinNitro"));
            theMatrix=static_cast<Matrix*>(actNode->getComponent("Matrix"));

            thePrevMinNitro=static_cast<MinNitro*>(prevNode->getComponent("MinNitro"));
            thePrevWaterpore=static_cast<Waterpore*>(prevNode->getComponent("Waterpore"));
            thePrevMatrix=static_cast<Matrix*>(prevNode->getComponent("Matrix"));


            double flow=theWaterpore->theSubNodes.at(0)->topcur(timeStep);
            double wcontent_prev=thePrevWaterpore->watercontent();
            double wcontent=theWaterpore->watercontent();
            double NO3_cur=theMinNitro->getNO3()+theMinNitro->getNO3t();
            double NO3_cur_prev=thePrevMinNitro->getNO3()+thePrevMinNitro->getNO3t();

            double NO3_thre=5e-7*(theMatrix->getMass()/theMatrix->getVolume());
            double NO3_thre_prev=5e-7*(thePrevMatrix->getMass()/thePrevMatrix->getVolume());

            NO3_cur=(NO3_cur>NO3_thre)?NO3_cur-NO3_thre:0;
            NO3_cur_prev=(NO3_cur_prev>NO3_thre_prev)?NO3_cur_prev-NO3_thre_prev:0;

            if(flow>=0){
                actNode->nitrate_in+=(NO3_cur_prev/wcontent_prev)*flow;
                prevNode->nitrate_out+=(NO3_cur_prev/wcontent_prev)*flow;

            }
            else{
                prevNode->nitrate_in-=(NO3_cur/wcontent)*flow;
                actNode->nitrate_out-=(NO3_cur/wcontent)*flow;
            }

        }
        prevNode=actNode;
    }

}

double SoilProfile::getPlantPsi(double sow_depth){
    SoilNode *actNode;
    Waterpore* theWaterpore;

    foreach(actNode, theNodes){
        if((actNode->getDepth()<sow_depth)&&((actNode->getDepth()+actNode->getVolume())>=sow_depth)){
            theWaterpore=static_cast<Waterpore*>(actNode->getComponent("Waterpore"));
            return theWaterpore->psi();
        }
    }
    return(0);
}

double SoilProfile::getPlantTemp(double sow_depth){
    SoilNode *actNode;
    Waterpore* theWaterpore;

    foreach(actNode, theNodes){
        if((actNode->getDepth()<sow_depth)&&((actNode->getDepth()+actNode->getVolume())>=sow_depth)){
            theWaterpore=static_cast<Waterpore*>(actNode->getComponent("Waterpore"));
            return theWaterpore->temperature();
        }
    }
    return(0);
}



void SoilProfile::doSeed(double theSeed)
{
    int i;
    double seeed = static_cast<bodiumrunner*>(parent())->thePlantStand.seedDepth();
    for(i=1;theNodes.at(i)->depth< seeed;i++);
    theNodes.at(i-1)->getComponent("Root")->addUpdate(theSeed);

}

void SoilProfile::organic_fert(double manure,double fert_depth){
    FomPool *actFomPool;
    Fom *theFOM;
    int i;
    for(i=1;theNodes.at(i)->depth< fert_depth;i++);
    theFOM=static_cast<Fom*>(theNodes.at(i-1)->getComponent("FOM"));

    foreach(actFomPool, theFOM->theFomPools){
        if(actFomPool->getName()== "Fom_Manure"){
            actFomPool->addUpdate(manure);
            actFomPool->doUpdate();
        }
    }

}

void SoilProfile::straw_fert(double straw, double fert_depth, double cn){
    FomPool *actFomPool;
    Fom *theFOM;
    int i;
    for(i=1;theNodes.at(i)->depth< fert_depth;i++);
    theFOM=static_cast<Fom*>(theNodes.at(i-1)->getComponent("FOM"));

    foreach(actFomPool, theFOM->theFomPools){
        if(actFomPool->getName()== "Fom_Roots"){
            actFomPool->addUpdate(straw);
            actFomPool->setCNratio(cn,straw);
            actFomPool->doUpdate();
        }
    }
}

void SoilProfile::fertilize(double newNO3, double newNH4, double fert_depth){
    MinNitro *theMinNitro;
    int i;
    for(i=1;theNodes.at(i)->depth< fert_depth;i++);
    theMinNitro=static_cast<MinNitro*>(theNodes.at(i-1)->getComponent("MinNitro"));
    theMinNitro->setNH4_solid(newNH4);
    theMinNitro->setNO3_solid(newNO3);

}

void SoilProfile::init()
{


    theOutlet = new WaterCell(theWNodes.last());
    if(groundwater)
        theOutlet->init(1.7E208, 1e-3,1,0,0,1e6);
    else
        theOutlet->init(1.7E208, 1e-3,1,-1e10,-1e-10, 0);
    theOutlet->setName("Outlet");
    theOutlet->setBoundary(true);
    theWNodes.last()->cells.append(theOutlet);

    wsnode *actNode, *oldNode=nullptr;
    bool isFirst = true;
    foreach(actNode, theWNodes){
        actNode->addInternalCond(theConducts);
        if(isFirst)
            isFirst = false;
        else
            actNode->addexternalCond(oldNode, theConducts);
        oldNode = actNode;
    }

    boundary = new wsnode(this);
    boundary->initBoundary();

    rainy = new WaterCell(boundary);
    evapy = new WaterCell(boundary);
    boundary->cells.append(rainy);
    boundary->cells.append(evapy);
    rainy->init(1000, 1e-3, 1, 100);
    evapy->init(0.0, 1e-12, 1, -1e8);
    rainy->setName("Rain");
    evapy->setName("Evi");
    actNode = theWNodes.first();


    conductor *newCond =new conductor(actNode);
    newCond->source = actNode->matrixCell;
    newCond->target = evapy;
    newCond->setPotTarget(evapy->psi_d());
    newCond->setDistSource(actNode->nodeThick()/2);
    newCond->setDistTarget(0.1);
    newCond->setCondTarget(1e-12);
    newCond->setKsat_target(1e-12);
    newCond->setCondSource(actNode->conduct());
    newCond->setGradientZ(9810);
    newCond->setTempCondTarget(10);
    connect(boundary, SIGNAL(tempChanged(double)), newCond, SLOT(setTempTarget(double)));
    connect(actNode, SIGNAL(tempChanged(double)), newCond, SLOT(setTempSource(double)));
    connect(actNode, SIGNAL(tempConductChanged(double)),newCond, SLOT(setTempCondSource(double)));
    connect(this,SIGNAL(stepTime(double)),newCond, SLOT(conductEnergy(double)));


    theConducts.append(newCond);
    connect(actNode->matrixCell,SIGNAL(psiChanged(double)),newCond,SLOT(setPotSource(double)));
    connect(actNode, SIGNAL(conductChanged(double)), newCond, SLOT(setCondSource(double)));
    connect(evapy, SIGNAL(horConductivityChanged(double)), newCond,SLOT(setCondTarget(double)));

    actNode->addSourceCond(newCond);
    actNode->setTopout(newCond);
    newCond =new conductor(actNode);
    newCond->target = actNode->matrixCell;
    newCond->source = rainy;
    newCond->setPotSource(rainy->psi_s());
    newCond->setDistSource(0.001);
    newCond->setDistTarget(actNode->nodeThick()/2);
    newCond->setCondSource(1e-2);
    newCond->setCondTarget(actNode->conduct());
    newCond->setKsat_target(actNode->ksat());
    newCond->setGradientZ(-9.81e3); // Pa/m
    newCond->setTempCondSource(10);
    theConducts.append(newCond);
    actNode->addTargetCond(newCond);
    actNode->setTopin(newCond);

    connect(actNode->matrixCell,SIGNAL(psiChanged(double)),newCond,SLOT(setPotTarget(double)));
    connect(actNode, SIGNAL(conductChanged(double)), newCond, SLOT(setCondTarget(double)));
    connect(rainy, SIGNAL(horConductivityChanged(double)), newCond,SLOT(setCondSource(double)));
    connect(boundary, SIGNAL(tempChanged(double)), newCond, SLOT(setTempSource(double)));
    connect(actNode, SIGNAL(tempChanged(double)), newCond, SLOT(setTempTarget(double)));
    connect(actNode, SIGNAL(tempConductChanged(double)),newCond, SLOT(setTempCondTarget(double)));
    connect(this,SIGNAL(stepTime(double)),newCond, SLOT(conductEnergy(double)));


    for(QList<conductor*>::iterator actCond = theConducts.begin(); actCond != theConducts.end(); actCond++){

        (*actCond)->checkEnds();

    }
    foreach(actNode, theWNodes){
        connect(actNode, SIGNAL(psiChanged(double)),this, SLOT(matrixChanges(double)));
        actNode->conduct();
    }
}

void SoilProfile::resetChangeRates()
{
    wsnode* actNode;
    WaterCell* theCell;
    conductor* theCond;
    foreach(actNode, theWNodes){
        foreach(theCell, actNode->cells){
           theCell->setZeroChange();
        }
        actNode->matrixCell->setZeroChange();
    }
    evapy->setZeroChange();
    rainy->setZeroChange();
}

struct Wrapper{
    void operator()(wsnode *x)
    { x->findZero();}
};
#define maxErr 1e-10 //error of water content change per soil node

void SoilProfile::doTimeStep(double timestep)
{
    double trans_pot=static_cast<bodiumrunner*>(parent())->thePlantStand.l_trans_pot;
    setH2(trans_pot);
    setDepthLoad();
    if(static_cast<bodiumrunner*>(parent())->thePlantStand.getDevstage()>0 && static_cast<bodiumrunner*>(parent())->thePlantStand.getDevstage()<3){
        setRootFrac();
        calcWuptake(trans_pot);
        calcNuptake(timestep);
    }

    double fixed_timestep=timestep;

    double maxTime=timestep;
    wsnode* actNode;
    WaterCell* theCell;
    conductor* theCond;
    foreach(theCond, theConducts){
        theCond->checkSwitch();
    }
    foreach(actNode, theWNodes){
            actNode->topzero();
        }

    while((maxTime = timestep)){

        //Wrapper w;
        do{
            matrixChanged = false;
            QList<wsnode*>::iterator iter;
            for(iter = theWNodes.begin();iter != theWNodes.end();iter++){
                (*iter)->findZero();
            }
            QList<wsnode*>::reverse_iterator riter;
            for(riter = theWNodes.rbegin();riter != theWNodes.rend();riter++){
                (*riter)->findZero();
            }

        }while(matrixChanged);

        foreach(actNode, theWNodes){
            foreach(theCell, actNode->cells){
                double actMax = theCell->linearTime();
                maxTime = actMax<maxTime?actMax:maxTime;
                if(maxTime <= minTimeStep)
                    break;
            }
            actNode->tempConduct();
        }
        double actMax = evapy->linearTime();
        maxTime = actMax<maxTime?actMax:maxTime;
        actMax = rainy->linearTime();
        maxTime = actMax<maxTime?actMax:maxTime;
        foreach(actNode, theWNodes){
            foreach(theCell, actNode->cells){
                theCell->doTimeStep(maxTime);
            }
            actNode->matrixCell->doTimeStep(maxTime);
        }


        foreach(actNode, theWNodes){
                actNode->topbal(maxTime);

            }

        evapy->doTimeStep(maxTime);
        rainy->doTimeStep(maxTime);
        emit stepTime(maxTime);

        calcNtransport(maxTime);
        actualTime += maxTime;
        if(waterOut){
            outWaterStream<<getVals().toUtf8().constData()<<"\n\n";
        }

        timestep -= maxTime;
    }



}


QString SoilProfile::getVals()
{
    wsnode* actNode;
    double massBal;
    QString ret(""), loc;
    loc = "%1: %2, %3, %4, %5, %6, %7\n";
    massBal =0;
    ret.append(loc.arg("#Time").arg(actualTime));
    foreach(actNode, theWNodes){

        ret.append(loc.arg(actNode->name()).arg(actualTime).arg(actNode->theta()).arg(actNode->psi()).arg(actNode->conduct()).arg(actNode->topval()).arg(actNode->temperature()));
        massBal += actNode->theta()* actNode->nodeThick();
    }
    ret.append(loc.arg("#Boundary").arg(rainy->theta()).arg(evapy->capacity()).arg(leachate()));
    ret.append(loc.arg("#BoundaryResidual").arg(residualRain).arg(residualEva));
    ret.append(loc.arg("#Mass balance").arg(massBal*1000));
    return ret;
}

double SoilProfile::getChangeSum()
{
    wsnode* actNode;
    WaterCell* theCell;
    double retval = 0;
    foreach(actNode, theWNodes){
        foreach(theCell, actNode->cells){
            retval += theCell->deltaTheta();
        }
    }
    return retval;
}

void SoilProfile::setEvaRain(double rain, double evapo, double ttemp)
{
    residualRain = rainy->theta();
    residualEva = evapy->capacity();
    rainy->setTheta(rain);
    evapy->setCapacity(evapo);
    evapy->setTheta(0);
    boundary->setTemperature(ttemp);
}

void SoilProfile::setEva(double theEva)
{
    residualEva = evapy->capacity();
    evapy->setCapacity(theEva);
    evapy->setTheta(0);

}
void SoilProfile::setTemp(double theTemp)
{
    boundary->setTemperature(theTemp);

}
void SoilProfile::setRain(double theRain)
{
    residualRain = rainy->theta();
    rainy->setTheta(theRain);

}
void SoilProfile::StructChange(const QJsonObject &theInit)
{
    wsnode* actNode;
    foreach(actNode, theWNodes){
        if(actNode->name() == theInit["Horizon"].toString()){
            actNode->addMacro(theInit["Macro+"].toDouble());
        }
    }

}

double SoilProfile::soilAlbedo()
{
    return theWNodes.at(0)->psi()>(-10000)?theSoilWetAlbedo:theSoilWetAlbedo*2;
}
double SoilProfile::leachate()
{
    return theOutlet->theta();
}

void SoilProfile::doOutput(){
    SoilNode *actNode, *nextNode;
    Component *actComponent;

    //int b=0;
    int b=1;        //ignore first node
    QList<double> sum_mass;
    double sum_totalmass=0;
    bool check_depth=true;

    for(int l=0;l<theNodes.at(0)->theComponents.size();l++){
        sum_mass.append(0);
    }

    //sumOut is set within config and allows to sum components over a specific depth
    //if the value set by the user is larger than the profile depth it is summed over the whole profile
    sumOut=(sumOut>theNodes.at(theNodes.size()-1)->getDepth())?theNodes.at(theNodes.size()-1)->getDepth()+theNodes.at(theNodes.size()-1)->getVolume():sumOut;

    //getMass returns values of kg / m² and needs thus to be divided with the thickness of the specific node
    //rel controls if the whole node is used for the summary or if it needs to be splitted because the node reaches deeper than the wished value
    while(check_depth){
        actNode=theNodes.at(b);
        double diff=sumOut-actNode->getDepth();
        double rel=(diff>actNode->getVolume())?1:diff/actNode->getVolume();

        int j=0;
        foreach(actComponent,actNode->theComponents){
            sum_mass[j]=sum_mass.at(j)+actComponent->getMass()*rel;
            j++;
        }

        sum_totalmass+=actNode->getMass()*rel;
        b++;
        check_depth=((rel<1)||b>theNodes.size()-1)?false:true;
    }
    //Extra loop for AWC
    check_depth = true;
    b=1;
    double awc = 0;
    double awcDepth=1;
    while(check_depth){
        actNode=theNodes.at(b);
        double diff=awcDepth-actNode->getDepth();
        double rel=(diff>actNode->getVolume())?1:diff/actNode->getVolume();
        awc+= ((Waterpore*)(actNode->getComponent("Waterpore")))->awc()*rel;


        b++;
        check_depth=((rel<1)||b>theNodes.size()-1)?false:true;
    }

    //filter calculates the microbial activity multiplied with the fraction of water that remains in the node as indicator for the filter function and sums it up over the whole profile or up to 2m depth
    double max_depth=(theNodes.at(theNodes.size()-1)->getDepth()<2)?theNodes.at(theNodes.size()-1)->getDepth():2;
    int j;
    double filter=0;
    for(j=1;(theNodes.at(j)->getDepth()+theNodes.at(j)->getVolume())< max_depth;j++);
    double diff;
    for(int a=1;a<=j-1;a++){
        actNode=theNodes.at(a);
        nextNode=theNodes.at(a+1);
//        double water_loss=(static_cast<Waterpore*>(nextNode->getComponent("Waterpore"))->topval()<=0)?0.01:static_cast<Waterpore*>(nextNode->getComponent("Waterpore"))->topval();
//        filter+=actNode->active_ratio*(actNode->getComponent("Waterpore")->getMass()/water_loss);
        diff = (actNode->getComponent("Waterpore")->getMass()-static_cast<Waterpore*>(nextNode->getComponent("Waterpore"))->topval());
        if(diff < 0)
            diff = 0;


        filter+=actNode->active_ratio*(diff/actNode->getComponent("Waterpore")->getMass());
    }


    for(int l=0;l<theNodes.at(0)->theComponents.size();l++){
        std::cout<<"\t"<<sum_mass.at(l)/sumOut;
    }

    std::cout<<"\t"<<sum_totalmass/sumOut;
    std::cout<<"\t"<<awc<<"\t"<<filter;

    double depth=0;
    int i;
    foreach(depth, out_depth){
        for(i=1;theNodes.at(i)->getDepth()< depth;i++);
        actNode=theNodes.at(i-1);

        std::cout<<"\t"<<depth;

        foreach(actComponent, actNode->theComponents){
            double mass_comp=actComponent->getMass()/actNode->getVolume();
            std::cout<<"\t"<<actComponent->getValue()<<"\t"<<actComponent->getVolume()<<"\t"<<mass_comp;
        }
        double mass_comp=actNode->getMass()/actNode->getVolume();
        std::cout<<"\t"<<mass_comp<<"\t"<<actNode->getVolume()<<"\t"<<actNode->getDepth()<<"\t"<<actNode->getLoad()<<"\t"<<static_cast<Waterpore*>(actNode->getComponent("Waterpore"))->temperature()<<"\t"<<static_cast<Waterpore*>(actNode->getComponent("Waterpore"))->topval()<<"\t"<<actNode->nitrate_out<<"\t"<<actNode->getCO2()<<"\t"<<static_cast<NAom*>(actNode->getComponent("NAOM"))->getCNratio()<<"\t"<<static_cast<Aom*>(actNode->getComponent("AOM"))->getCNratio()<<"\t"<<static_cast<Ssom*>(actNode->getComponent("SSOM"))->getCNratio()<<"\t"<<actNode->microbes_waterreduc(static_cast<Waterpore*>(actNode->getComponent("Waterpore"))->psi())<<"\t"<<static_cast<Microbes*>(actNode->getComponent("Microbes"))->getFBratio();
    }

    for(i=1;(theNodes.at(i)->getDepth()+theNodes.at(i)->getVolume())< 2.0;i++);
    actNode=theNodes.at(i-1);

    std::cout<<"\t"<<actNode->getDepth();

    foreach(actComponent, actNode->theComponents){
        double mass_comp=actComponent->getMass()/actNode->getVolume();
        std::cout<<"\t"<<actComponent->getValue()<<"\t"<<actComponent->getVolume()<<"\t"<<mass_comp;
    }
    double mass_comp=actNode->getMass()/actNode->getVolume();
    std::cout<<"\t"<<mass_comp<<"\t"<<actNode->getVolume()<<"\t"<<actNode->getDepth()<<"\t"<<actNode->getLoad()<<"\t"<<static_cast<Waterpore*>(actNode->getComponent("Waterpore"))->temperature()<<"\t"<<static_cast<Waterpore*>(actNode->getComponent("Waterpore"))->topval()<<"\t"<<actNode->nitrate_out<<"\t"<<actNode->getCO2()<<"\t"<<static_cast<NAom*>(actNode->getComponent("NAOM"))->getCNratio()<<"\t"<<static_cast<Aom*>(actNode->getComponent("AOM"))->getCNratio()<<"\t"<<static_cast<Ssom*>(actNode->getComponent("SSOM"))->getCNratio()<<"\t"<<actNode->microbes_waterreduc(static_cast<Waterpore*>(actNode->getComponent("Waterpore"))->psi())<<"\t"<<static_cast<Microbes*>(actNode->getComponent("Microbes"))->getFBratio();

    nout=actNode->getNitrateOut();
    foreach(actNode,theNodes) actNode->nitrate_out=0;

}

int SoilProfile::mixing(double depth)
{
    SoilNode *actNode;
    Component *actComponent;
    int b=1; // ignore first node
    QList<double> sum_val;
    bool check_depth=true;
    Fom *theFOM;
    theFOM=static_cast<Fom*>(theNodes.at(0)->getComponent("FOM"));

    int needed_length=theNodes.at(0)->theComponents.size()+theFOM->theFomPools.size()+1;

    for(int l=0;l<needed_length;l++){
        sum_val.append(0);
    }

    //calculate sum of nodes which should be mixed
    while(check_depth){
        actNode=theNodes.at(b);
        double diff=depth-actNode->getDepth();
        double rel=(diff>actNode->getVolume())?1:diff/actNode->getVolume();

        int j=0;
        foreach(actComponent,actNode->theComponents){
            if(actComponent->getTillMix()){
                if(actComponent->getName()== "FOM"){
                    FomPool *actFomPool;
                    theFOM=static_cast<Fom*>(actComponent);
                    foreach(actFomPool, theFOM->theFomPools){
                        sum_val[j]=sum_val.at(j)+actFomPool->getValue();
                        j++;
                    }
                }
                if(actComponent->getName()== "MinNitro"){
                    MinNitro *theMinNitro;
                    theMinNitro=static_cast<MinNitro*>(actComponent);
                    sum_val[j]=sum_val.at(j)+theMinNitro->getNH4();
                    j++;
                    sum_val[j]=sum_val.at(j)+theMinNitro->getNO3();
                    j++;
                }
                else{sum_val[j]=sum_val.at(j)+actComponent->getValue();j++;}
             }
            else j++;

        }

        b++;
        check_depth=((rel<1)||b>theNodes.size()-1)?false:true;
    }
    int c=1;
    while(c<b){
        actNode=theNodes.at(c);

        int j=0;
        foreach(actComponent,actNode->theComponents){
            if(actComponent->getTillMix()){
                if(actComponent->getName()== "FOM"){
                    FomPool *actFomPool;
                    theFOM=static_cast<Fom*>(actComponent);
                    foreach(actFomPool, theFOM->theFomPools){
                        actFomPool->setValue((sum_val.at(j)/(b-1)));
                        j++;
                    }
                }
                if(actComponent->getName()== "MinNitro"){
                    MinNitro *theMinNitro;
                    theMinNitro=static_cast<MinNitro*>(actComponent);
                    theMinNitro->setNH4((sum_val.at(j)/(b-1)));
                    j++;
                    theMinNitro->setNO3((sum_val.at(j)/(b-1)));
                    j++;
                }
                else{actComponent->setValue((sum_val.at(j)/(b-1)));j++;}
             }
            else j++;
        }
        c++;
    }

    return b;
}


void SoilProfile::tillage(double depth)
{
    SoilNode *actNode;
    int b=mixing(depth);

    int c=1; // ignore first node
    while(c<b){
        actNode=theNodes.at(c);
        actNode->tilleffect();
        c++;
    }
}
