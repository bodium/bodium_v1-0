#ifndef wsnode_H
#define wsnode_H

#include <QObject>
#include <QJsonObject>
#include <QList>
#include <QString>
#include <QMap>
/*#include "watercell.h"*/
/*#include "conductor.h"*/

class WaterCell;
class conductor;
/***************************************************//**
 * @brief The wsnode class
 * 
 * Wsnode is the container for pores and their water and air interchange.
 * 
 * Structure dynamics: there are two types of water pores: biopores and general pores. Biopores are either formed by root decay (with a diameter of 0.2 mm) 
 * or by earthworms (anecic activity). General pores are formed by surfaces, packings and swell/shrinkage etc.. They are initialised according to the initial 
 * pore model, e.g. Mualem/vanGenuchten. All pores can be destroyed by load, tillage, root growth (plugging), earthworm activity. This 
 ******************************************************/
class wsnode : public QObject
{
        Q_OBJECT
public:
    /** Standard constructor, parent should be Soil profile */
    wsnode(QObject *parent = nullptr);
    /** Constructor with integrated initialisation */
    wsnode(const QJsonObject &init, QObject *parent = nullptr);
    void init(const QJsonObject &init);
    void initBoundary(void);
    QList<WaterCell*> cells;
    WaterCell* matrixCell;
    void addInternalCond(QList<conductor*> &condList);
    double nodeThick(void){return nodeThickness;}
    void setNodeThick(double thickness){nodeThickness=thickness;}
    void addexternalCond(wsnode *upper, QList<conductor*> &condList);
    /**
     * @brief return the dimensionless water content of a soil node (Vol/vol)
     * @return Water content \f$[-]\f$
     */
    double theta(void);
    /**
     * @brief psi Matric potential
     * @return Matric potential \f$[\si{\Pa}]\f$
     */
    double psi(void);
    double watercontent(void);
    double poreDensity(void);
    double icecontent(void);
    /*******************************//**
    *@brief wsnode::heatCapacity
    *
    * @return heat capacity of node in joule/Kelvin
    *
    * **********************************/
    double heatCapacity(void);


    /*******************************//**
    * @brief wsnode::tempConduct
    *
    * @return thermal energy conductivity \f$[\si{\joule\per\second\per\meter\per\kelvin}]\f$
    * Heat transport is goverend by diffusion and convection. For the diffusive transport the
    * apparent thermal conductivity of the soil is calculated according to \cite Peng+2016 \cite Hansson+2004 (attention for units!).
    * \f[ \lambda = C_1 + C2(\theta_w + F\theta_i)- (C_1-C_4) \exp[C_3(\theta_w + F\theta_i)^{C_5}] \f]
    * \f[
    * F = 1+ F_1\theta_i^{F_2}
    * \f]
    * Typical values:
    * \f$C_1(\si{\joule\per\second\per\meter\per\kelvin})\f$|\f$C_2(\si{\joule\per\second\per\meter\per\kelvin})\f$|\f$C_3\f$|\f$C_4(\si{\joule\per\second\per\meter\per\kelvin})\f$|\f$C_5\f$|\f$F_1\f$|\f$F_2\f$
    * ---------------|---------------|---------------|---------------|---------------|---------------|---------------
    * 0.65|1.77|2.97|0.25|4|12.36|1.06
    * 0.55|0.8|3.77|0.13|4|13.05|1.06
    *
    */
    double tempConduct(void);
    /************************//**
     * @brief wsnode::conduct Matrix conductivity of the soil node
     *
     * The matrix conductivity describes the ease of water movement in a soil node. It acts both on the
     * water movement through the node and within the node. At the moment it is realized as a
     * capillary bundle model summing up the conductivities of the different capacity \link WaterCell \endlink.
     * @return conducticvity (\f$\frac{j_wdx}{d\psi}\f$)
     */
    double conduct(void);
    double horConduct(void);
    QString name(){return horName;}
    double ksat(void){return lKsat;}
    /************************************/ /**
     * @brief Set the potential of the node
     *
     * In order to ascertain the mass balance the potential is determined.
     * <A HREF="https://ganymed.math.uni-heidelberg.de/~lehre/SS12/numerik0/2-nullstellen.pdf">Nullstellensuche</A>
     *
     * Definition 2.24
     * Sekantenverfahren, stabilisiert:
     *
     * Given a steady function on \f$ [a_0, b_0] := [a,b] \f$ and \f$ f(a)f(b) < 0\f$.
     *
     * The latter is achieved only after an initial guess in our implementation.
     * Approximation of \f$ f(x_{n+1}) = 0 \f$ is done by \f[
     * x_{n+1} := a_n - \frac{f(a_n)(b_n-a_n)}{f(b_n) - f(a_n)}
     * \f]
     *
     * Next interval is defined by \f[
     * [a_{n+1}, b_{n+1}] = \left\{\begin{array}{ll} [a_n, x_{n+1}]& \mathrm{if } f(a_n)f(x_{n+1} < 0 \\
     * {}[x_{n+1}, b_n]&\mathrm{if } f(x_{n+1}f(b_n) < 0 \\
     * {}[x_{n+1}, x_{n+1}] &\mathrm{if } f(x_{n+1}) = 0\end{array} \right.
     * \f]
     *
     ***********************************************************************/
    void findZero();
    void addSourceCond(conductor* condi);
    void addTargetCond(conductor* condi);
    void setTopin(conductor* condi){topin = condi;}
    void setTopout(conductor* condi){topout = condi;}
    double topval(void);
    double topcur(double timestep);
    void addMacro(double macro);
    double temperature(void){return lTemp;}
    void setTemperature(double ttemp){lTemp = ttemp;emit tempChanged(lTemp);}
    void addHeat(double energy);
    double getMacropores(double boundary=1e-4);
    void topzero(void){topsum=0;}
    void topbal(double timestep);
    /************************************/ /**
     * @brief return the water capacity below a potential
     ******************************************************/
    double waterCapa(double potential);

    /************************************/ /**
     * @brief return the available water capacity
     ******************************************************/
    double awc(){return waterCapa(-6310)-waterCapa(-15e5);}

signals:
    void psiChanged(double psi);
    void tempChanged(double temp);
    void conductChanged(double conductor);
    void tempConductChanged(double tempCond);
    void horConductChanged(double conductor);
    void stressOccurred(double stress);

public slots:
    void evalDTheta(void);
    void setRootUp(double rootup_l);
    void stressMe(double stress){emit stressOccurred(stress);}

private:
    int numCells;
    bool isBoundary =false;
    double innerDist, nodeThickness, oldConduct, oldHorConduct, oldLambda, lTemp=8.5, lMass=0, lKsat;
    QString horName;
    QList<conductor*> condSourceList;
    QList<conductor*> condTargetList;
    conductor *topin, *topout;
    double topsum;
    double rootUp=0;


};

#endif // wsnode_H
