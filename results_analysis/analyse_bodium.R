library_list<-c('ggplot2','viridis','tidyft','dplyr','tidyr','scales','data.table')

#check if needed packages are installed and load them
ipak <- function(pkg){
  new.pkg <- pkg[!(pkg %in% installed.packages()[, "Package"])]
  if (length(new.pkg)) 
    install.packages(new.pkg, dependencies = TRUE)
  sapply(pkg, require, character.only = TRUE)
}

ipak(library_list)


numextract <- function(colname,datalist){
  str_extract(sapply(strsplit(datalist, colname), "[", 2), "\\d+\\.\\d*e-\\d*|\\d+[\\.e-]*\\d*")
}

#####USER INPUT NEEDED#########

####varied parameters for specific simulations, applied year for specific simulations
varied_par<-c("till","V120")

###set wording directory of results data
setwd("~/Bodium_v1-0/results")

###############################

alldata<-list.files(getwd())

cols<-c("dat_time","Plant_biomass","Plant_biomass_SO","Plant_nitrogen","Waterstress","Dev_stage","Nout","AWC","filter")
cols_node<-c("AOM_mass","NAOM_mass","SSOM_mass","AOM_cn","NAOM_cn","SSOM_cn","Microbes_mass","SleepMicrobes_mass","MinNitro_mass","total_mass","Matrix_mass","FB_ratio")

check_data<-fread(alldata[1],header=T,check.names = T,nrows = 3)

cur_select<-check_data %>%select(starts_with("out_depth"))%>%gather("Node", "Depth")
depth_vec<-unique(cur_select$Depth)

##############USER INPUT POSSIBLE: Here, you can change the depth over which you want to have the values summed, currently 20 cm
depth_length<-length(depth_vec[depth_vec<=0.2])

if(depth_length>1){
  for(i in 1:(depth_length-1)){
    cols_node<-c(cols_node,sapply(cols_node,function(x){return(paste(x,".",i,sep=""))}))
  }
}

cols_all<-c(cols,cols_node)
mycols<-which(colnames(check_data)%in%cols_all )

data_list<-lapply(alldata,fread,select=mycols,header=T,check.names=T)

print(paste("Read ",length(alldata)," data files"))

datal<-dim(data_list[[1]])[1]

####set year

data_list <- lapply(data_list, function(x){
  x$year<-format(as.Date(x$dat_time,"%d.%m.%Y"),"%Y");
  x$dat_time<-format(as.Date(x$dat_time,"%d.%m.%Y"));
  x$season<-ifelse(x$Dev_stage<3,1,0);
  return(x);
})

cur_dat<-data_list[[1]]
year<-cur_dat$year

print("calculate yield")

####calculate yield
cur_data<-data_list[[1]]
yield<-setDT(cur_data[cur_data$season==1,]) %>% group_by(year) %>% summarise(yield=max(Plant_biomass),yield_SO=max(Plant_biomass_SO),nout=sum(Nout),waterstress=sum(1-Waterstress))

yield$pnitro<-NA
for(j in 1:length(yield$year)){
  yield$pnitro[j]<-cur_data$Plant_nitrogen[last(which(cur_data$Plant_biomass==yield$yield[j]))]
}

for(p in 1:length(varied_par)){
  yield[[varied_par[p]]]<-numextract(varied_par[p],alldata[1])
}

for(i in 2:length(alldata)){
  cur_data<-data_list[[i]]
  yield1<-setDT(cur_data[cur_data$season==1,]) %>% group_by(year) %>% summarise(yield=max(Plant_biomass),yield_SO=max(Plant_biomass_SO),nout=sum(Nout),waterstress=sum(1-Waterstress))
  
  yield1$pnitro<-NA
  for(j in 1:length(yield1$year)){
      yield1$pnitro[j]<-cur_data$Plant_nitrogen[last(which(cur_data$Plant_biomass==yield1$yield[j]))]
   }

  for(p in 1:length(varied_par)){
      yield1[[varied_par[p]]]<-numextract(varied_par[p],alldata[i])
  }
  yield<-rbind(yield,yield1)
  
}  

print("yield calculated")

write.table(yield,"yield_df.csv",col.names=TRUE,row.names = FALSE,quote=FALSE)

print("calculate node based data")
###depth analysis

calc_val<-c("AOM_mass","NAOM_mass","SSOM_mass")
calc_cn<-c("AOM_cn","NAOM_cn","SSOM_cn")

sum_vec<-c("Microbes_mass","SleepMicrobes_mass","MinNitro_mass","total_mass","AOM_mass","SSOM_mass","NAOM_mass","Matrix_mass")

for (i in 1:length(data_list)){
  for(j in 1:length(varied_par)){
    data_list[[i]][[varied_par[j]]]<-numextract(varied_par[j],alldata[i])
  }
}


data_4nut <- lapply(data_list, function(x){
  b<-x[,1]
  
  for(j in 1:length(varied_par)){
    b[[varied_par[j]]]<-x[[varied_par[j]]]
  }

  sub_fb<-x%>%select(starts_with("FB_ratio"))
  
  cnf<-ifelse(("cn_f" %in% colnames(x)),as.numeric(x$cn_f[1]),14)

  sub_mic_cn<-(sub_fb*cnf+6)/(sub_fb+1)
  
  sub_mic_tot<-x%>%select(starts_with("Microbes_mass"))+x%>%select(starts_with("SleepMicrobes_mass"))
  
  b$Calc_Mic_CN<-rowSums(sub_mic_cn*sub_mic_tot)
  
  for(k in 1:length(calc_val)){
    sub_val<-x%>%select(starts_with(calc_val[k]))
    sub_cn<-x%>%select(starts_with(calc_cn[k]))
    new_colname<-paste("Calc",calc_val[k],sep="_")
    b[[new_colname]]<-rowSums(sub_cn*sub_val)
    
  }
  
  for(i in 1:length(sum_vec)){
    new_colname<-paste(sum_vec[i],"sum",sep="_")
    b[[new_colname]]<-rowSums((x %>% select(starts_with(sum_vec[i]))))
  }
  
  b$AWC<-x$AWC
  b$filter<-x$filter
  return(b)
})

  
nut_df<-do.call(rbind.data.frame, data_4nut)

nut_df$corg<-nut_df$SSOM_mass_sum+nut_df$AOM_mass_sum+nut_df$NAOM_mass_sum+nut_df$Microbes_mass_sum+nut_df$SleepMicrobes_mass_sum
nut_df$corg_rel<-nut_df$corg/nut_df$Matrix_mass_sum

nut_df$AOM_CN<-nut_df$Calc_AOM_mass/nut_df$AOM_mass_sum
nut_df$SSOM_CN<-nut_df$Calc_SSOM_mass/nut_df$SSOM_mass_sum
nut_df$NAOM_CN<-nut_df$Calc_NAOM_mass/nut_df$NAOM_mass_sum
nut_df$Mic_CN<-nut_df$Calc_Mic_CN/(nut_df$Microbes_mass_sum+nut_df$SleepMicrobes_mass_sum)

nut_df$Soil_CN<-(nut_df$Calc_AOM_mass+nut_df$Calc_NAOM_mass+nut_df$Calc_SSOM_mass+nut_df$Calc_Mic_CN)/(nut_df$AOM_mass_sum+nut_df$NAOM_mass_sum+nut_df$SSOM_mass_sum+nut_df$Microbes_mass_sum+nut_df$SleepMicrobes_mass_sum)

nut_df$nitro_total<-(nut_df$corg_rel/nut_df$Soil_CN)+(nut_df$MinNitro_mass_sum/nut_df$Matrix_mass_sum)

nut_df$cn_mic<-round<-ifelse(("cn_f" %in% colnames(nut_df)),round((as.numeric(nut_df$cn_f)+as.numeric(nut_df$cn_f)+6)/3),round((14+14+6)/3))

write.table(nut_df,"nut_df.csv",col.names=TRUE,row.names = FALSE,quote=FALSE)

