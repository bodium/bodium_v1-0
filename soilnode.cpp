#include "soilnode.h"
#include "soilprofile.h"
#include "bodiumrunner.h"
#include "timex.h"
#include "plant_stand.h"

#include <math.h>
#include <iostream>
#include <QJsonObject>
#include <QJsonArray>
#include <QString>
#include <QtMath>
#include <QDateTime>

SoilNode::SoilNode(QObject *parent,SoilNode *topNo) : QObject(parent)
{
    topNode = topNo;
    theComponents.append(static_cast<Component*>(new Matrix(this)));
    theComponents.append(static_cast<Component*>(new Root(this)));
    theComponents.append(static_cast<Component*>(new Macropore(this)));
    theComponents.append(static_cast<Component*>(new BioX(this)));
    theComponents.append(static_cast<Component*>(new RootDemand(this)));
    theComponents.append(static_cast<Component*>(new Fom(this)));
    theComponents.append(static_cast<Component*>(new Aom(this)));
    theComponents.append(static_cast<Component*>(new Ssom(this)));
    theComponents.append(static_cast<Component*>(new MinNitro(this)));
    theComponents.append(static_cast<Component*>(new Waterpore(this)));
    theComponents.append(static_cast<Component*>(new Microbes(this)));
    theComponents.append(static_cast<Component*>(new SleepMicrobes(this)));
    theComponents.append(static_cast<Component*>(new NAom(this)));
    theComponents.append(static_cast<Component*>(new Oxygen(this)));


    static_cast<Matrix*>(getComponent("Matrix"))->setMass(150);
    static_cast<Matrix*>(getComponent("Matrix"))->setVolume(0.09);
    static_cast<Matrix*>(getComponent("Matrix"))->setValue(0);

    getComponent("Root")->setValue(0.0);
    getComponent("Macropore")->setValue(0.01);
    static_cast<Macropore*>(getComponent("Macropore"))->setVolume(0.01);
    getComponent("BioX")->setValue(0.0);
    getComponent("RootDemand")->setValue(0.0);
    getComponent("FOM")->setValue(0);

    //AOM, SSOM, and Microbes in kg kg-1 as relative value, set in next step absolute via matrix mass

    getComponent("AOM")->setValue(6e-4);
    getComponent("SSOM")->setValue(1e-3);
    getComponent("MinNitro")->setValue(0);
    getComponent("Microbes")->setValue(2e-4);
    getComponent("SleepMicrobes")->setValue(2e-4);
    getComponent("NAOM")->setValue(1e-3);

    theProcesses.append(static_cast<Process*>(new RootGrowth(this)));
    theProcesses.append(static_cast<Process*>(new Feed_Breed(this)));
    theProcesses.append(static_cast<Process*>(new Som_Turn(this)));
    theProcesses.append(static_cast<Process*>(new Nitro_Cycle(this)));

    theProcesses.append(static_cast<Process*>(new oxyTransport(this)));
    theProcesses.append(static_cast<Process*>(new structCompaction(this)));

    if(static_cast<SoilProfile*>(parent)->explicit_microbes){
      theProcesses.append(static_cast<Process*>(new Microbe_Dyn(this)));

    }

}

SoilNode::SoilNode(const QJsonObject  &theConfig, QObject *parent, SoilNode *topno) : SoilNode(parent, topno)
{
    topNode =topno;
 if(!theConfig["Matrix_Weight"].isUndefined())
     static_cast<Matrix*>(getComponent("Matrix"))->setMass(theConfig["Matrix_Weight"].toDouble());
 if(!theConfig["Matrix_Volume"].isUndefined())
     static_cast<Matrix*>(getComponent("Matrix"))->setVolume(theConfig["Matrix_Volume"].toDouble());
 if(!theConfig["Macropore_Volume"].isUndefined())
     static_cast<Macropore*>(getComponent("Macropore"))->setVolume(theConfig["Macropore_Volume"].toDouble());
 if(!theConfig["BioX_Value"].isUndefined())
     getComponent("BioX")->setValue(theConfig["BioX_Value"].toDouble());
 if(!theConfig["Nitro_Value"].isUndefined())
      getComponent("MinNitro")->setValue(theConfig["Nitro_Value"].toDouble()*getComponent("Matrix")->getMass());


 getComponent("BioX")->setValue(0.001);

 if(!theConfig["Corg"].isUndefined()){
     double corg=theConfig["Corg"].toDouble();
     getComponent("SSOM")->setValue(corg*0.85);
     if(static_cast<SoilProfile*>(parent)->explicit_microbes){
         getComponent("NAOM")->setValue(corg*0.05);
         getComponent("AOM")->setValue(corg*0.05);
         getComponent("Microbes")->setValue(corg*0.01);
         getComponent("SleepMicrobes")->setValue(corg*0.04);
     }
     else{
         getComponent("AOM")->setValue(corg*0.15);
     }

 }

 if(!theConfig["CN"].isUndefined())cn_total=theConfig["CN"].toDouble();

 if(!theConfig["Oxygen"].isUndefined())
     getComponent("Oxygen")->configure(theConfig);

 getComponent("AOM")->setValue(getComponent("AOM")->getValue()*getComponent("Matrix")->getMass());
 getComponent("SSOM")->setValue(getComponent("SSOM")->getValue()*getComponent("Matrix")->getMass());

 getComponent("Microbes")->setValue(getComponent("Microbes")->getValue()*getComponent("Matrix")->getMass());
 getComponent("SleepMicrobes")->setValue(getComponent("SleepMicrobes")->getValue()*getComponent("Matrix")->getMass());
 getComponent("NAOM")->setValue(getComponent("NAOM")->getValue()*getComponent("Matrix")->getMass());

 getComponent("Waterpore")->configure(theConfig);

 connect(this, SIGNAL(waterup_changed(double)), static_cast<Waterpore*>(getComponent("Waterpore")),SLOT(rootUp(double)));

}

Component* SoilNode::getComponent(QString theName)
{
    Component *theMatch;
    foreach(theMatch, theComponents){
        if(theMatch->getName() == theName)
            return theMatch;
    }
    return nullptr;

}

void Component::merge(Component other)
{
    theValue += other.theValue;
    theUpdateValue += other.theUpdateValue;

}

Component *Component::split(double splitter)
{
    Component *newComp = new Component;
    newComp->theValue = theValue;
    newComp->theUpdateValue = theUpdateValue;
    return newComp;
}

Process* SoilNode::getProcess(QString theName)
{
    Process *theMatch;
    foreach(theMatch, theProcesses){
        if(theMatch->getName() == theName)
            return theMatch;
    }
    return nullptr;
}

void Root::doUpdate(){
    Component::doUpdate();
    if(getValue()<=0){
        setValue(0);
        setVolume(0);
    }
    else{
        double mean_dia=static_cast<bodiumrunner*>(parent()->parent()->parent())->thePlantStand.mean_root_dia;
        double mean_length_rts=static_cast<bodiumrunner*>(parent()->parent()->parent())->thePlantStand.mean_length;
        setVolume(M_PI*(mean_dia/2)*(mean_dia/2)*(getValue()*mean_length_rts));
    }
}

double Root::getMass(){
    double massi;
    double rootiVol=static_cast<SoilProfile*>(parent()->parent())->rootVol;
    massi=(rootiVol>0)?(getVolume()/rootiVol)*getVolume()*static_cast<bodiumrunner*>(parent()->parent()->parent())->thePlantStand.biomass_values[4]:0;
    return massi;
}
void Root::configure(const QJsonObject &theConfig)
{
    setVolume(0);
}


void Root::getSeed(double seedling)
{
    addVolume(seedling);
}

void Root::removeVol(double bio,double nitro){
    setVolume(0.0);
}

void Matrix::doUpdate(){
    Component::doUpdate();
    theVolume += updateVolume;
    updateVolume = 0;
    theMass += updateMass;
    updateMass = 0;
    if(getValue()<0)setValue(0);
    if(theVolume<0)theVolume = 0;
    if(theMass<0)theMass = 0;

}
void Macropore::doUpdate(){
    Component::doUpdate();
    theVolume += updateVolume;
    updateVolume = 0;
    if(getValue()<0)setValue(0);
    if(theVolume<0)theVolume = 0;



}

//each FomPool is configured with specific synthesis coefficient eta and rate coefficient k_fom
FomPool::FomPool(const QJsonObject  &theConfig, QObject *parent) : FomPool(parent)
{
    eta = theConfig["eta"].isUndefined()?eta:theConfig["eta"].toDouble();
    k_fom = theConfig["k_fom"].isUndefined()?k_fom:theConfig["k_fom"].toDouble();
    pool = theConfig["Name"].isUndefined()?pool:theConfig["Name"].toString();
    cn_ratio = theConfig["cn_ratio"].isUndefined()?cn_ratio:theConfig["cn_ratio"].toDouble();
    ini = theConfig["ini"].isUndefined()?ini:theConfig["ini"].toDouble();

    double node_mass=static_cast<SoilNode*>(parent->parent())->getComponent("Matrix")->getMass();
    setValue(ini*node_mass);

}

void Fom::configure(const QJsonObject &jsonNodes)
{
    theFomPools.clear();
    QJsonArray jsonArray = jsonNodes["FomPools"].toArray();
    foreach (const QJsonValue & value, jsonArray) {
        theFomPools.append(new FomPool(value.toObject(), this));
   }

    //to vary the cn-ratio of organic fertilizer while increase the amount at the same time - to fertilize the same amount of N!
    //this takes as a basis the amount of N in the 'original' config
    double cn_variation=static_cast<bodiumrunner*>(parent()->parent()->parent())->thePlantStand.cn_var;
    if(cn_variation>0){
        FomPool *actFomPool;
        foreach(actFomPool, theFomPools){
            if(actFomPool->getName()== "Fom_Manure"){
                double orgFert=static_cast<bodiumrunner*>(parent()->parent()->parent())->thePlantStand.fertilizeOrg;
                double orgFert_perN=orgFert/(actFomPool->cn_ratio+1);
                orgFert=orgFert_perN*(cn_variation+1);
                if(static_cast<bodiumrunner*>(parent()->parent()->parent())->var_orgfert) static_cast<bodiumrunner*>(parent()->parent()->parent())->thePlantStand.fertilizeOrg=orgFert;
                static_cast<bodiumrunner*>(parent()->parent()->parent())->var_orgfert=false;
                actFomPool->cn_ratio=cn_variation;
            }
        }
    }
    till_mix=true;
}
/**
 * @brief Updates by summing up values of all FomPool
 */

void Fom::doUpdate(){
    FomPool *actFomPool;
    double sumpool=0;
    foreach(actFomPool, theFomPools){
        actFomPool->doUpdate();
        sumpool +=actFomPool->getValue();
    }
    setValue(sumpool);
    Component::doUpdate();
    if(getValue()<0)setValue(0);
    setMass(getValue());
}

void Fom::root_harv(double root_bio,double newcn){

    double rootiVol=static_cast<SoilProfile*>(parent()->parent())->rootVol;
    double relVol=(rootiVol>0)?(static_cast<SoilNode*>(parent())->getComponent("Root")->getVolume()/rootiVol):0;
    FomPool *actFomPool;

    if(relVol>0){
        foreach(actFomPool, theFomPools){
            if(actFomPool->getName()== "Fom_Roots"){
                actFomPool->addUpdate(root_bio*relVol);
                actFomPool->setCNratio(newcn,root_bio*relVol);          //set cn ratio based on current nitrogen in roots
                actFomPool->doUpdate();
            }
        }
    }
    static_cast<SoilNode*>(parent())->getComponent("Root")->setValue(0);
}


void FomPool::doUpdate(){
    if(theUpdateValue>0){
        theUpdateValue=theUpdateValue;
    }
    theValue+=theUpdateValue; theUpdateValue = 0;
    if(getValue()<0)setValue(0);
}

void Aom::configure(const QJsonObject &theConfig)
{
    cn_ratio=static_cast<SoilNode*>(parent())->cn_total;
    till_mix=true;

}

void Aom::doUpdate(){
    Component::doUpdate();
    if(getValue()<0)setValue(0);
    value_temp=getValue();
    setCNratio_fin();
    setMass(getValue());
}

void NAom::doUpdate(){
    Component::doUpdate();
    if(getValue()<0)setValue(0);
    value_temp=getValue();
    setCNratio_fin();
    setMass(getValue());
}

void NAom::configure(const QJsonObject &theConfig)
{
    cn_ratio=static_cast<SoilNode*>(parent())->cn_total;
    till_mix=true;

}

void Ssom::doUpdate(){
    Component::doUpdate();
    if(getValue()<0)setValue(0);
    setMass(getValue());
}

void Ssom::configure(const QJsonObject &theConfig)
{
    cn_ratio=static_cast<SoilNode*>(parent())->cn_total;
    QJsonObject ssomInit = theConfig["SomTurn"].toObject();

    till_destabilize = ssomInit["till_destab"].isUndefined()?till_destabilize:ssomInit["till_destab"].toDouble();
    till_mix=true;

}

void BioX::doUpdate(){
    if(theUpdateValue != theUpdateValue) theUpdateValue=0.0;
    Component::doUpdate();
    if(getValue()<0)setValue(0);
    setMass(getValue());
}

void BioX::disturbance(double NO3_fert,double NH4_fert){
    if(dist_fert){
        addUpdate(-getValue()*dist_rate);
    }
}

void BioX::configure(const QJsonObject &theConfig){
    QJsonObject bioxInit = theConfig["FeedBreed"].toObject();
    dist_rate = bioxInit["dist_rate"].isUndefined()? dist_rate:bioxInit["dist_rate"].toDouble();
    dist_fert = bioxInit["dist_bool"].isUndefined()? dist_fert:bioxInit["dist_bool"].toBool();
    till_mix=true;

}

void MinNitro::configure(const QJsonObject &theConfig)
{
    setNH4(getValue()*0.5);
    setNO3(getValue()*0.5);
    setMass(getValue());
    till_mix=true;

}

void MinNitro::doUpdate(){
    setNH4(getNH4()+NH4_temp);
    setNO3(getNO3()+NO3_temp);
    if(getNH4()<0)setNH4(0);
    if(getNO3()<0)setNO3(0);
    setValue(NO3+NH4);
    Component::doUpdate();
    if(getValue()<0)setValue(0);
    setMass(getValue());
}

void Microbes::doUpdate(){
    Component::doUpdate();
    if(getValue()<0)setValue(0);
    setMass(getValue());
}

void Microbes::configure(const QJsonObject &theConfig)
{
    QJsonObject micInit = theConfig["MicrobeDyn"].toObject();
    till_death = micInit["till_d"].isUndefined()? till_death:micInit["till_d"].toDouble();
    till_inactive = micInit["till_inact"].isUndefined()? till_inactive:micInit["till_inact"].toDouble();
    till_mix=true;

}


void SleepMicrobes::doUpdate(){
    Component::doUpdate();
    if(getValue()<0)setValue(0);
    setMass(getValue());
}

void SleepMicrobes::configure(const QJsonObject &theConfig)
{
    till_mix=true;

}

void Waterpore::doUpdate(){
    setValue(theta());
    Component::doUpdate();
}

double SoilNode::getMass(){
    double theMass = 0;
    QList<Component*>::iterator i;
     for (i = theComponents.begin(); i != theComponents.end(); ++i)
         theMass += (*i)->getMass();
     return theMass;
}

double SoilNode::getVolume(){
    double theVol = 0;
    QList<Component*>::iterator i;
     for (i = theComponents.begin(); i != theComponents.end(); ++i)
         theVol += (*i)->getVolume();
     return theVol;
}

/**
 * @brief Time step implementation of simulation
 *
 * All active processes are evaluated and afterwards all active components updated
*/
void SoilNode::doTimeStep(double timeStep){
    Process *actProcess;
        foreach(actProcess, theProcesses){
        actProcess->evaluate(timeStep);
    }
}
void SoilNode::doUpdate()
{
    Component *actComponent;

    foreach(actComponent, theComponents){
        actComponent->doUpdate();
    }

    active_ratio=getComponent("Microbes")->getValue()/(getComponent("Microbes")->getValue()+getComponent("SleepMicrobes")->getValue());
}


void SoilNode::setWaterup(double newWaterup)
{
    water_up=newWaterup;
    emit waterup_changed(water_up/86400); //Attention, hard coded time step!
}

void SoilNode::setRTSdepth(bool rts_dp){
    rts_depth=rts_dp;
}

double SoilNode::microbes_waterreduc(double psi_m){
    double water_reduc=0;
    double h_high=-100;
    double h_mid1=-3162.278;
    double h_mid2=-31622.78;
   double h_low=-316227766;

    water_reduc=(psi_m>h_high)?0.6:(psi_m>h_mid1)?(0.6+0.4*log10(-psi_m/100)/1.5):(psi_m>h_mid2)?1:(psi_m>h_low)?(1.625-log10(-psi_m/100)/4):0;
    return water_reduc;
}

void SoilNode::tilleffect(){
    BioX *theBioX=static_cast<BioX*>(getComponent("BioX"));
    Aom *theAom=static_cast<Aom*>(getComponent("AOM"));
    Ssom *theSsom=static_cast<Ssom*>(getComponent("SSOM"));
    Oxygen *theOxygen=static_cast<Oxygen*>(getComponent("Oxygen"));
    Waterpore *theWaterpore=static_cast<Waterpore*>(getComponent("Waterpore"));


    theBioX->setValue(theBioX->getValue()*0.7);
    theOxygen->setValue(0.3);

    double destabilize=theSsom->getValue()*theSsom->till_destabilize;
    theSsom->setValue(theSsom->getValue()-destabilize);
    theWaterpore->stressMe(1);

    if(static_cast<SoilProfile*>(parent())->explicit_microbes){
         Microbes *theMicrobes=static_cast<Microbes*>(getComponent("Microbes"));
        SleepMicrobes *theSleepMicrobes=static_cast<SleepMicrobes*>(getComponent("SleepMicrobes"));
        Microbe_Dyn *theMicroDynamics=static_cast<Microbe_Dyn*>(getProcess("Microbe_Dyn"));
        NAom *theNAom=static_cast<NAom*>(getComponent("NAOM"));

        double necro=(theMicrobes->getValue()+theSleepMicrobes->getValue())*(theMicrobes->getFBratio()/(theMicrobes->getFBratio()+1))*theMicrobes->till_death; //assume only Fungi die, but how much?
        double inactive=theMicrobes->getValue()*(theMicrobes->getFBratio()/(theMicrobes->getFBratio()+1))*theMicrobes->till_inactive; //assume only Fungi go inactive, but how much?

        //if only fungi are dying/going inactive this would also change the FB!
        theMicrobes->setValue(theMicrobes->getValue()-inactive);
        theSleepMicrobes->setValue(theSleepMicrobes->getValue()+inactive-necro);

        if(theSsom->till_destabilize>0){
            theAom->setValue(theAom->getValue()+((necro+destabilize)*(1-theMicroDynamics->necro_am)));
            theNAom->setValue(theNAom->getValue()+((necro+destabilize)*theMicroDynamics->necro_am));

            double aom_cn=((theMicrobes->getCNratio()*(necro*(1-theMicroDynamics->necro_am)))+(theSsom->getCNratio()*(destabilize*(1-theMicroDynamics->necro_am))))/((necro+destabilize)*(1-theMicroDynamics->necro_am));
            double naom_cn=((theMicrobes->getCNratio()*(necro*theMicroDynamics->necro_am))+(theSsom->getCNratio()*(destabilize*theMicroDynamics->necro_am)))/((necro+destabilize)*theMicroDynamics->necro_am);

            theAom->setCNratio(aom_cn,(necro+destabilize)*(1-theMicroDynamics->necro_am));
            theNAom->setCNratio(naom_cn,(necro+destabilize)*theMicroDynamics->necro_am);

            theAom->setCNratio_fin();
            theNAom->setCNratio_fin();
        }

    }
    else{
        theAom->setValue(theAom->getValue()+destabilize);
    }


}

Process::Process(QObject *parent) : QObject(parent)
{
}

Component::Component(QObject *parent) : QObject(parent)
{
}


RootGrowth::RootGrowth(QObject *parent) :  Process(parent)
{
    Components.append(static_cast<SoilNode*>(parent)->getComponent("Macropore"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("Matrix"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("Root"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("RootDemand"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("FOM"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("MinNitro"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("Waterpore"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("AOM"));

    theRoot = (Root*)Components.at(2);
    theMatrix = (Matrix*)Components.at(1);
    theDemand = (RootDemand*)Components.at(3);
    theFom = (Fom*)Components.at(4);
    thePore = (Macropore*)Components.at(0);
    theMinNitro = (MinNitro*)Components.at(5);
    theWaterpore=static_cast<Waterpore*>(Components.at(6));
    theAom=static_cast<Aom*>(Components.at(7));

    rts_dp=((SoilNode*)parent)->rts_depth;
}

void RootGrowth::evaluate(double timeStep)
{
    double reduc_N=0;
    double reduc_water=0, reduc_T=0;
    double temp=theWaterpore->temperature();
    double psi_t=theWaterpore->psi();
    double thick=static_cast<SoilNode*>(parent())->getVolume(); //m
    double depth=static_cast<SoilNode*>(parent())->getDepth();  //m
    double rooting_depth=static_cast<SoilProfile*>(parent()->parent())->getRootDepth();
    double depth_gro_opt=static_cast<bodiumrunner*>(parent()->parent()->parent())->thePlantStand.depth_gro_max;
    double mean_length_rts=static_cast<bodiumrunner*>(parent()->parent()->parent())->thePlantStand.mean_length;
    double max_depth=static_cast<bodiumrunner*>(parent()->parent()->parent())->thePlantStand.pen_depth;
    double seeed = static_cast<bodiumrunner*>(parent()->parent()->parent())->thePlantStand.seedDepth();

    //very simple reduction function of the structure,
    //reduces length growth but not depth growth
    double mean_dia=static_cast<bodiumrunner*>(parent()->parent()->parent())->thePlantStand.mean_root_dia;
    double macropores=theWaterpore->getMacropores((mean_dia/2)-mean_dia/1000);
    double root_macro_ratio=theRoot->getVolume()/(macropores*thick*1e-3);

    double reduc_structure=1;
    if((root_macro_ratio>0.8)&&(root_macro_ratio<=1)){
        reduc_structure=10*pow((1-root_macro_ratio),2);}
    else if(root_macro_ratio>1){reduc_structure=0;}

    reduc_T=static_cast<bodiumrunner*>(parent()->parent()->parent())->thePlantStand.tempFunc_Optima(temp,0,25,35);
    reduc_water=static_cast<SoilProfile*>(parent()->parent())->waterReduc(psi_t);

    double depth_gro=0;                                               //m s-1
    if((theDemand->getValue()>0)&&(rooting_depth>0) && (rooting_depth>=depth) && (rooting_depth<(depth+thick)) && (rooting_depth<=max_depth)){          //check if we are in the node where roots are ending
        double availN=static_cast<SoilProfile*>(parent()->parent())->availableN(rooting_depth);
        reduc_N=1-(exp(-0.15*availN*10000));
        reduc_N=(reduc_N<=0.01)?0.01:reduc_N;
        depth_gro=depth_gro_opt*timeStep*reduc_N*reduc_water;
        static_cast<SoilProfile*>(parent()->parent())->addRootDepth(depth_gro);
    }

    /**
    * The root length growth rate \f$\mu_{rts,L}\,[m\,m^{-2}\,s^{-1}]\f$ is calculated with
    * \f[\mu_{rts,L}=\mu_{rts}\,\zeta_{rtl}\f]
    * with \f$\zeta_{rtl}\,[m\,kg^{-1}]\f$ the mean length of roots.
    * The growth rate of the root length density \f$\mu_{rts,d}\,[m\,m^{-3}\,s^{-1}]\f$ is then calculated with
    * \f[\mu_{rts,d}=\mu_{rts,L}\,\frac{f_{RLF}}{\Delta\,z\,\sum_{i=1}^{i_{r}}f_{RLF}(i)}\,f_{str}\f]
    * with \f$\mu_{rts,L}\,[m\,m^{-2}\,s^{-1}]\f$ the root length growth rate, \f$i\f$ the current soil node, \f$i_{r}\f$ the current node the deepest rooted soil node, and \f$f_{str}\f$ the reduction function of the soil structure
    * \f[\f]
    * \f[f_{str}=\begin{cases}
    10\,(1-d_{rts,L}/MP_{rts})^{2} & \text{if $d_{rts,L}/MP_{rts}\,>\,$ 0.7} \\
    1 & \text{otherwise} \\
   \end{cases}\f]
    * with \f$d_{rts,L}\,[m\,m^{-3}]\f$ the total root length density and \f$MP_{rts}\,[m\,m^{-3}]\f$ the length of uncolonized/unrooted pores larger then the mean root radius.\n
    * For calculating the total root length density \f$d_{rts,L}\,[m\,m^{-3}]\f$, the growth rate of root length density is integrated over time taking into account a decay rate \f$\sigma_{rts,L}\,[m\,m^{-3}\,s^{-1}]\f$ of \f$0.5\%\f$
    * \f[d_{rts,L}=\int_{t_{o}}^{t}(\mu_{rts,L}-\sigma_{rts,L})d\tau\f]
    */

    double rts_l=0;       //root length growth rate for specific node / thickness [m m-2 s-1]
    rooting_depth=static_cast<SoilProfile*>(parent()->parent())->getRootDepth();
    if((theDemand->getValue()>0)&&(rooting_depth>=depth)&&((depth+thick)>=seeed)){
        double fac_rtl=static_cast<SoilNode*>(parent())->factorRTL/(static_cast<SoilProfile*>(parent()->parent())->sum_factor_rtl);
        rts_l=(fac_rtl<0 || isnan(fac_rtl))?0:theDemand->getValue()*static_cast<SoilNode*>(parent())->factorRTL/(static_cast<SoilProfile*>(parent()->parent())->sum_factor_rtl);

        rts_l=rts_l*reduc_structure;
        double weight=rts_l/mean_length_rts;            //kg m² (kg/node)
        theRoot->addUpdate((isinf(weight))?0:(weight-theRoot->getValue()*r_mot));

        FomPool *actFomPool;

        foreach(actFomPool, theFom->theFomPools){
            if(actFomPool->getName()== "Fom_Roots"){
                actFomPool->addUpdate(0.45*theRoot->getValue()*r_mot);
                if(theRoot->getValue()>0)actFomPool->setCNratio(static_cast<bodiumrunner*>(parent()->parent()->parent())->thePlantStand.root_cn,theRoot->getValue()*r_mot);
            }
        }

        double exudation=(fac_rtl<0 || isnan(fac_rtl)|| isinf(fac_rtl))?0:static_cast<bodiumrunner*>(parent()->parent()->parent())->thePlantStand.exudates*fac_rtl;
        if(exudation>0){
            theAom->addUpdate(exudation);
            theAom->setCNratio(15,exudation);   //mean cn ratio of exudates from literature
        }
    }

    //update Macropore

}


Feed_Breed::Feed_Breed(QObject *parent) : Process(parent)
{
    Components.append(static_cast<SoilNode*>(parent)->getComponent("Macropore"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("Matrix"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("BioX"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("FOM"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("Waterpore"));

    theBioX = (BioX*)Components.at(2);
    theMatrix = (Matrix*)Components.at(1);
    thePore = (Macropore*)Components.at(0);
    theFom = (Fom*)Components.at(3);
    theWaterpore=static_cast<Waterpore*>(Components.at(4));

}

//at present BioX only feeds from Fom (fresh organic matter)

/**
 * BioX feeds from fresh organic matter, at present starting with the first FomPool and feeds through all until demand is fulfilled.
 * Biomass of BioX is then increased depending on given FOM and rate coefficient.
 * Macropore may be increased through burrowing or decreased throug excretion
 * Bulk Density of Matrix is affected as well
 */
void Feed_Breed::evaluate(double timeStep)
{
    FomPool *actFomPool;
    //romul_hum version

   if(!process_act) theBioX->setValue(0);

   double k5=l_osom/86400;
   double temp=theWaterpore->temperature();
   double theta=theWaterpore->theta();
   double corr_temp, corr_water;
   double assi_eff=l_somo2;
   double cn_bio=4;
   double death=l_somo1/365/86400;

   if(temp<=3){corr_temp=0.0333*temp;}
   else if(temp<=13){corr_temp=0.1*temp-0.3;}
   else if(temp<=25){corr_temp=1;}
   else{corr_temp=2-0.04*temp;}
   corr_temp=(corr_temp<0)?0:corr_temp;

   if(theta<=0.02){corr_water=0;}
   else if(theta<=0.15){corr_water=0.0769*theta*100-0.1538;}
   else if(theta<=0.7){corr_water=1;}
   else{corr_water=2.4*0.02*theta*100;}

   double f_ingest,mass,cnfood=0;
   f_ingest=k5*theFom->getValue()*corr_water*corr_temp*theBioX->getValue()*timeStep;

   mass=f_ingest*assi_eff;

   foreach(actFomPool, theFom->theFomPools){
       if(actFomPool->getName()== "Fom_Bio"){actFomPool->addUpdate(f_ingest*(1-assi_eff));}  //erstellen wenn nicht vorhanden?input of Fom Pool through dead organisms?
   }
   thePore->addVolume(theBioX->getValue()*timeStep*l_op - theBioX->getValue()*thePore->getVolume()*timeStep*l_pop);

   if(f_ingest>0){
   foreach(actFomPool, theFom->theFomPools){                               //feeding from all pools, beginning with the first (whatever it is...)
         if(actFomPool->getValue()>=f_ingest){
           actFomPool->addUpdate(-f_ingest);
           f_ingest = 0;
           cnfood=(cnfood==0)?actFomPool->getCNratio():(cnfood+actFomPool->getCNratio())/2;
           }
         else{
           f_ingest -= actFomPool->getValue();
           actFomPool->setValue(0.0);
           cnfood=(cnfood==0)?actFomPool->getCNratio():(cnfood+actFomPool->getCNratio())/2;
         }
     }
   }

   theBioX->addUpdate((mass>0)?(mass/cnfood)*cn_bio-theBioX->getValue()*death*timeStep:0);


}

void Feed_Breed::configure(const QJsonObject &theConfig)
{
    QJsonObject fbInit = theConfig["FeedBreed"].toObject();
    l_osom = fbInit["osom"].isUndefined()?l_osom:fbInit["osom"].toDouble();
    l_somo1 = fbInit["somo1"].isUndefined()?l_somo1:fbInit["somo1"].toDouble();
    l_somo2 = fbInit["somo2"].isUndefined()?l_somo2:fbInit["somo2"].toDouble();
    l_op = fbInit["op"].isUndefined()?l_op:fbInit["op"].toDouble();
    l_pop = fbInit["pop"].isUndefined()?l_pop:fbInit["pop"].toDouble();

    process_act = fbInit["process_bool"].isUndefined()?process_act:fbInit["process_bool"].toBool();

}

Som_Turn::Som_Turn(QObject *parent) : Process(parent)
{
    Components.append(static_cast<SoilNode*>(parent)->getComponent("FOM"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("AOM"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("SSOM"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("Matrix"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("Waterpore"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("Microbes"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("SleepMicrobes"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("NAOM"));

    theFom = (Fom*)Components.at(0);
    theAom = (Aom*)Components.at(1);
    theSsom = (Ssom*)Components.at(2);
    theMatrix = (Matrix*)Components.at(3);
    theWaterpore=static_cast<Waterpore*>(Components.at(4));
    theMicrobes=static_cast<Microbes*>(Components.at(5));
    theSleepMicrobes=static_cast<SleepMicrobes*>(Components.at(6));
    theNAom=static_cast<NAom*>(Components.at(7));


}
//update docu!
/*! \brief SOM turnover
 *
 * For simulating soil organic matter turnover, 2 different approaches are implement. The first one is a simple pool concept based on the model CANDY (Franko et al.,1998), the second one integrates explicit microbial activity.
 *
 * 1. Pool approach
 *
 * Soil organic matter consists of the 3 pools FOM (fresh organic matter), AOM (active organic matter) and SSOM (stabilized organic matter). The dynamics are described as follows:
 * \f[FOM=k_{FOM}\,FOM\,BAT\f]
 * \f[AOM=(\eta\,k_{FOM}\,FOM-(k_m+k_s)\,AOM+k_a\,SSOM)\,BAT\f]
 * \f[SSOM=(k_s\,AOM-k_a\,SSOM)\,BAT\f]
 * with \f$\eta\f$ the synthesis coefficient, \f$k_{FOM}, k_m, k_s, k_a\f$ the different turnover rate coefficients and \f$BAT\,[s]\f$ the biological active time.
 *
 * The concept of BAT is introduced, for including factors affecting biological processes of SOM turnover. The BAT is defined as the time interval of the day in which microbes are active and only limited by substrate availability, while in the rest of the day they are completely inactive.
 * The BAT is determined by reduction functions of aeration \f$f_A\f$, temperature \f$f_T\f$, and water \f$f_{\theta}\f$:
 * \f[BAT=\delta t\,f_A(\epsilon_L, z, FAT)\,f_T\,f_{\psi}\f]
 * with
 * \f[f_A(\epsilon_L, z, FAT)=exp(-z\,\sqrt{\frac{\vartheta(FAT)\,f_T(T)\,f_{\theta}(\theta)}{\epsilon_L\,(\epsilon_L\,\epsilon_P)}})\f]
 * with \f$\epsilon_L\f$ the relative air volume of the pore space, \f$z\,[m]\f$ the depth, \f$FAT\,[\%]\f$ the amount of clay and fine silt, \f$\vartheta(FAT)\f$ the soil texture function defined by \f$\vartheta(FAT)=0.2844\,FAT-1.4586\f$ for \f$ FAT \geq 6 \f$ and \f$\epsilon_P\f$ the constant relative pocket volume (set to 0.17); and
 * \f[f_T=min(Q_{10}^{(T-35)/10};1)\f]
 * with \f$T\,[\si{\degreeCelsius}]\f$ the soil temperature and \f$Q_{10}\f$ the reduction quotient (set to 2.1); and \f$f_ {\psi}\f$ a water reduction function (see \link waterReduc \endlink).
 *
 * 2. Microbial approach (see microbial component (link) for microbial dynamics)
 *
 * Soil organic matter is distinguished in FOM (fresh organic matter), NAOM (not assimilable organic matter), AOM (assimilable organic matter), and SSOM (stabilized organic matter).
 * Fresh organic matter gots input \f$FOM_{in}\,[kg\,m^{-2}]\f$ from organic fertilization, root decay, plant straw; but other sources can be implemented.
 * \f[FOM=FOM+FOM_{in}-k_{FOM}\,FOM\f]
 * with \f$k_{FOM}\,[-]\f$ a turnover rate coefficient. Part of carbon of the fresh organic matter is directly emitted to the atmosphere as \f$CO_2\f$, depending on a synthesis coefficient \f$\eta\f$, while the other part is decomposed to not assimilable organic matter.
 * \f[NAOM=NAOM+k_{FOM}\,\eta\,FOM+0.9\,k_{dest}\,SSOM+0.9\,\sigma_{mic}\,(Mic_{all})\,f_{mic}(T)-k_{assi}\,\frac{Mic_{act}}{Mic_{inact}}\,NAOM-0.9\,k_{stab}\,NAOM\f]
 * with \f$k_{dest}\,[-]\f$ the destabilization rate, \f$\sigma_{mic}\,[-]\f$ the maximum mortality rate of microbes, \f$Mic_{all}\,[kg\,m^{-2}]\f$ the total microbial biomass, \f$f_{mic}(T)\,[-]\f$ a temperature reduction function, \f$k_{assi}\,[-]\f$ the assimilation coefficient, \f$Mic_{act}\,[kg\,m^{-2}]\f$ the active, \f$Mic_{inact}\,[kg\,m^{-2}]\f$ the inactive microbial biomass, and \f$k_{stab}\,[-]\f$ the stabilization coefficient.
 * Note, that we assume that the main turnover between stabilized and not-stabilized organic matter takes place between NAOM and SSOM indicated by the fraction 0.9. Also most of the microbial necromass is not-assimilable. How much organic matter is broken down to an assimilable pool depends on the presence of exoenzymes, thus on the activity of microbes.
 * The assimilable organic matter has direct input by root exudates and is consumed by microbes.
 * \f[AOM=AOM+k_{assi}\,\frac{Mic_{act}}{Mic_{inact}}\,NAOM+0.1\,k_{dest}\,SSOM+0.1\,\sigma_{mic}\,(Mic_{all})\,f_{mic}(T)+RTS_{exu}-0.1\,k_{stab}\,AOM-C_{up}\f]
 * with \f$RTS_{exu}\,[kg\,m^{-2}]\f$ the root exudates and \f$C_{up}\,[kg\,m^{-2}]\f$ the consumed organic matter by microbes.
 * The stabilized soil organic matter is then calculated with
 * \f[SSOM=SSOM+0.9\,k_{stab}\,NAOM+0.1\,k_{stab}\,AOM-k_{dest}\,SSOM\f]
 * For simulating microbial dynamics based on stoichiometric considerations, all organic matter pools have a flexible C:N ratio, which is adapted depending on the C:N ratio of incoming organic matter
 * \f[CN_x=\frac{(CN_x*OM_x)+(CN_in*OM_in)}{OM_x+OM_in}\f]
 * with \f$CN_{x}\,[kg\,kg^{-1}]\f$ the C:N ratio of the organic matter pool (with x=NAOM, AOM, SSOM), \f$OM_{x}\,[kg\,m^{-2}]\f$ the current organic matter mass of x, \f$CN_{in}\,[kg\,kg^{-1}]\f$ the C:N ratio of the incoming organic matter and \f$OM_{in}\,[kg\,m^{-2}]\f$ the amount of incoming organic matter.
 */
void Som_Turn::evaluate(double timeStep)
{
    double c_rep=0,c_emissions=0, func_fat, temperature, reduc_temp, reduc_water, reduc_aeration,z, air_vol, BAT;//,act_timestep;
    FomPool *actFomPool;
    double theta=theWaterpore->theta();
    double pv=theWaterpore->poreDensity();
    func_fat=0.2844*FAT - 1.4586;
    temperature=theWaterpore->temperature();
    z=static_cast<SoilNode*>(parent())->getDepth();
    double mass=theMatrix->getMass();
    air_vol=pv-theta;
    air_vol=(air_vol<=0)?0.01:air_vol;

    reduc_temp=pow(2.5,((temperature-25)/10));

    if(reduc_temp>1) reduc_temp=1;

    reduc_water=4*(theta/pv)*(1-(theta/pv));
    reduc_water=(reduc_water<=0)?1:reduc_water;

    if(reduc_water>1) reduc_water=1;
    reduc_aeration=exp(-z*sqrt((func_fat*reduc_temp*reduc_water)/(air_vol*(air_vol*pock_vol))));

    static_cast<SoilNode*>(parent())->BAT=timeStep*reduc_aeration*reduc_temp*reduc_water;

    BAT=static_cast<SoilNode*>(parent())->BAT;

    if(static_cast<SoilProfile*>(parent()->parent())->explicit_microbes){

        double fom_cn=0;
        foreach(actFomPool, theFom->theFomPools){
            double turn=0;
            turn = actFomPool->getValue()*actFomPool->getEta()*actFomPool->getRate()*timeStep;
            actFomPool->addUpdate(-turn);
            fom_cn=(turn>0)?((fom_cn*c_rep)+(actFomPool->getCNratio()*turn))/(c_rep+turn):fom_cn;
            c_rep+=turn;
        }
        double na_to_a=0,a_to_s=0, s_to_a=0,aom_cn=0,naom_cn=0,ssom_cn=0, na_to_s=0,s_to_na=0;

        na_to_a=k_ntoa*theNAom->getValue()*static_cast<SoilNode*>(parent())->active_ratio*timeStep; //need to think about a value for the rate k_ntoa...

        a_to_s=k_s*(1-stab_am)*theAom->getValue()*timeStep;
        s_to_a=k_a*(1-stab_am)*theSsom->getValue()*timeStep;
        na_to_s=k_s*stab_am*theNAom->getValue()*timeStep;
        s_to_na=k_a*stab_am*theSsom->getValue()*timeStep;

        aom_cn=((theNAom->getCNratio()*na_to_a)+(theSsom->getCNratio()*s_to_a))/(na_to_a+s_to_a);
        naom_cn=((fom_cn*c_rep)+(theSsom->getCNratio()*s_to_na))/(c_rep+s_to_na);
        ssom_cn=(na_to_s+a_to_s>0)?((theNAom->getCNratio()*na_to_s)+(theAom->getCNratio()*a_to_s))/(na_to_s+a_to_s):theSsom->getCNratio();

        theNAom->addUpdate(c_rep-na_to_a-na_to_s+s_to_na);
        theAom->addUpdate(na_to_a-a_to_s+s_to_a);
        theSsom->addUpdate(a_to_s-s_to_a+na_to_s-s_to_na);
        theSsom->setCNratio(ssom_cn,(a_to_s+na_to_s));
        theAom->setCNratio(aom_cn,(na_to_a+s_to_a));
        theNAom->setCNratio(naom_cn,(c_rep+s_to_na));

   }
    else{
//Candy version
        foreach(actFomPool, theFom->theFomPools){
            double c_rep_act,c_emi;
            c_rep_act = actFomPool->getValue()*actFomPool->getEta()*actFomPool->getRate()*BAT;
            c_emi=actFomPool->getValue()*(1-actFomPool->getEta())*actFomPool->getRate()*BAT;
            actFomPool->addUpdate(-actFomPool->getValue()*actFomPool->getRate()*BAT);
            c_rep+=c_rep_act;
            c_emissions+=c_emi;
        }

        theAom->addUpdate(c_rep-(k_m+k_s)*theAom->getValue()*timeStep+k_a*theSsom->getValue()*timeStep-theAom->getValue()*0.02);
        theSsom->addUpdate(k_s*theAom->getValue()*timeStep-k_a*theSsom->getValue()*timeStep);
        c_emissions=c_emissions+k_m*theAom->getValue()*timeStep;
        static_cast<SoilNode*>(parent())->setCO2(c_emissions);
    }

}

void Som_Turn::configure(const QJsonObject &theConfig)
{
    QJsonObject fbInit = theConfig["SomTurn"].toObject();
    eta = fbInit["eta"].isUndefined()?eta:fbInit["eta"].toDouble();
    k_s = fbInit["k_s"].isUndefined()?k_s:fbInit["k_s"].toDouble();
    k_a = fbInit["k_a"].isUndefined()?k_a:fbInit["k_a"].toDouble();
    k_m = fbInit["k_m"].isUndefined()?k_m:fbInit["k_m"].toDouble();
    k_ntoa = fbInit["k_ntoa"].isUndefined()?k_ntoa:fbInit["k_ntoa"].toDouble();
    k_fom = fbInit["k_fom"].isUndefined()?k_fom:fbInit["k_fom"].toDouble();
    FAT = fbInit["FAT"].isUndefined()?FAT:fbInit["FAT"].toDouble();                         //FAT needs to be >6
    pock_vol = fbInit["pock_vol"].isUndefined()?pock_vol:fbInit["pock_vol"].toDouble();
    stab_am = fbInit["stab_am"].isUndefined()?stab_am:fbInit["stab_am"].toDouble();

}

Nitro_Cycle::Nitro_Cycle(QObject *parent) : Process(parent)
{
    Components.append(static_cast<SoilNode*>(parent)->getComponent("FOM"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("AOM"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("MinNitro"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("Matrix"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("Waterpore"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("Microbes"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("SleepMicrobes"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("NAOM"));

    theFom = (Fom*)Components.at(0);
    theAom = (Aom*)Components.at(1);
    theMinNitro = (MinNitro*)Components.at(2);
    theMatrix = (Matrix*)Components.at(3);
    theWaterpore=static_cast<Waterpore*>(Components.at(4));
    theMicrobes=static_cast<Microbes*>(Components.at(5));
    theSleepMicrobes=static_cast<SleepMicrobes*>(Components.at(6));
    theNAom=static_cast<NAom*>(Components.at(7));

}

/**
 * The amount of ammonium \f$N_{NH4}\,[kg\,m^{-2}]\f$ depends on mineralization of N from the fresh organic matter pool \f$FOM\,[kg\,m^{-2}]\f$ and immobilization of N needed for growth of AOM
 * \f[N_{NH4}=k_{FOM}*FOM*(\frac{1}{CN_{FOM}}-k_{FOM})*FOM*\eta*(\frac{1}{CN_{AOM}})\,BAT\f]
 * with \f$k_{FOM}\,[s^{-1}]\f$ the turnover rate of FOM, \f$CN_{FOM}\f$ the C/N ratio of the FOM pool, \f$\eta\f$ the synthesis coefficient of the FOM pool, \f$CN_{FOM}\f$ the C/N ratio of the AOM pool, and \f$BAT\,[s]\f$ the biological active time (see SOM turnover for calculation).
 * If not enough N is available for growth of the active organic matter \f$AOM\,[kg\,m^{-2}]\f$ throug NH4 pool, it is taken from the NO3 pool. NO3 is increased by nitrification of N from NH4 and further decreased through denitrification to atmosphere and leaching to ground water
 * \f[N_{NO3}=k_{nitri}-n_{rep,NO3}-(k_{denitri}*AOM*f_{T}*f_{\psi,denitri})*N_{NO3}-k_{leach}\f]
 * with \f$k_{nitri}\,[kg\,m^{-2}s^{-1}]\f$ the nitrification rate based on
 * \f[k_{nitri}=\frac{k_{nitri,max}*c_{NH4}}{K_{nitri}+c_{NH4}}\,N_{NH4}\,BAT\f]
 * with \f$k_{nitri,max}\,[s^{-1}]\f$ the maximum nitrification rate, \f$K_{nitri}\,[kg\,kg^{-1}]\f$ the half saturation coefficient, and \f$c_{NH4}\,[kg\,kg^{-1}]\f$ the ammonium concentration. \f$n_{rep,NO3}\,[kg\,m^{-2}]\f$ is the amount of N taken from the NO3 pool for respiration of AOM, \f$k_{denitri}\,[kg\,m^{-2}\,s^{-1}]\f$ the denitrification rate, \f$f_{T}\,[-]\f$ the denitrification reduction function for temperature, \f$f_{\psi,denitri}\,[-]\f$ the denitrification reduction function for water
 * \f[f_{\psi,denitri}=1-f_{\psi}\f]
 * wich is just the opposite of the water reduction function for aerobic processes; and \f$k_{leach}\,[kg\,m^{-2}\,s^{-1}]\f$ the nitrate loss through leaching
 * \f[k_{leach}=c_{NO_3}\,\omega_w\f]
 * with \f$c_{NO3}\,[kg\,kg^{-1}]\f$ the nitrate concentration and \f$\omega_w\,[kg\,m^{-2}\,s^{-1}]\f$ the water flow.
 *
 */
void Nitro_Cycle::evaluate(double timeStep){
    FomPool *actFomPool;
    double c_repn=0;
    double n_rep=0;
    double n_rep_no3=0;
    double nitri=0, denitri=0;
    double reduc_temp=0;
    double temperature=theWaterpore->temperature();
    double psi_n=theWaterpore->psi();
    double BAT=static_cast<SoilNode*>(parent())->BAT;
    double depth=static_cast<SoilNode*>(parent())->getDepth();  //m
    double thick=static_cast<SoilNode*>(parent())->getVolume();  //m

    double ntransp_in=static_cast<SoilNode*>(parent())->nitrate_in;
    double ntransp_out=static_cast<SoilNode*>(parent())->nitrate_out;
    QDateTime actTime = static_cast<bodiumrunner*>(parent()->parent()->parent())->myTimer.currentModelTime;
    QString todayStr=actTime.date().toString("MM-dd");
    if(depth>0 && depth<0.1){
	theMinNitro->addNH4((n_atmo/365)*10e-5);
   //  if(static_cast<bodiumrunner*>(parent()->parent()->parent())->thePlantStand.growth_season){
    //     theMinNitro->addNH4(static_cast<bodiumrunner*>(parent()->parent()->parent())->thePlantStand.nfix_rate*10e-5);
   // }
    // else{theMinNitro->addNH4(0.1*10e-5);}
    }

    theMinNitro->addNO3(dissolve_rate*theMinNitro->getNO3_solid());
    theMinNitro->addNH4(dissolve_rate*theMinNitro->getNH4_solid());
    theMinNitro->setNO3_solid(theMinNitro->getNO3_solid()-dissolve_rate*theMinNitro->getNO3_solid());
    theMinNitro->setNH4_solid(theMinNitro->getNH4_solid()-dissolve_rate*theMinNitro->getNH4_solid());

    double NH4_cur=theMinNitro->getNH4()+theMinNitro->getNH4t();
    double NO3_cur=theMinNitro->getNO3()+theMinNitro->getNO3t();

    theMinNitro->addNO3(ntransp_in-ntransp_out);

    if(static_cast<SoilProfile*>(parent()->parent())->explicit_microbes){ //calculated in MicrbeDyn
        c_repn=0;
        n_rep=0;

     }
    else{
    foreach(actFomPool, theFom->theFomPools){    

            c_repn+=actFomPool->getValue()*actFomPool->getRate()*(1-actFomPool->getEta())*(1/actFomPool->getCNratio())*BAT;
            double nin=actFomPool->getValue()*actFomPool->getRate()*actFomPool->getEta()*(1/actFomPool->getCNratio())*BAT;
            n_rep +=nin*(1-(8/actFomPool->getCNratio()));
        }
    }

    if(n_rep>NH4_cur){
        n_rep_no3=n_rep-NH4_cur;
        n_rep=NH4_cur;
    }

    nitri=((k_nitri*thick)*NH4_cur*static_cast<SoilNode*>(parent())->active_ratio*timeStep)/(Kconst_nitri*thick+NH4_cur);

    if((temperature>=1)&&(psi_n>=-300)&&(psi_n<=0)){
        reduc_temp=0.1*exp(0.046*temperature);
        if(static_cast<SoilProfile*>(parent()->parent())->explicit_microbes) denitri=k_denitri*timeStep*NO3_cur*static_cast<SoilNode*>(parent())->active_ratio*reduc_temp;
        else  denitri=k_denitri*timeStep*NO3_cur*theAom->getValue()*reduc_temp;

    }

    theMinNitro->addNH4(c_repn-n_rep-nitri);
    theMinNitro->addNO3(nitri-denitri);
    if(theMinNitro->getNO3()+theMinNitro->getNO3t()>n_rep_no3){
        theMinNitro->addNO3(-n_rep_no3);
    }
    else{
        theMinNitro->setNO3(0);
    }
    static_cast<SoilNode*>(parent())->nitrate_in=0;

}

void Nitro_Cycle::configure(const QJsonObject &theConfig)
{
    QJsonObject fbInit = theConfig["NitroCyc"].toObject();
    Kconst_nitri=fbInit["Kconst"].isUndefined()?Kconst_nitri:fbInit["Kconst"].toDouble();
    k_nitri=fbInit["k_nitri"].isUndefined()?k_nitri:fbInit["k_nitri"].toDouble();
    k_denitri=fbInit["k_denitri"].isUndefined()?k_denitri:fbInit["k_denitri"].toDouble();
    dissolve_rate=fbInit["dissolve"].isUndefined()?dissolve_rate:fbInit["dissolve"].toDouble();
    n_atmo=fbInit["n_atmo"].isUndefined()?n_atmo:fbInit["n_atmo"].toDouble();

}

Microbe_Dyn::Microbe_Dyn(QObject *parent) : Process(parent)
{
    Components.append(static_cast<SoilNode*>(parent)->getComponent("FOM"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("AOM"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("Microbes"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("Matrix"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("Waterpore"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("SleepMicrobes"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("NAOM"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("MinNitro"));
    Components.append(static_cast<SoilNode*>(parent)->getComponent("Oxygen"));

    theFom = (Fom*)Components.at(0);
    theAom = (Aom*)Components.at(1);
    theMicrobes=static_cast<Microbes*>(Components.at(2));
    theMatrix = (Matrix*)Components.at(3);
    theWaterpore=static_cast<Waterpore*>(Components.at(4));
    theSleepMicrobes=static_cast<SleepMicrobes*>(Components.at(5));
    theNAom=static_cast<NAom*>(Components.at(6));
    theMinNitro=static_cast<MinNitro*>(Components.at(7));
    theOxygen=static_cast<Oxygen*>(Components.at(8));
}

/**
 * Microorganisms are distinguished in active and inactive. The amount of active microorganisms \f$Mic_{act}\,[kg\,m^{-2}]\f$ consists of the increase in biomass \f$Mic_{\mu}\,[kg\,m^{-2}]\f$, and the amount of activated \f$\Upsilon_{act}\,[kg\,m^{-2}]\f$ and inactivated microorganisms \f$\Upsilon_{inact}\,[kg\,m^{-2}]\f$.
 * \f[Mic_{act}=Mic_{\mu}-\Upsilon_{inact}(\Psi,T,O_2,AOM)+\Upsilon_{act}(\Psi,T)\f]
 *  where the increase depends on substrake uptake \f$C_{up}\,[kg\,m^{-2}]\f$ and the nitrogen dependend carbon use efficiency \f$CUE(N) [-]\f$
 * \f[Mic_{\mu}=C_{up}\,CUE(N)\f]
 * following Monod kinetics
 * \f[C_{up}=\mu_{mic}\,Mic_{act}\,f_{mic}(\Psi,T)\,(1-\frac{Mic_{act}}{Cap(\Theta)})\,\frac{AOM}{AOM+K_{AOM}}\,\frac{O_2}{O_2+K_{O_2}}\f]
 * with \f$\mu_{mic}\,[h^{-1}]\f$ the maximum growth rate of microorganisms, \f$f_{mic}(\Psi,T)\,[-]\f$ a growth reduction function depending on water potential and temperature (Verlinken auf Reduktionsfunktionen!), \f$Cap(\Theta)\,[kg\,m^{-2}]\f$ the water content depending capacity or colonizable space, \f$AOM\,[kg\,m^{-2}]\f$ the amount of assimilable organic carbon, \f$O_2\,[kg\,m^{-2}]\f$ the amount of oxygen, and \f$K_{AOM}\,[kg\,m^{-2}]\f$ and \f$K_{O_2}\,[kg\,m^{-2}]\f$ the half-saturation constant for assimilable organic carbon and oxygen, respectively.\n
 * The carbon use efficiency is reduced if not enough nitrogen is available to compensate for the microbial C:N ratio. For this, the temporal microbial C:N ratio \f$CN_{mic,temp}\,[-]\f$ after subsrate consumption is calculated
 * \f[CN_{mic,temp}=\frac{CN_{mic}\,Mic_{act}+CN_{AOM}\,Mic_{\mu}}{Mic_{act}+Mic_{\mu}}\f]
 * with \f$CN_{mic}\,[-]\f$ the static microbial C:N ratio (set to 8) and \f$CN_{AOM}\,[-]\f$ the current C:N ratio of the assimilable organic matter. If the temporal microbial C:N ratio is larger then the static microbial C:N ratio, nitrogen can be immobilised to compensate for this difference.\n
 * The potential immobilized nitrogen \f$N_{imm,pot}\,[kg\,m^{-2}]\f$ is calculated
 * \f[N_{imm,pot}=\begin{cases}
     (\frac{CN_{mic,temp}}{CN_{mic}}-1)\,\frac{Mic_{act}}{CN_{mic}}& \text{if $CN_{mic,temp} > CN_{mic} $} \\
      0 & \text{if $CN_{mic,temp} \leq CN_{mic} $}
        \end{cases}
 *\f]
 * If the potential immobilized nitrogen is more than the available nitrogen in form of ammonium \f$N_{NH4}\,[kg\,m^{-2}]\f$, the carbon use efficiency is reduced accordingly.
 * \f[CUE(N)=\begin{cases}
      ((\frac{Mic_{act}}{CN_{mic}}+N_{NH4})\,CN_{mic}-Mic_{act})\,\frac{1}{C_{up}}& \text{if $N_{imm,pot} > N_{NH4} $} \\
       CUE_{max} & \text{if $N_{imm,pot} \leq N_{NH4}  $}
        \end{cases}
 *\f]
 * with \f$CUE_{max}\,[-]\f$ the maximum carbon use efficiency.\n
 * The exchange with the mineral ammonium \f$N_{ex}\,[kg\,m^{-2}]\f$ depends on stoichiometric constrains
 *\f[N_{ex}=\begin{cases}
     -1\,(\frac{CN_{mic,temp}}{CN_{mic}}-1)\,\frac{Mic_{act}}{CN_{mic}}& \text{if $CN_{mic,temp} > CN_{mic} $} \\
      (\frac{CN_{mic}}{CN_{mic,temp}}-1)\,\frac{Mic_{act}}{CN_{mic}} & \text{if $CN_{mic,temp} < CN_{mic} $}\\
      0 & \text{if $CN_{mic,temp} = CN_{mic} $}
        \end{cases}
 \f]
 * with immbolization reducing the ammonium content if the temporal C:N ratio is larger then the microbial C:N ratio and mineralization releasing ammonium into the soil if it is smaller.
 * The amount of inactive microorganisms \f$Mic_{inact}\,[kg\,m^{-2}]\f$ is described
 * \f[Mic_{inact}=\Upsilon_{inact}(\Psi,T,O_2,AOM)-\Upsilon_{act}(\Psi,T)-\sigma_{mic}\,(Mic_{all})\,f_{mic}(T)\f]
 * with \f$\sigma_{mic}\,[-]\f$ the maximum mortality rate of microorganisms, \f$Mic_{all}\,[kg\,m^{-2}]\f$ the total amount of active and inactive microorganisms (sum of \f$Mic_{act}\f$ and \f$Mic_{inact}\f$) and \f$f_{mic}(T)\,[-]\f$ a temperature dependend reduction function. How much microorganisms are activated or inactivated depends on a threshold \f$f_{mic}^{thr}\,[-]\f$, with the activation dependend on water potential and temperature
 * \f[\Upsilon_{act}(\Psi,T)=\begin{cases}
        Mic_{inact}\,(1-\frac{Mic_{all}-Mic_{inact}}{Mic_{all}})\,0.6 & \text{if $f_{mic}(\Psi,T) \geq f_{mic}^{thr} $} \\
         0 & \text{if $f_{mic}(\Psi,T) < f_{mic}^{thr}$}
         \end{cases}
  * \f]
  * , while the inactivation depends on water potential and available carbon
  * \f[\Upsilon_{inact}(\Psi,AOM)=\Upsilon_{inact}(\Psi,T,O_2)+\Upsilon_{inact}(AOM)\f]
  * with \f$f_{mic}^{per}[-]\f$ an additional parameter describing the percentage of active microorganisms going inactive in case of drought
  * \f[\Upsilon_{inact}(\Psi,T,O_2)=\begin{cases}
        Mic_{act}\,(1-min(f_{mic}(\Psi),f_{mic}(T)))\,f_{mic}^{per} & \text{if $f_{mic}(\Psi) \leq f_{mic}^{thr,\Psi}$ or $f_{mic}(T) \leq f_{mic}^{thr,T}$ or $O_2 \leq f_{mic}^{thr,O_2}$} \\
         0 & \text{if $f_{mic}(\Psi) > f_{mic}^{thr}$ and $f_{mic}(T) > f_{mic}^{thr,T}$ and $O_2 > f_{mic}^{thr,O_2}$}
         \end{cases}
  * \f]
  * , and if not enough carbon is available for the maintenance of active microbes
  * \f[\Upsilon_{inact}(AOM)=\begin{cases}
        Mic_{act}\,(1-\frac{(\alpha_{mic}\,Mic_{act})-AOM}{\alpha_{mic}\,Mic_{act}}) & \text{if $AOM < (\alpha_{mic}\,Mic_{act}) $} \\
         0 & \text{if $AOM \geq (\alpha_{mic}\,Mic_{act})$}
         \end{cases}
  * \f]
  * with \f$\alpha_{mic}[h^{-1}]\f$ the maintenance rate of microorganisms.\n
  * To distinguish between growth types of oligothropic and heteroptrophic microorganisms, a reduction of maximum growth rate and mortility rate is introduced based on available carbon
  * \f[\mu_{mic}=\begin{cases}
        \mu_{mic}\,0.5 & \text{if $\frac{AOM}{AOM+K_{AOM}} < 0.25 $} \\
        \mu_{mic} & \text{if $\frac{AOM}{AOM+K_{AOM}} \geq 0.25$}
         \end{cases}
  * \f]
  * \f[\sigma_{mic}=\begin{cases}
        \sigma_{mic}\,0.75 & \text{if $\frac{AOM}{AOM+K_{AOM}} < 0.25 $} \\
        \sigma_{mic} & \text{if $\frac{AOM}{AOM+K_{AOM}} \geq 0.25$}
         \end{cases}
  * \f]
 */
void Microbe_Dyn::evaluate(double timeStep){
    double availC, necro=0, cur_cap, resp=0, reduc_temp,reduc_water,reduc_all, microbes_val,inact_val,duration,goactive=0,goinactive=0;
    double psi_t=theWaterpore->psi();
    double temperature=theWaterpore->temperature();
    cur_cap=theWaterpore->watercontent()*capacity; //colonizable space = capacity * waterfilled space
    double km=kmm*static_cast<SoilNode*>(parent())->getVolume();   //michaelis-menten constant in kg / m³ *thickness = kg / m²
    double oxy=(theOxygen->getTempOxy()>0)?theOxygen->concentration():theOxygen->concentration()+theOxygen->getTempOxy();

    reduc_temp=pow(2.5,((temperature-25)/10));
    if(reduc_temp>1) reduc_temp=1;

    reduc_water=static_cast<SoilNode*>(parent())->microbes_waterreduc(psi_t);

    reduc_all=reduc_water*reduc_temp;

    microbes_val=theMicrobes->getValue();
    inact_val=theSleepMicrobes->getValue();

    duration=timeStep/3600;

    double availN=theMinNitro->getNH4()+theMinNitro->getNH4t();

    availC=theAom->getValue();

    double accessC=0.5;
    availC=availC*accessC; //assume only half of C is accesible
    availN=availN*0.5; //assume only half of N is accesible
    double availO2=oxy*theWaterpore->airContent()*0.5;


    //oxygen consumption
    //0.5 % O2 consumption per hour in 180 ml
    //100 % O2 eq 1.5 kg m-3 dh 0.5 % eq 0.75*10-2 kg m-3 * 180*10-6 m3 * 24 = 3.24*10-5 kgO2 d-1
    //3.24*10-5 kgO2 d-1 / (10-4 kgmic kgBoden-1 *0.14 kg Boden (Einwaage)) = 2.3 kg O2 kgmic-1 d-1 = oxyConRate
    double oxyConsume=oxyConRate*theMicrobes->getValue();

    double maint=maint_r*duration*microbes_val;

    if ((reduc_water>(drought_thre))&&(reduc_temp>(temp_thre))){

        double diff1=microbes_val/(microbes_val+inact_val);
        goactive=inact_val*(1-diff1)*active_p*reduc_water*reduc_temp;

        maint=maint_r*duration*(microbes_val+goactive);
        if(maint>availC){
            double maint_act=maint_r*duration*microbes_val;
            goactive=(availC<maint_act)?0:(availC-maint_act)/(maint_r*duration);
            maint=maint_r*duration*(microbes_val+goactive);
        }
        double diff=((inact_val-sporolater)/inact_val);
        inact_val-=goactive;
        sporolater-=goactive*(1-diff);
        if(sporolater>inact_val){
            sporolater=inact_val;
        }
    }

    if ((reduc_water<=drought_thre)||(reduc_temp<=temp_thre)||(oxy<0.25*0.3)){

        double diff=(reduc_water>reduc_temp)?1-reduc_temp:1-reduc_water;
        goinactive=diff*drought_per*microbes_val;

        inact_val+=goinactive;
        microbes_val-=goinactive;
        sporolater+=goinactive* spor_am;
    }

    maint=maint_r*duration*(microbes_val+goactive);

    //distinguish growth types: oligotroph growth if low C conc = maintenance growth dependent, growth rate reduced (see docu)
    bool oligotroph=((availC/(availC+km))<0.25)?true:false;
    theMicrobes->setCredu(availC/(availC+km));

    double inact_temp=0, increase=0, complete=0;
      if((availC>maint)){
         maint=(oligotroph)?maint:maint+maint*(availC/(availC+km)); //growth related maintenance
        availC=availC-maint;
        //michaelis menten growth, as basis for stoichiometrie - "own" kinetics for each nutrient
        complete=(microbes_val<cur_cap)?(oligotroph)?(((microbes_val*exp(growth_r*0.5*duration))-microbes_val))*reduc_all*(1-microbes_val/cur_cap)*availC/(availC+km)*oxy/(oxy+km_ox):(((microbes_val*exp(growth_r*duration))-microbes_val))*reduc_all*(1-microbes_val/cur_cap)*availC/(availC+km)*oxy/(oxy+km_ox):0;

        increase=complete*CUE;
        resp=complete*(1-CUE);
        double diff=(complete-availC)/complete;

        if(complete>availC){
            increase=(1-diff)*increase;
            resp=(1-diff)*resp;
            complete=availC;
        }

    }
    else if(availC<=maint){
        double diff=(maint-availC)/maint;
        inact_temp=(1-diff)*microbes_val;
        maint=availC;
        availC=0;
        microbes_val=microbes_val-inact_temp;
        sporolater+=inact_temp* spor_am;
    }

   oxyConsume=(oxyConsume>availO2)?availO2:oxyConsume;
   theOxygen->consume(-oxyConsume);

   theMicrobes->setFBratio(1); //FB ratio currently static, will be dynamic in the future

   theMicrobes->setCNratio((theMicrobes->getFBratio()*cn_ratio_f+cn_ratio_b)/(theMicrobes->getFBratio()+1));

   double cn_into=increase/((maint+complete)/theAom->getCNratio());
   double cn_ratio_pot=((theMicrobes->getCNratio()*microbes_val)+(cn_into*increase))/(microbes_val+increase);
   double n_needed=((cn_ratio_pot/theMicrobes->getCNratio())>1)?(((cn_ratio_pot/theMicrobes->getCNratio())-1)*((microbes_val+goactive)/theMicrobes->getCNratio())):0;

    if(n_needed>availN){

        double new_n=(microbes_val+goactive)/theMicrobes->getCNratio()+availN;
        double new_c_increase=new_n*theMicrobes->getCNratio()-(microbes_val+goactive);
        increase=(complete>0)?new_c_increase:0;
        resp=(complete>0)?complete-new_c_increase:0;
    }

    if(increase>0){
        double cn_into=increase/((maint+complete)/theAom->getCNratio());
        theMicrobes->setCNratio_temp(((theMicrobes->getCNratio()*microbes_val)+(cn_into*increase))/(microbes_val+increase));
    }
    else{
        theMicrobes->setCNratio_temp(theMicrobes->getCNratio());
    }

    inact_val=inact_val+inact_temp;
    microbes_val=microbes_val+increase+goactive;

    availC=availC-increase-resp;
    availC=(availC<0)?0:availC;

    static_cast<SoilNode*>(parent())->setCO2(resp);

    theMicrobes->addUpdate(microbes_val-theMicrobes->getValue());

    theAom->addUpdate(-((theAom->getValue()*accessC)-availC));

    theSleepMicrobes->addUpdate(inact_val-theSleepMicrobes->getValue());

    double micro_cn_diff=theMicrobes->getCNratio()/theMicrobes->getCNratio_temp();
    double c_repn=0, n_rep=0;

    if((microbes_val-theMicrobes->getValue())>0){
        if(micro_cn_diff>1){
            c_repn=(micro_cn_diff-1)*(theMicrobes->getValue()/theMicrobes->getCNratio());
        }
        else{
            n_rep=((1/micro_cn_diff)-1)*(theMicrobes->getValue()/theMicrobes->getCNratio());
        }
    }

    theMinNitro->addNH4(c_repn-n_rep);

    //5-15 % of whole microbial biomass dies per day (measured) - but will be taken only from inactive pool in the model
    necro=death_r*(microbes_val+inact_val)*reduc_temp;      //death rate is temperature dependend
    necro=(oligotroph)?necro*0.75:necro;
    necro=(necro<(inact_val-sporolater))?necro:(inact_val-sporolater);

    theAom->addUpdate(necro*(1-necro_am));
    theAom->setCNratio(theMicrobes->getCNratio(),necro*(1-necro_am));
    theSleepMicrobes->addUpdate(-necro);

    theNAom->addUpdate(necro*necro_am);          //most part of the necromass joining not assimilable pool
    theNAom->setCNratio(theMicrobes->getCNratio(),necro*necro_am);

}

void Microbe_Dyn::configure(const QJsonObject &theConfig){
    QJsonObject fbInit = theConfig["MicrobeDyn"].toObject();
    death_r = fbInit["r_death"].isUndefined()?death_r:fbInit["r_death"].toDouble();
    growth_r = fbInit["r_growth"].isUndefined()?growth_r:fbInit["r_growth"].toDouble();     //h-1 !!!
    capacity = fbInit["capacity"].isUndefined()?capacity:fbInit["capacity"].toDouble();
    kmm = fbInit["km"].isUndefined()?kmm:fbInit["km"].toDouble();
    maint_r = fbInit["r_maint"].isUndefined()?maint_r:fbInit["r_maint"].toDouble();
    drought_thre=fbInit["drought_th"].isUndefined()?drought_thre:fbInit["drought_th"].toDouble();
    drought_per=fbInit["drought_p"].isUndefined()?drought_per:fbInit["drought_p"].toDouble();
    necro_am = fbInit["necro_am"].isUndefined()?necro_am:fbInit["necro_am"].toDouble();
    spor_am = fbInit["spor_am"].isUndefined()? spor_am:fbInit["spor_am"].toDouble();
    CUE = fbInit["CUE"].isUndefined()? CUE:fbInit["CUE"].toDouble();
    active_th=fbInit["active_th"].isUndefined()?active_th:fbInit["active_th"].toDouble();
    active_p=fbInit["active_p"].isUndefined()?active_p:fbInit["active_p"].toDouble();
    temp_thre=fbInit["temp_th"].isUndefined()?temp_thre:fbInit["temp_th"].toDouble();
    oxyConRate=fbInit["oxyConRate"].isUndefined()?oxyConRate:fbInit["oxyConRate"].toDouble();
    cn_ratio_b = fbInit["cn"].isUndefined()? cn_ratio_b:fbInit["cn"].toDouble();
    cn_ratio_f = fbInit["cn_f"].isUndefined()? cn_ratio_f:fbInit["cn_f"].toDouble();
    km_ox = fbInit["km_oxy"].isUndefined()? km_ox:fbInit["km_oxy"].toDouble();

    sporolater= spor_am*theSleepMicrobes->getValue();


}

void Waterpore::configure(const QJsonObject &theConfig)
{

    if(!isConfigured){

        if(!theConfig["SubNodes"].isUndefined())
            subNum = theConfig["SubNodes"].toInt();
        else
            subNum = 1;
        for(int i = 0;i<subNum;i++){
            wsnode *theNode = new wsnode(theConfig, this);
            ((SoilProfile*)(parent()->parent()))->theWNodes.append(theNode );
            theSubNodes.append(theNode);
            //theNodes.last()->setNodeThick(thick);
            //connect(this, SIGNAL(stressOccurred(double)), theNode, SLOT(stressMe(double)));

        }
        isConfigured = true;
    }

}

double Waterpore::watercontent()
{
    double psiSum =0;
    wsnode* actNode;
    foreach(actNode, theSubNodes)
    {
        psiSum += actNode->watercontent();
    }
    return psiSum;
}
double Waterpore::getVolume()
{
    double volSum =0;
    wsnode* actNode;
    foreach(actNode, theSubNodes)
    {
        volSum += actNode->getMacropores(0);
    }
    return volSum/1000;
}

double Waterpore::psi()
{
    double psiSum =0;
    wsnode* actNode;
    foreach(actNode, theSubNodes)
    {
        psiSum += actNode->psi();
    }
    return psiSum/subNum;
}

double Waterpore::theta()
{
    double psiSum =0;
    wsnode* actNode;
    foreach(actNode, theSubNodes)
    {
        psiSum += actNode->theta();
    }
    return psiSum/subNum;
}
double Waterpore::poreDensity()
{
    double psiSum =0;
    wsnode* actNode;
    foreach(actNode, theSubNodes)
    {
        psiSum += actNode->poreDensity();
    }
    return psiSum/subNum;
}
double Waterpore::topval()
{
    return theSubNodes.at(0)->topval();
}
double Waterpore::temperature()
{
    double psiSum =0;
    wsnode* actNode;
    foreach(actNode, theSubNodes)
    {
        psiSum += actNode->temperature();
    }
    return psiSum/subNum;
}
double Waterpore::getMacropores(double boundary){
    double pore=0;
    wsnode*actNode;

    foreach(actNode,theSubNodes)
    {
        pore+=actNode->getMacropores(boundary);
    }
    return pore;
}
double Waterpore::awc(void){
    double pore=0;
    wsnode*actNode;

    foreach(actNode,theSubNodes)
    {
        pore+=actNode->awc();
    }
    return pore;
}
void Waterpore::rootUp(double rootup_l)
{
    wsnode* actNode;
    foreach(actNode, theSubNodes)
    {
        actNode->setRootUp(rootup_l/subNum);
        //static_cast<SoilNode*>(parent())->rootiup+=actNode->getRootup();
    }
}
double Waterpore::airContent()
{
    double airCont =getVolume() - watercontent()/1000;
    airCont = airCont<1E-5?1e-5:airCont;
    return(airCont); //watercontent is kg/m², volume is m³/m²
}
double Waterpore::getAirDiffusivity(void)
{
    double nodeThick = static_cast<SoilNode*>(parent())->getVolume();
    double Phi =getVolume()/nodeThick;
    double epsilon = airContent()/nodeThick;
    double epsi100 = getMacropores(1.5e-5)/1000/nodeThick;
    epsi100 = epsi100 < 1e-4?1e-4:epsi100;
    double Chi = log10((2*pow(epsi100,3)+0.04*epsi100)/Phi*Phi)/log10(epsi100/Phi);
    double diffy = Phi*Phi*pow(epsilon/Phi, Chi)*1.76e-5;

    return(diffy);


}

void Waterpore::stressMe(double theStress)
{
    wsnode* actNode;
    foreach(actNode, theSubNodes)
    {
        actNode->stressMe(theStress);
        //static_cast<SoilNode*>(parent())->rootiup+=actNode->getRootup();
    }
}
//Waterpore::Waterpore(QObject *parent) : Component(parent)
//{

//}

Oxygen::Oxygen(QObject *parent): Component(parent)
{
    concOxy=tempOxy=0;
    thePore = static_cast<Waterpore*>(static_cast<SoilNode*>(parent)->getComponent("Waterpore"));

}

double Oxygen::getMass()
{
    return(concOxy*thePore->airContent());

}

void Oxygen::configure(const QJsonObject &theConfig)
{

        if(!theConfig["Oxygen"].isUndefined())
            concOxy = theConfig["Oxygen"].toDouble();


}
void Oxygen::doUpdate(void)
{

    tempOxy=(isnan(tempOxy))?0:tempOxy;
    concOxy += tempOxy;
    concOxy = concOxy>0.3?0.3:concOxy;//limit to atmospheric boundary
    if(concOxy<0)
        concOxy=0;
    tempOxy = 0;
    Component::doUpdate();
}

oxyTransport::oxyTransport(QObject *parent): Process(parent)
{
    theNode = static_cast<SoilNode*>(parent);
    theUpperNode = theNode->getTopNode();
    theWater = static_cast<Waterpore*>(theNode->getComponent("Waterpore"));
    theUpperWater = theUpperNode == nullptr?nullptr:static_cast<Waterpore*>(theUpperNode->getComponent("Waterpore"));
    theOxy = static_cast<Oxygen*>(theNode->getComponent("Oxygen"));
    theUpperOxy = theUpperNode == nullptr?nullptr:static_cast<Oxygen*>(theUpperNode->getComponent("Oxygen"));

}
void oxyTransport::evaluate(double timeStep)
{
    double topConc, theConc, topDiffu, theDiffu, topDist, theDist, oxTrans, oxMaxTrans;
    if(theUpperNode== nullptr){
        topConc = 0.3;//kg/m³ in free air
        topDiffu = 1.76e-5;//oxygen diffusivity in air
        topDist = 0.01;// small distance of 1cm (thump/sucking) to the free air
    }else{
        topConc = theUpperOxy->concentration();
        topDiffu = theUpperWater->getAirDiffusivity();
        topDist = theUpperNode->getVolume()/2;
    }
    theConc = theOxy->concentration();
    theDiffu = theWater->getAirDiffusivity();
    theDist = theNode->getVolume()/2;
    theDiffu = 1/(topDist+theDist)/(topDist+theDist)/(theDist/theDiffu+topDist/topDiffu)*timeStep;
    theDiffu = theDiffu>0.5?0.5:theDiffu<0?0:theDiffu;
    oxTrans = (topConc-theConc)*theDiffu;
    //if(oxTrans > 0){
        oxMaxTrans = 0.3 - theOxy->concentration();
        oxTrans = oxTrans>oxMaxTrans?oxMaxTrans:oxTrans;
        if(theUpperNode != nullptr){
            oxMaxTrans = theUpperOxy->concentration();
            oxTrans = oxTrans>oxMaxTrans?oxMaxTrans:oxTrans;
        }
        /*
    }else{
        oxMaxTrans = theOxy->getMass();
        oxTrans = oxTrans>oxMaxTrans?oxMaxTrans:oxTrans;
        if(theUpperNode != nullptr){
            oxMaxTrans = (theUpperWater->getVolume()-theUpperWater->watercontent()/1000)*topConc-theUpperOxy->getMass();
            oxTrans = oxTrans>oxMaxTrans?oxMaxTrans:oxTrans;
        }
    }*/

    theOxy->addToOxy(oxTrans);
    if(theUpperNode != nullptr)
        theUpperOxy->addToOxy(-oxTrans);

}

structCompaction::structCompaction(QObject *parent): Process(parent)
{
    theNode = static_cast<SoilNode*>(parent);
    theWater = static_cast<Waterpore*>(theNode->getComponent("Waterpore"));

}
#define tropsec 31556925.26
void structCompaction::evaluate(double timestep)
{
    double load = theNode->loading;
    load *= timestep/tropsec/1000;
    theWater->stressMe(-load);

}
