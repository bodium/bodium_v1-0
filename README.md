# BODIUM Version 1-0

Here, we give you instructions on [how to build and / or run the BODIUM model](## Build and run BODIUM) and how to [analyse the output with R and plot those results](## Analyse and plot the results with R). 

## Build and run BODIUM

How to build and / or run the BODIUM model depends on your operating system: [Linux](### Linux), [Windows](### Windows) or [Apple](### Apple)
However, necessary components for all users are the Qt Base Library 5 or higher and a C++ Compiler. You can visit https://www.qt.io/download-qt-installer or install the packages from distribution

### Linux

Download the BODIUM:

```
git clone https://git.ufz.de/bodium/Bodium_v1-0.git
cd Bodium_v1-0
```
Alternative to using git you can also take a web browser, enter the web address https://git.ufz.de/bodium/Bodium_v1-0 and use the download button.

Configure make process:
```
$path_to_your_Qt/qmake Bodium.pro
```

Build program:
```
make
```

This will generate the program locally. You can now run it "as is" in your local folder. It will use the local config.json and run the BODIUM model. Output will be written to the local folder.
```
Bodium
```
If you want to test different configuration scenarios you can rename the config file and pass it as an argument:

```
Bodium mySplendidConfig.json
```

You can also move things to different folders, change your working directory and the like. Just make sure that your configs mimick this by having the right path names (relative or absolute), that you give the right path to the program (or have it in your binary search path) and address the correct config file.

### Windows

For Windows systems, there are three possibilities:

#### 1. Run the Bodium.exe


Visit https://git.ufz.de/bodium/Bodium_v1.0

Download all folders (you do not need the single files on the top level)

Extract everything what in the folder "Run_on_windows" and make sure it is one level above all the other folders. 

Double click Bodium.exe

To run other config files, you can simply drag & drop the config on the Bodium.exe or open the Windows Command Prompt (type cmd to the search) and start BODIUM with

start Bodium.exe yourconfig.json

#### 2. Build BODIUM with QtCreator

Visit https://www.qt.io/download-qt-installer

Download and install Qt, QtCreator and a C++ compiler.

Visit https://git.ufz.de/bodium/Bodium_v1-0

and clone the whole project. 
Open Bodium.pro with QtCreator and follow the instructions to configure the build settings.

Build the project. You can now start BODIUM also following the instructions in [number 1.](#### 1. Run the Bodium.exe)

#### 3. Build BODIUM via command line

Here, you do not need to install QtCreator. 
Visit https://www.qt.io/download-qt-installer

Download and install Qt and a C++ compiler.

Open a command line software of your choice (e.g. cygwin, Cmder)
Set environmental variables for Qt and the C++ compiler.

```
git clone https://git.ufz.de/bodium/Bodium_v1-0.git
cd $path_to_bodium
```
Then configure the make process

```
$path_to_qmake/qmake.exe Bodium.pro -spec $compiler
```
You find the qmake.exe here Qt/$version/$compiler/bin/qmake.exe (e.g. if you have installed mingw under Qt 6.2.3 it is Qt/6.2.3/mingw_64/bin/qmake.exe)
and the compiler specification depends on your installed C++ installer with win32-g++ for MinGW and win32-msvc for MSVC

Then, build BODIUM depending on your compiler with:
```
mingw32-make.exe
```
or
```
jom.exe
```

### Apple

You can follow the [Linux](### Linux) instructions, but you have to first install Xcode including CommandLineTools.

## Analyse and plot the results with R

### Analyse
The output file is organised such that R can process it as data.table
If you just want to analyse on simulation result, you can simply import it to R and plot whatever you want. If you want to minimize the data size, use fread (package: data.table) and select the columns you are interested in before you import it to R. For example
```
fread("BL_till.csv", select=c("dat_time","Plant_biomass_SO"))
```
will only give you the Date and the plant biomass of the storage organs.

If you have several simulations, you can use the R script 
```
Rscript analyse_bodium.R
```
Here, you have to set the working directory, which is the folder where your results are, and identifier for the different simulations based on the file names. For example, if you have performed two simulations, one with tillage and one without, the filenames should include something like "till_1" and "till_0", then set varied_par<-c("till") in the R script and a column will be included with the identifier. 

After you have made this changes, you can run the R script. 
The output are two files. 
The first yield_df includes yearly information on the yield, the nitrate leachate, the summed waterstress over the vegetation period and the nitrogen content of the plant.
The second nut_df includes daily values of mass and C:N ratio of different SOM pools, active and inactive microbial biomass, total soil organic carbon, total nitrogen, soil matrix (all summed over the depth of 20 cm - this you can adapt in the script), the available water content AWC in the upper 30 cm (ploughing depth), and the filter indicator. 

### Plotting

For plotting the results, you can use whatever R package you prefer or other software as well.
The R script plot4ms.R gives you some examplary plots, and also reproduces the figures of the related manuscript (König, et al., in prep).
You need to set the working directory, where the two files yield_df and nut_df are, and then you can run the script.
