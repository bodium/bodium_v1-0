#ifndef waterProfile_H
#define waterProfile_H

#include <QObject>
#include <QJsonObject>
#include <QJsonArray>
#include <QString>
#include "wsnode.h"
#include "conductor.h"
#include "watercell.h"

class waterProfile : public QObject
{
    Q_OBJECT
public:
    explicit waterProfile(QObject *parent = 0);
    void init(const QJsonObject &theInit);
    void StructChange(const QJsonObject &theInit);
    /** central working beast. At each time step, determine matrix potentials,
     * evaluate duration of stable fluxes, and iterate over time until time step is completed.
     * Determination of matrix potentials: each soil node is requested to update its matrix
     * potential to balance the fluxes. If the potential changes, it emits a signal, which is received by
     * the waterProfile. This step is reiterated until no changes are signalled.
     * Then the thermal fluxes are calculated.
     * */
    void doTimestep(double timestep);
    QList<wsnode*> theNodes;
    QList<conductor*> theConducts;
    WaterCell *rainy, *evapy;
    QString getVals(void);
    double getChangeSum(void);
    void resetChangeRates(void);
    void setEvaRain(double rain, double evapo, double ttemp);


signals:
    void stepTime(double timestep);

public slots:
    void matrixChanges(double matrix){matrixChanged = true;}

private:
    double residualRain, residualEva;
    bool matrixChanged;
    wsnode *boundary;
};

#endif // waterProfile_H
