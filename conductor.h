#ifndef CONDUCTOR_H
#define CONDUCTOR_H

#include <QObject>
#include <math.h>
#include "watercell.h"
/**********************************//**
 * @brief The conductor class implements the water flux elements
 *
 * Water fluxes between \link WaterCell water cells\endlink are realized with the conductor class.
 * Each  water cell is linked to the matrix cell of its \link wsnode \endlink. water flux is determined by
 * conductivity of the conductor and the potential difference at both source and target. The conductivity is
 * determined by the harmonic mean of the conductivities of the source and the target.
 */
class conductor : public QObject
{
    Q_OBJECT
public:
    explicit conductor(QObject *parent = 0);//!< Constructor, gets normally the wsnode as parent.
    WaterCell *source, *target;
    double conduct(void);/**< conduct of water per time unit. The call to this function triggers evaluation of
                          * conductance changes and adapts time change rates of source and target cells (if necessary).
                          * conduct is calculated as \f$j_c = \frac{\psi_s - \psi_t} {d_s/k_s + d_t/k_t}\f$. Overfull source cells
                          * release water potential free. If source has high potential (around 0) water can enter target with saturated
                          * conductivity, meaning it is not bound to conducting waterways. */
    double cond(void){return conductance;}
    double tempConduct(void);/**< conduct of thermal energy per unit of time. \f$\si{\joule\per\second\per\square\meter}\f$*/
    void checkEnds(void);/**< evaluate whether conductor is active or not. */
    void checkSwitch(void);/**< test for switching conductor on or off. */
    void unSwitch(){switchMe = false;}/**< supress switching if it was activated before. */
    bool isItActive(void){return isActive;}
    void setKsat_target(double ksat){lKsat_target = ksat;}
    double ksat_target(void){return lKsat_target;}


signals:

public slots:
    void noteSwitch(bool state);//!< get signal to switch if necessary
    void sourceEmpty(bool state);
    void targetFull(bool state);
    void setCondSource(double theCondSource){condSource=theCondSource;isValid=false;}//!< adjust conductivity of source WaterCell
    void setCondTarget(double theCondi){condTarget=theCondi;isValid=false;}//!<  adjust conductivity of target WaterCell
    void setDistSource(double theDistSource){distSource=theDistSource;isValid=false;}//!< adjust conductivity of source distance \f$d_s\f$
    void setDistTarget(double theDistTarget){distTarget=theDistTarget;isValid=false;}//!< adjust conductivity of target distance \f$d_t\f$
    void setPotSource(double thePot){potSource = thePot; isValid=false;}//!< adjust potential of source \f$\psi_s\f$
    void setPotTarget(double thePot){potTarget = thePot; isValid=false;}//!< adjust potential of target \f$\psi_t\f$
    void setGradientZ(double theGradient){gradient_z = theGradient;isValid=false;}//!< set gravity
    void setTempSource(double theTemp){tempSource = theTemp;}//!< set sourceTemperature
    void setTempTarget(double theTemp){tempTarget = theTemp;}//!< set targetTemperature
    void setTempCondSource(double theCond){tempCondSource = theCond;}//!< set source Thermal conductivity
    void setTempCondTarget(double theCond){tempCondTarget = theCond;}//!< set target thermal conductivity
    void conductEnergy(double timestep);




private:
    double conductance, gradient_z, thermalConduct;
    double sat_source, lKsat_target;
    double condSource, condTarget, distSource, distTarget, potSource=0, potTarget=0, tempSource, tempTarget,tempCondSource, tempCondTarget;
    bool isActive, switchMe, isValid;
    Q_DISABLE_COPY(conductor)
};

#endif // CONDUCTOR_H

