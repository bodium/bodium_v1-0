#include "watercell.h"
#include <math.h>

WaterCell::WaterCell(QObject *parent) : QObject(parent)
{
    lActRate = 0;

}
double WaterCell::horConduct(void)
{
    if(radius < 0){
        return(static_cast<wsnode*>(parent())->horConduct());

    }
    return lKappa;
}
double WaterCell::conduct(void)
{
    if(radius < 0){
        return(static_cast<wsnode*>(parent())->conduct());

    }

    //double thick = static_cast<wsnode*>(parent())->nodeThick();
    if(lCapacity == 0.0)return lKappa;
    if(CellName=="Rain"||CellName=="Evi"||CellName=="Outlet")
        return lKappa;
    return lCapacity>=lTheta?lKappa*lTheta:lKappa*lCapacity;
}
void WaterCell::addToChange(double ch_rate)
{
    //if(ch_rate != 0)
        lActRate += ch_rate;
    //if(lActRate<0&&CellName=="Outlet")
      //  lActRate+=0.1;
    //if(fabs(lActRate) < 1e-19)
      //  lActRate = 0;
}

void WaterCell::setPsi(double psi)
{
    if(psi<-1e8)
        printf("schmarrn!");
    //if(fabs(lPsi_d-psi)>1e-15){
    lPsi_d =lPsi_s =psi;
    emit psiChanged(psi);
    //}
}
void WaterCell::setDeltaTheta(double delta)
{
    lActRate = delta;
}
void WaterCell::doTimeStep(double time_t)
{
    double t_horCo;
    if(fabs(lActRate) >1e-10 && isMatrix)
      //  printf("Fuck!");
        t_horCo=3;

    lTheta += time_t*lActRate;
    if(lTheta < 0)lTheta = 0;
    t_horCo = horConduct();
    if(fabs(t_horCo - lastHorCond) > 1e-12 ){
        emit horConductivityChanged(t_horCo);
        lastHorCond = t_horCo;
    }
    t_horCo = conduct();
    if(fabs(t_horCo - lastCond) > 1e-12){

        emit conductivityChanged(t_horCo);
        lastCond = t_horCo;

    }
    if(!isMatrix){
    if(lTheta <=1e-12){
        if(!isEmpty){
            emit goneEmpty(isEmpty = true);
        }
        if(isFull)
            if(lCapacity > 0)
                emit goneFull(isFull = false);

    }else if(lTheta>=lCapacity){
        if(!isFull)
            emit goneFull(isFull = true);
        if(isEmpty)
            emit goneEmpty(isEmpty = false);

    }else{
        if(isFull)
            if(lTheta < lCapacity*0.99)
                emit goneFull(isFull = false);
        if(isEmpty)
            if(lTheta > lCapacity*.01)
                emit goneEmpty(isEmpty = false);

    }
    }

}

double WaterCell::linearTime()
{
    double retval,testval;
    if(isEmpty && lActRate > 1e-12)
        retval = lCapacity*.2/lActRate;
    else if(isFull){
        if(lActRate < -1e-12)
            retval = -lCapacity*.3/lActRate;
        else
            retval = 1e307;

    }else{
        testval = lActRate>0?(lCapacity - lTheta)/lActRate:lActRate < 0?-lTheta/lActRate:1.7e307;
        retval = lActRate>0?fabs(lCapacity/lActRate)*0.1:1.7e307;
        retval = testval>retval?retval:testval;
    }
    if(fabs(lActRate)<1E-12)
        retval = 1e307;
    if(retval < minTimeStep)
        retval = minTimeStep;
    return retval;//>minTimeStep? retval:retval < 0?1e107:minTimeStep;

}

void WaterCell::init(double capa, double resist, double cosGamma, double psid, double psis, double i_theta, double i_kappa_l, double i_kappa_h)
{
    lCapacity = capa;
    if(capa >0&& capa <1e3)
        lKappa = resist/capa;
    else
        lKappa = resist;
    lPsi_d = psid;
    lPsi_s = psis < 0?psis:psid;
    radius = lPsi_d <0?-290/cosGamma/(lPsi_d +lPsi_s)/1000:0.01; //J/m²*2
    lTheta = i_theta;
    isEmpty = lTheta <= 0.00001;
    isFull = lTheta >= lCapacity*0.999;
    lActRate = 0;
    lKappa_high =i_kappa_h > 0? i_kappa_h: capa;
    lKappa_low =i_kappa_h > 0? i_kappa_l: capa;
}
void WaterCell::setTheta(double tet)
{
    double t_horCo;
    lTheta = tet;
    if(!isMatrix){
    if(lTheta <=0){
        if(!isEmpty)
            emit goneEmpty(isEmpty = true);
        if(isFull&&lCapacity>0)
            emit goneFull(isFull = false);
    }else if(lTheta>=(lCapacity-lIce)){
        if(!isFull)
            emit goneFull(isFull = true);
        if(isEmpty)
            emit goneEmpty(isEmpty = false);

    }else{
        if(isFull)
            if(lTheta < lCapacity*0.99)
                emit goneFull(isFull = false);
        if(isEmpty)
            if(lTheta > 0.00001)
                emit goneEmpty(isEmpty = false);

    }
    }
    t_horCo = horConduct();
    if(fabs(t_horCo - lastHorCond) > 1e-12 ){
        emit horConductivityChanged(t_horCo);
        lastHorCond = t_horCo;
    }
    t_horCo = conduct();
    if(fabs(t_horCo - lastCond) > 1e-12){

        emit conductivityChanged(t_horCo);
        lastCond = t_horCo;

    }
}
void WaterCell::setCapacity(double capa)
{
    lCapacity = capa;
    if(!isMatrix){
    if(lTheta <=0){
        if(!isEmpty)
            emit goneEmpty(isEmpty = true);
        if(isFull)
            if(lCapacity > 0)
                emit goneFull(isFull = false);

    }
    if(lTheta>=lCapacity){
        if(!isFull)
            emit goneFull(isFull = true);
        if(isEmpty)
            emit goneEmpty(isEmpty = false);

    }else{
        if(isFull)
            if(lTheta < lCapacity*0.99)
                emit goneFull(isFull = false);
        if(isEmpty)
            if(lTheta > lCapacity*.01)
                emit goneEmpty(isEmpty = false);

    }
    }
}

bool WaterCell::full()
{
    bool retval;
    if(isMatrix)
        return(false);
    //retval = lTheta >= lCapacity;
    return(isFull);
}
bool WaterCell::empty()
{
    //bool retval;
    if(isMatrix)
        return(false);
    //retval =lTheta<=0?true:false;
    return(isEmpty);
}

double WaterCell::freezeMelt(double water)
{
    if(water < 0){//freezing
        if(water < -lTheta)
            water = -lTheta;
    }else if (water > lIce) {
        water = lIce;
    }
lIce -= water;
lTheta += water;
return water;
}
void WaterCell::stressMe(double stressRate)
{
    double kappa_A = fmin(lKappa_high, lCapacity);
    double Capa_new = exp(stressRate)*(kappa_A - lKappa_low) + lKappa_low;
    Capa_new = Capa_new >=0?Capa_new:0;
    setCapacity(Capa_new);

}
