#ifndef PLANT_STAND_H
#define PLANT_STAND_H

#include <QObject>
#include "soilnode.h"
#include <math.h>

#define no_dev 5 //number of developement stages, default is 5

/*! \brief Class for simulating crop stock
 *
 * includes processes for assimilate distribution, photosynthesis, maintenance, and growth \n
 * calculates also root biomass growth; explicit growth distribution in soilnode (?) \n
 * needs information from soilnode about soil water, temperature, nitrogen
 *
 * mainly based on SPASS and SUCROS (Expert-N)
 */

class Plant_stand : public QObject
{
    Q_OBJECT
public:
    explicit Plant_stand(QObject *parent = nullptr);
    void configure(const QJsonObject &theConfig);
    void config(const QJsonObject &theConfig);
    double getDevstage(void){return s_dev;} /**< @return Current developement stage */
    /**
     *\brief Sets one of five different developement stages
     *
     * @param n_s_dev new stage out of 5 stages: (i) sowing -1 (ii) germination -0.5 (iii) emergence 0 (iv) flowering 1 (v) ripening 2
     */
    void setDevstage(double n_s_dev){s_dev=n_s_dev;}
    void setReserve(double n_res_stm){
        reserve_values[1]=reserve_values[1]+n_res_stm;
        reserve_values[0]=reserve_values[1]+reserve_values[2];
    }
    void setAssim(double assi_dist[], double assi_n){
        for (int i=0;i<5;i++){assim_values[i]=assi_dist[i]*assi_n;}
    }
    /**
     * \brief Calculation of current developement stage and rate
     * @param water_psi Water potential in soilnode of specific sowing depth (seed_depth)
     * @param soil_temp Soil temperature in soilnode of specific sowing depth (seed_depth)
     * @param temp Daily air temperature from external climate data
     * @param exposure photoperiod [d] from external climate data
     * @return development rate
     */
    double calcDevstage(double water_psi, double today, double soil_temp,double temp, double exposure, double timeStep);
    /**
     * \brief Distribution of assimilates to roots, leaves, stem, and storage organs
     *
     * @param new_assimi Amount of newly builded assimilates
     * @param water_stress to be calculated with current daily transpiration and potential daily transpiration (p. 229): \f$TR^{day}_{act}/TR^{day}_{pot}\f$
     */
    void assimilateDist(double water_stress, double new_assimi);
    /**
     * \brief Calculation of temperature function with optimum
     * @param T current temperature
     * @param Tmin minimum needed temperature
     * @param Topt optimal temperature
     * @param Tmax maximum bearable temperature
     */
    double tempFunc_Optima(double T, double Tmin,double Topt,double Tmax );
    /**
     * @brief Interpolation function
     * @param x current x value for whom y value should be interpolated
     * @param vec_x known x values used for interpolation
     * @param vec_y known y values used for interpolation
     * @return interpolated y value
     */
    double interpolList(double x, int size, QList<double> list_x, QList<double> list_y);

    double getNfactor(double n_act,double n_min, double n_opt){
        double n_fac=0;
       // n_fac=((n_act-n_min)/(n_opt-n_min));
        //Monica
        n_fac=1-exp(n_min-(5*((n_act-n_min)/(n_opt-n_min))));
        n_fac=(n_fac<0)?0:n_fac;
        return n_fac;
    } /**< @return reduction function for nitrogen \f[f_N=(\nu-\nu^{min})/(\nu^{opt}-\nu^{min})\f] with \f$\nu[kg kg^{-1}]\f$ the current specific nitrogen amount, \f$\nu^{min}[kg kg^{-1}]\f$ the minimal nitrogen amount and \f$\nu^{opt}[kg kg^{-1}]\f$ the optimal nitrogen amount */

    /**
     * @brief Calculation of photosynthesis based on SPASS
     * @param radiation Global radiation
     * @param Cex external \f$CO_2\f$ concentration in the air
     * @param temp Daily air temperature from external climate data
     * @return daily photosynthesis rate of stock
     */
    double photosynthesis(double radiation, double Cex, double temp, double timeStep, double water_stress);
    /**
     * @brief Maintenance and biomass growth
     * @param temp Daily air temperature from external climate data
     * @param photo daily photosynthesis rate of stock
     * @param dev_rate development rate calculated in \link calcDevstage() \endlink
     * @return root length growth rate which is delivered to soilnode for calculating root growth
     */
    double growth(double temp, double photo, double dev_rate, double timeStep);
    void setRootBio(double root_bio){biomass_values[4]=root_bio;}

    /**
     * @brief Optical coverage of plant stand
     * @return proportion of optical coverage by plants
     */
    double coverage(void);
    /**
     * @brief Optical reflectance dependent on plant development stage
     * @return proportion of optical reflectance
     */
    double plantAlbedo(void);
    double seedDepth(void){return seed_depth;}
    double rts_length=0;

    double nitro_demand[5]={0,0,0,0,0}; //current nitrogen demand for [whole offspring, leaves, stem, storage organs, roots] [kg_N m-2]
    double l_trans_pot=0.3;
    double biomass_values[5]={0,0,0,0,0}; //current living dry biomass in [whole offspring, leaves, stem, storage organs, roots] in kg m-2
    double mean_root_dia=5e-4;              //mean root diameter in m?
    double depth_gro_max=2.314815e-07;   //maximum root depth growth rate [m s-1], 20 mm d-1 in Groot 1987 p 29, is 2.2 mm d-1 for wheat (expertN p-201)
    double pen_depth=1.2;       //maximum penetration depth [m] crop specific
    double mean_length=10.5;             //crop specific: mean length of one kg root [m kg-1] e.g. wheat 1.05*10^5, maize 0.8 10^5
    double root_cn=20;
    double exudates;
    double n_lim=0.0004;
    bool growth_season=false;
    double nfix_rate=0.3;
    double fertilizeOrg=0;
    double cn_var=0;


signals:
    void seed(double);
    void rootdemand(double demand);
    void transpi(double transpi);
    void totaldepth(double);
    void ferti(double,double,double);       //N03, NH4, depth
    void manure(double,double);             //FOM, depth
    void straw(double,double,double);             //FOM, depth, cnratio
    void rootharv(double,double);
    void setfirstroot(double);
    void rtl_fac();
    void till_event(double); //depth

public slots:
    void doTimeStep(double timeStep);
    /**
     * @brief Update of nitrogen amount in the different plant organs after uptake (see \link SoilProfile::(calcNuptake(void)) \endlink)
     * @param total_n_dem total nitrogen uptake over whole soil profile
     */
    void updateN(double total_n_up);
    void setETP(double ETP){l_trans_pot =ETP*coverage();}
    void setPAR(double PAR){l_radiation = PAR;}
    void setPhotoperiod(double photoperiod){l_photoperiod = photoperiod;}
    void setTemp(double tempera){l_temp=tempera;}

private:
    bool fert_straw=true;
    double harv_stage=2;

    double l_temp=10;                         //dummy, need this from weather data
  //  double l_radiation=0.001736111;           //dummy, need this from weather data, KJ m-2s-1 ~ 150 KJ m-2d-1
    double l_radiation=0.034;           //dummy, need this from weather data, KJ m-2s-1 ~ 3000 KJ m-2d-1
    double l_photoperiod=6;

    double l_seedday=lambda_seedday;
    double l_harvest = lambda_harvest;

    bool c4=false;
    double s_dev=3.5;   // s_dev development stage, 3.5 to mark before sowing, 999 means failure
    double assim_values[5]={0,0,0,0,0}; //kgCO2 m-2 s-1 current assimilate in [whole offspring, leaves, stem, storage organs, roots]
    double assim_dist[5]={0,0,0,0,0};    //current assimilate distribution / proportion in [whole offspring, leaves, stem, storage organs, roots]
    double nitro_values[5]={0,0,0,0,0}; //current nitrogen, relative, in [whole offspring, leaves, stem, storage organs, roots] [kg_N kg-1]
    double nitro_values_m[5]={0,0,0,0,0}; //current nitrogen, total, in [whole offspring, leaves, stem, storage organs, roots] [kg_N m-2]
    double reserve_values[3]={0,0,0};  //reserve substances in [whole plant, stem, storage organs]
    double r_growth[5]={0,0,0,0,0};       //biomass growth rate of [whole offspring, leaves, stem, storage organs, roots]
    double LAI=0;               //leaf area index -  (starts with 0.4?)
    double nvd=0;                  //amount of vernalisation days = days till germination
    double nitro_rich_tot=0;        //total enriched nitrogen?


    double seed_depth=0.05;  // crop specific sowing depth [m]
    double offspring_rate=1.736111E-7;   //Sproßlängenwachstumsrate m s-1 (15 mm d-1 for wheat or barley, 4 for potatoes
    double psi_pwp=-12000;   // water potential at permanent welking point
    double veg_max=2.314815e-07;        // crop specific: maximum developement rate vegetative phase [s-1] e.g. 0.02 d-1 for wheat (penning table 12)
    double gen_max=3.18287e-07;        // crop specific: maximum developement rate generative phase [d-1] 0.0275 d-1 for wheat
    double t_min_veg=-10;   // crop specific: minimum temperature vegetative phase (wheat penning)
    double t_max_veg=35;   // crop specific: maximum temperature vegetative phase
    double t_opt_veg=25;   // crop specific: optimum temperature vegetative phase
    double t_min_gen=10;   // crop specific: minimum temperature generative phase ?? is this correct?
    double t_max_gen=35;   // crop specific: maximum temperature generative phase
    double t_opt_gen=20;   // crop specific: optimum temperature generative phase
    double t_min_photo=0;   // crop specific: minimum temperature for photosynthesis ACHTUNG werte geraten ;-)
    double t_max_photo=40;   // crop specific: maximum temperature for photosynthesis
    double t_opt_photo=20;   // crop specific: optimum temperature for photosynthesis
    double daytype=1;           //planttype, either 1 for long-day plants (LDP) or -1 for short-day plants (SDP)
    double exposure_opp=0.738;         //maximum (SDP) or minimum (LDP) photoperiod [d]; value for wheat, barley from ExpertN Doku p.214
    double nvmin=0;       // crop specific: minimum vernalisation days ACHTUNG werte geraten ;-)
    double nvmax=0;       // crop specific: maximum vernalisation days
    double photo_sens=0;        //crop specific: sensitivityfactor [d-1] ACHTUNG komplett geraten bzw. geschlussfolgert, finde Wert aber seltsam siehe Kommentar im Code calcDevstage()
    double photo_max=1.111111e-06;               //maximum photosynthesis rate at CO2 concentration of 340 vppm [kgCO2 m-2s-1], wheat: 40 kg ha-1h-1, Table 4 penning de vries
    bool stomReg=false;                   //plant without (0) or with (1) stomatic regulation
    double a=0.9;                       //relation between internal and external CO2 concentration; 0.7 for C3, 0.4 for C4
    double spec_extinct=0.6;         //crop specific: extinction coefficient - wheat 0.6 from p.244 ExpertN doku, but this is the effective extinction coefficient for diffuse light..

    double maint_ref[5]={6.365741e-7,3.472222e-7,1.736111e-7,1.157407e-7,1.736111e-7};    //kg CO2 kg-1s-1 maintenance respiration of [whole offspring, leaves, stem, storage organs, roots] at reference temperature: {0.055,0.03,0.015,0.01,0.015} kgCO2 kgb d-1 at 25 degree groot 1987
    double temp_ref=25;     //??reference temperature??
    double Gamma=50;                //CO2 compensation point at optimal temperature, C3= 50, C4=5
    double Gamma_start=38;              //CO2 compensation point temperature at reference temperature, 0 for C4 plants (no photorespiration)
    double resp_c[5]={0,0.461,0.408,0.347, 0.406};   //CO2 production factors for [whole offspring, leaves, stem, storage organs, roots], values for wheat penning table 11
    double total_resp=0;

      // new as lists as different crops have different numbers of dev stages (penning de vries, table 18)
    // assimilate distribution and developement stages need to be same length - catch error!!
    QList<double> assi_off;                 //crop specific: assimilate distribution key for entire shoot
    QList<double> assi_off_dev;             //for different developement stages
    QList<double> assi_leaf;                //crop specific: assimilate distribution key for leaves
    QList<double> assi_leaf_dev;             //for different developement stages
    QList<double> assi_stem;                //crop specific: assimilate distribution key for stem
    QList<double> assi_stem_dev;             //for different developement stages

    double nitro_min[5]={0.015,0.005,0.005,0.005,0.005}; //crop specific: minimum n amount in [whole offspring, leaves, stem, storage organs, roots] ACHTUNG werte geraten ;-)
    double nitro_opt[5]={0.4,0.1,0.1,0.1,0.1}; //vorher 0.02 .... crop specific: optimum n amount in [whole offspring, leaves, stem, storage organs, roots] - depends on dev stage? ACHTUNG werte geraten ;-)

    double n_a=1.35;                 //for Monica Version / EU Rotate N / Rahn et al 2010
    double n_b=3;
    double n_lux=1.2;
    double n_lux_f=1.5;
    double exud_rate=0.1;

    double fertilizeN=2e-2;

    double weight_lvs_avg=0.05;  //mean weight of leaf (dry mass) [kg m-2]; between 200-800; wheat 500, winter weat 425, barley 325 kg ha-1(Penning de Vries, Table 19)

    QList<double> rel_weight_lvs;           //specific leaf weight relative to weight_lvs_avg as a function of
    QList<double> lvs_dev_stage;            // these development stages

    int crop_year=1;
    QList<QString> crop_rot;
    bool monocrop=true;
    bool managefile=false;
    int mancount=0;
    QDate eventdate;
    QJsonObject eventObject;
    QString cropsFiles="/crops/";

    double seed_bio=0.015;                 //start biomass of seed kg m²
    double seed_n=0.01;                    //start N concentration in seed kg kg-1
};

#endif // PLANT_STAND_H
