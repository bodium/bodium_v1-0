#include "boundary.h"
#ifdef WITH_GUI
#include <QMessageBox>
#endif

#include <QDir>

Boundary::Boundary(QObject *parent):QObject(parent)
{

}
Boundary::Boundary(QObject *parent, const QJsonObject &theInit): QObject(parent)
{
    //Weather is read in through separate file

    QFile weatherFile(theInit["WeatherFile"].toString());
    if (!weatherFile.open(QIODevice::ReadOnly)) {
            qWarning("Couldn't open weather file.");
        }
    inLines.setDevice(&weatherFile);
}
void Boundary::configure(const QJsonObject &theInit)
{
    //Weather is read in through separate file
    weatherFile.close();

    weatherFile.setFileName(theInit["WeatherFile"].toString());
    if (!weatherFile.open(QIODevice::ReadOnly)) {
            qWarning("Couldn't open weather file.");
#ifdef WITH_GUI
            QMessageBox msgBox;
            msgBox.setText("Weather file not found, please check path in parameter window");
            msgBox.exec();
#endif

        }
    inLines.setDevice(&weatherFile);

    QJsonObject manInit = theInit["Management"].toObject();
    prec_per = manInit.contains("prec_var")? manInit["prec_var"].toDouble():prec_per;

}

void Boundary::doTimeStep(double step)
{
    double evaAlbedo, eVapor;
    //read in Date, Temperature, radiation part of evaporation per second, saturation deficit part of evaporation per second, precipitation per second, duration of potential sunshine, radiation per second
    QDate theTime;
    QString timeString;
    QDate theCurTime=(static_cast<bodiumrunner *>(parent())->myTimer.currentModelTime.date());
    do{
        inLines>>timeString>>tempVal>>evaAlbedo>>eVapor>>rainVal>>photoperiod>>radiation;
        theTime = QDate::fromString(timeString, "yyyy-MM-dd");

     }while (theTime<theCurTime);

    if(theTime>(static_cast<bodiumrunner *>(parent())->myTimer.currentModelTime.date())){
        qWarning("Weather file has wrong or missing values, simulation is aborted");

#ifdef WITH_GUI
        QMessageBox msgBox;
        msgBox.setText("Weather file has wrong or missing values, simulation is aborted");
        msgBox.exec();
#endif
        exit(5);

    }
    if(QDate::fromString("1979-04-09", "yyyy-MM-dd") == static_cast<bodiumrunner *>(parent())->myTimer.currentModelTime.date()){
        printf("Action");
    }
    evaVal=(eVapor + evaAlbedo*(1-albedo()))*step;
    rainVal=rainVal*step*(1+prec_per);
    radiation = radiation*step*1000;

    tempVal=(static_cast<bodiumrunner *>(parent())->landtrans)?lt_tempVal:tempVal;

    emit(precipitation(rainVal));
    emit(etp(evaVal));
    emit(temperature(tempVal));
    emit(photo(photoperiod));
    emit(par(radiation));
}
void Boundary::setTranspi(double transpiro)
{
    transpi = transpiro;
    emit eva(evaVal - transpi);
}

double Boundary::albedo()
{
    Plant_stand *thePlant = &(static_cast<bodiumrunner *>(parent())->thePlantStand);
    double cover = thePlant->coverage();
    return thePlant->plantAlbedo()*cover + static_cast<bodiumrunner *>(parent())->theProfile.soilAlbedo()*(1-cover);
}
