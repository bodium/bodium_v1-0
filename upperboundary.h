#ifndef UPPERBOUNDARY_H
#define UPPERBOUNDARY_H

#include <QObject>
class SoilProfile;

/**
 * @brief Defines upper boundary
 *
 */

class upperBoundary: public QObject
{
    Q_OBJECT
public:
    explicit upperBoundary(QObject *parent = 0);
public slots:
    private:
    SoilProfile *theProfile;
    double timeX;
};



#endif // UPPERBOUNDARY_H
