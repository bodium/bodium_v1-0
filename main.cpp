/***************************************************//**
* Modeling soil as a natural resource requires the
*
*
*
*
******************************************************/
#ifdef WITH_GUI
#include "bodiumwindow.h"
#endif
#include "bodiumrunner.h"
#include <QObject>
#include <QWidget>

#include <QApplication>
#include <QCommandLineParser>
QCommandLineParser optionParser;

int main(int argc, char *argv[])
{

#ifdef WITH_GUI
    QApplication a(argc, argv);
    QApplication::setApplicationName("Bodium");
    QApplication::setApplicationVersion("0.7071068");
#else
    QCoreApplication a(argc, argv);
    QCoreApplication::setApplicationName("Bodium");
    QCoreApplication::setApplicationVersion("1.0");
#endif
    optionParser.setApplicationDescription("Bodium, the complete soil simulator.");
    optionParser.addHelpOption();
    optionParser.addVersionOption();
    optionParser.addPositionalArgument("initfile", QCoreApplication::translate("main", "Initialisation file."));
    optionParser.process(a);

    bodiumrunner r;

#if !defined(WITH_GUI) && !defined(MAKE_LIB)
    r.run();
#endif

    double bvb=9;
   return a.exec();
}
