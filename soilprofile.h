#ifndef SOILPROFILE_H
#define SOILPROFILE_H

#include "wsnode.h"

#include "upperboundary.h"
#include <QJsonObject>
#include <QTextStream>
#include <QFile>

class SoilNode;

/**
 * @brief Soil profile consisting of several SoilNode
 */

class SoilProfile : public QObject
{
    Q_OBJECT
public:
    explicit SoilProfile(QObject *parent = 0);
    void init();
    void StructChange(const QJsonObject &theInit);
    /** central working beast. At each time step, determine matrix potentials,
     * evaluate duration of stable fluxes, and iterate over time until time step is completed.
     * Determination of matrix potentials: each soil node is requested to update its matrix
     * potential to balance the fluxes. If the potential changes, it emits a signal, which is received by
     * the waterProfile. This step is reiterated until no changes are signalled.
     * Then the thermal fluxes are calculated.
     * */
    QList<wsnode*> theWNodes;
    QList<conductor*> theConducts;
    WaterCell *rainy, *evapy;
    QString getVals(void);
    double getChangeSum(void);
    double getTransAct(void){return lTrans_act;}
    void setTransAct(double);
    void resetChangeRates(void);
    double getRootDepth(void){
        return rootdepth;}
    void addRootDepth(double new_depth){rootdepth+=new_depth;}
    void setEvaRain(double rain, double evapo, double ttemp);
    void setH2(double trans_pot){h2=(trans_pot<transp_low)?h2L:((trans_pot>=transp_low)&&(trans_pot<=transp_high))?(h2L+((transp_high-trans_pot)/(transp_high-transp_low))*(h2H-h2L)):h2H;}
    double getH2(void){return h2;}
    double getH3(void){return h3;}
    /**
    *@brief Calculation of water reduction function based on matric potential
    */
    double waterReduc(double psi);

    void setSumFtRtl(double newFactor){sum_factor_rtl=newFactor;}
    void setDepthLoad(void);
    void setRootFrac(void);
    /**
     * @brief Caclulation of nitrogen uptake based on simple approach of WAVE
     */
    void calcNuptake(double timeStep);
    /**
     * @brief Calculation of water uptake by roots based on approach of SWATRE
     * @param trans_pot Potential transpiration
     */
    void calcWuptake(double transp_pot);
    /**
     * @brief Calculation of root-available nitrogen
     * @param rootingdepth Depth of rooting system
     */
    double availableN(double rootingdepth);
    double sumVolume(QString theName);
    double getPlantPsi(double sow_depth);
    double getPlantTemp(double sow_depth);
    void calcNtransport(double timeStep);
    double soilAlbedo();
    double leachate(void);
    void setOutdepths(QList<double> depth_list){out_depth=depth_list;}
    void setSumOut(double sum){sumOut=sum;}
    double getNout(void){return nout;}
    int mixing(double);

    QList<SoilNode*> theNodes;
    upperBoundary theUpper;
    double actRoot(void);
    double rootdepth=0;           //m
    double sum_factor_rtl=0;
    double h2=0;
    double rootVol=0;
    bool explicit_microbes=false;
    double nout=0;

signals:
        void stepTime(double timestep);
        void demandN(double n_demand);

public slots:
    void rootRequest(double request);
    void doSeed(double);
    void setNodes(const QJsonObject &jsonNodes);
    void matrixChanges(double matrix){matrixChanged = true;}
    void doTimeStep(double step);
    void setTemp(double tempi);
    void setEva(double theEva);
    void setRain(double transpi);
    void deleteNodes(void);
    void rootLengthFactor(void);
    void organic_fert(double,double);
    void fertilize(double,double,double);
    void straw_fert(double,double,double);
    void doOutput(void);
    void tillage(double);

private:
    double residualRain, residualEva;
    QTextStream inLine;
    QFile theWaterFile;
    QTextStream outWaterStream;
    bool matrixChanged;
    bool groundwater = false;
    wsnode *boundary;
    double h3=-1500000;            //values from Wesseling and Brandyk, 1985, Pa, except h3, Hydrus Doku S. 109
    double h2L=-80000;
    double h2H=-20000;
    double h1=-2500;
    double h0=-1000;
    double transp_low=1;          //lower potential transpiration rate
    double transp_high=5;         //higher potential transpiration rate
    double theSoilWetAlbedo = 0.05;
    WaterCell* theOutlet;
    double lTrans_act=0;           //m s-1 actual transpiration, is calculated in calcWuptake; needed in plant stand to calculate water stress

    double actualTime=0;
    bool waterOut= false;
    QList<double> out_depth;
    double sumOut=1000;


};

#endif // SOILPROFILE_H
