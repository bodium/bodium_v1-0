#include "conductor.h"

conductor::conductor(QObject *parent) : QObject(parent)
{

    switchMe = isActive = isValid = false;
    conductance = 0;
}


void conductor::checkEnds()
{
    if(!source->empty() && !target->full()){
        isActive = true;
    }else{
        isActive = false;
    }

}

void conductor::noteSwitch(bool state)
{
    switchMe = true;

}

void conductor::conductEnergy(double timestep)
{
    double amount, minCapa, Capa2;
    thermalConduct= 0;
    minCapa =source->heatCapacity();
    Capa2 = target->heatCapacity();
    minCapa = minCapa<Capa2?minCapa:Capa2;
    minCapa *=.5;
    if(tempSource > tempTarget){
        thermalConduct = timestep/(distSource/tempCondSource + distTarget/tempCondTarget);
        thermalConduct = thermalConduct>minCapa?minCapa:thermalConduct;
    }
    thermalConduct += timestep*conductance*4200;
    amount = (tempSource-tempTarget)* thermalConduct;
    source->addHeat(-amount);
    target->addHeat(amount);

}


void conductor::checkSwitch()
{
    if(!source->empty() && !target->full()){
        if(!isActive){
            source->addToChange(-conductance);
            target->addToChange(conductance);
            isActive = true;
        }

    }else if(isActive){
        source->addToChange(conductance);
        target->addToChange(-conductance);
        isActive = false;
    }
      //  switchMe = false;

    //}
}

double conductor::conduct()
{
    double conductChange;
    double meanCond;
    double tConductTarget;
    double l_potSource = potSource;
    if(isActive){
    //if(!isValid){
       // if(source->full()){//check for overfull source, it will release water freely
       //     if(source->capacity()<source->theta()*0.9)
       //         l_potSource = 0;
       // }


       // double gradi = (l_potSource - potTarget)/(distSource+distTarget) - gradient_z;//Potential gradient
        double gradi = (potSource - potTarget)/(distSource+distTarget) - gradient_z;//Potential gradient

        if(source->empty()||target->full()||gradi<0){
            conductChange= 0;
        }else{
            // Pa/m * m/s / (1000 kg/m³) / (9.81 N/kg)
            // N/s m/N 1/9810
            //m/s 1/9810
            //kg/m²s 1/9.81
//            if(l_potSource>-10)// free water can infiltrate full pore space, not only follow waterfilled waterways.
//                tConductTarget = l_potSource >= 0?lKsat_target:((10+l_potSource)*lKsat_target -l_potSource*condTarget)/10;
            if(potSource>-50)
                tConductTarget = potSource >= 0?lKsat_target:potSource>-10?((10+potSource)*lKsat_target -potSource*condTarget)/10:condTarget;
            else
                tConductTarget = condTarget;
            //if(gradient_z < 0)
            //geometric mean
               // meanCond = exp((distSource*log(condSource) + distTarget*log(condTarget))/(distSource+distTarget));
            //else
                // harmonic mean
            meanCond = (distSource+distTarget)/(distSource/condSource + distTarget/tConductTarget);

            conductChange = condSource>0.0?condTarget>0.0? \
                      gradi*meanCond/9.81:0:0;
        }
//check
        if(source->matrix()&& target->matrix()){
            gradi +=1;
            gradi-=1;
        }
        if(conductance < 0)
            gradi+=0;

        conductChange-=conductance;
        source->addToChange(-conductChange);
        target->addToChange(conductChange);


        conductance += conductChange;

        isValid = true;

    return conductance;

    }

    return(0);
}

void conductor::sourceEmpty(bool state)
{
    if(state)
    {
        source->addToChange(conductance);
        target->addToChange(-conductance);
        isActive = false;
        conductance = 0;
    }else{
        if(!target->full())
                isActive = true;
        //conduct();
    }
}
void conductor::targetFull(bool state)
{
    if(state)
    {
        source->addToChange(conductance);
        target->addToChange(-conductance);
        isActive = false;
        conductance = 0;
    }else{
        if(!source->empty())
            isActive = true;
        //conduct();
    }
}

double conductor::tempConduct(void)
{
    thermalConduct= 0;
    if(tempSource > tempTarget)
        thermalConduct = (tempSource-tempTarget)/(distSource/tempCondSource + distTarget/tempCondTarget);
    thermalConduct += (tempSource-tempTarget)* conductance*4200;
    return thermalConduct;
}
