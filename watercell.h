#ifndef WATERCELL_H
#define WATERCELL_H

#include <QObject>
#include "wsnode.h"
#define minTimeStep 1
class WaterCell : public QObject
{
        Q_OBJECT
public:
    /***********************************//**
     * @brief WaterCell constructor
     * @param parent object, should be wsnode
     */
    WaterCell(QObject *parent = 0);
    /***********************************//**
     * @brief init initialisation of watercell parameters
     * @param capa capacity of watercell with given proe size
     * @param resist obsolete: specific resistance of water movement. If feasible this will be determined from pore size.
     * @param cosTheta empirical parameter for contact angle.
     * @param psid desorption potential
     * @param psis sorption potential, if omitted, same as desporption.
     * @param i_theta initial water content, default is 0
     * @param i_kappa_low lower threshold for stress compression
     * @param i_kappa_high upper threshold for stress compression (if 0, compression is not considered, i.e. all thresholds equal current capa
     *
     * The initialisation sets the parameters for the given water cell,
     * i.e. the capacity of a node to store water in a certain pore size class.
     * The pore size is determined from the average of sorption and desorption potential
     * and corrected by a possible contact angle parameter.
     */
    void init(double capa, double resist, double cosTheta, double psid, double psis = 0, double i_theta = 0, double i_kappa_low = 0, double i_kappa_high = 0);
    /******************************************//**
     * @brief capacity actual water capacity of pore size class.
     * @return capacity in [m]
     */
    double capacity(void){return lCapacity;}
   // double capacity(void){return lCapacity-lTheta;}
    /**********************************//**
     * @brief theta actual water saturation
     * @return water content in [m]
     */
    double theta(void){return lTheta;}
    /**********************************//**
     * @brief ice actual ice saturation
     * @return ice content in [m]
     */
    double ice(void){return lIce;}
    /**********************************//**
     * @brief freezeMelt change actual ice saturation
     * @param water amount of water to melt (positive sign) or freeze (negative sign)
     * @return frozen quantity [m]
     */
    double freezeMelt(double water);

    /**********************************//**
     * @brief kappa obsolete for conductivity
     * @return
     */
    double kappa(void){return lKappa;}
    /**********************************//**
     * @brief psi_d desorption potential: energy required to remove an infinitely small amount of water from the node.
     * @return potential in [kPa]
     */
    double psi_d(void){return lPsi_d;}
    /**********************************//**
     * @brief psi_s sorption potential: energy released by adding an infinitely small amount of water to the node.
     * @return potential in [kPa]
     */
    double psi_s(void){return lPsi_s;}
    /**********************************//**
     * @brief conduct conductivity of pore size class.
     * @return conductivity in [m/s]
     *
     * conductivity of pore size class. Defined by radius and water content. Further development
     * may include tortuosity, for the moment only parallel capillary bundle is considered.
     * Note that conductivity has to consider the node thickness, to get relative water content.
     */
    double conduct(void);
    /**********************************//**
     * @brief horConduct horizontal conductivity pof pore size class
     * @return conductivity in [m/s]
     *
     * conductivity of pore size class. Defined by radius and water capacity. Further development
     * may include tortuosity, for the moment only parallel capillary bundle is considered.
     * Note that conductivity has to not consider the node thickness, we need absolute water capacity. The
     * cpacity dependence is derived from the image of moving interfaces, and the surface of the interface being
     * dependent on the capacity per volume.
     *
     */
    double horConduct(void);
    /**********************************//**
     * @brief empty tells the caller whether this cell is empty.
     * @return is empty or not? [boolean]
     */
    bool empty(void);
    /**********************************//**
     * @brief full
     * @return
     */
    bool full(void);
    /**********************************//**
     * @brief setZeroChange switch off change of water content
     */
    void setZeroChange(void){lActRate = 0;}
    /**********************************//**
     * @brief name
     * @return string with Cell name
     */
    QString name(void){return CellName;}
    /**********************************//**
     * @brief setName
     * @param name
     */
    void setName(QString name){CellName = name;}

    void setMatrix(void){isMatrix=true;lActRate=0;isEmpty=false;isFull=false;}

    /**********************************//**
     * @brief linearTime evaluate the maximum time span without substantial change
     * @return maximum time in [s]
     */
    double linearTime(void);
    /**********************************//**
     * @brief deltaTheta current change rate
     * @return
     */
    double deltaTheta(void){return lActRate;}
    /**************************************//**
     * @brief addHeat
     * @param energy
     */
    void addHeat(double energy){static_cast<wsnode*>(parent())->addHeat(energy);}
    /**************************************//**
     * @brief heatCapacity
     * @return
     */
    double heatCapacity(void){return static_cast<wsnode*>(parent())->heatCapacity();}
    /**************************************//**
     * @brief setBoundary
     * @param whatsit
     */
    void setBoundary(bool whatsit){isBoundary=whatsit;}
    /**************************************//**
     * @brief boundary
     * @return
     */
    bool boundary(void){return isBoundary;}

signals:

    /**********************************//**
     * @brief goneEmpty signal if cell has run empty
     * @param direction
     */
    void goneEmpty(bool direction);
    /**********************************//**
     * @brief goneFull
     * @param direction
     */
    void goneFull(bool direction);
    /**********************************//**
     * @brief horConductivityChanged
     * @param newCond
     */
    void horConductivityChanged(double newCond);
    /**********************************//**
     * @brief conductivityChanged
     * @param newCond
     */
    void conductivityChanged(double newCond);
    void psiChanged(double psi);

public slots:
    /**********************************//**
     * @brief
     * addToChange
     * @param ch_rate
     */
    void addToChange(double ch_rate);
    /**********************************//**
     * @brief doTimeStep
     * @param time_t
     */
    void doTimeStep(double time_t);
    /**********************************//**
     * @brief setCapacity
     * @param capa
     */
    void setCapacity(double capa);
    /**********************************//**
     * @brief setTheta
     * @param tet
     */
    void setTheta(double tet);
    /*********************************//**
     * @brief setDeltaTheta set change rate for water content
     * @param delta new change rate
     */
    void setDeltaTheta(double delta);
    /**********************************//**
     * @brief setPsi for the matrix cell, adapts psi and emits a change
     * @param psi new psi
     */
    void setPsi(double psi);
    /**********************************//**
     * @brief emitNotEmpty
     * @param state
     */
    void emitNotEmpty(bool state){if(isEmpty == state) emit goneEmpty(isEmpty = !state);}
    /**********************************//**
     * @brief emitNotFull
     * @param state
     */
    void emitNotFull(bool state){if(isFull == state) emit goneFull(isFull = !state);}
    double getRadius(){return radius;}
    void setRadius(double theRad){radius=theRad;}
    bool matrix(void){return isMatrix;}
    /**************************************//**
     * @brief stressMe apply structural stress
     * @param stressRate amount of stress
     *
     * stressMe collapses capacity of pore class. For each water cell class the structure stability is described by two values: an upper threshold (expressed in \f$\kappa_h\f$)
     * above which a structure cannot persist, and a lower threshold \f$\kappa_l\f$, where a structure cannot be further compressed. The upper threshold is between 1 and 0,
     * the lower below \f$\kappa_h\f$. It can be negative, in this case the structure can collapse completely in that pore size class.
     *
     * For any pore density there is a resistance value. It is 0 for densities higher than \f$\kappa_h\f$, and infinite for densities below \f$\kappa_l\f$ and given by
     * \f{equation}{
     *     r_\kappa = \begin{cases}
     *        0 &\| \kappa \leq \kappa_h\\
     *        \frac{1}{\kappa - \kappa_l}-\frac{1}{\kappa_h - \kappa_l} &\| \kappa_h \le \kappa \leq \kappa_l\\
     *        \infty & \| \kappa \le \kappa_l
     *        \end{cases}
     *        \f}
     * The amount of stress that leads from \f$\kappa_1\f$ to \f$\kappa_2\f$ is then given by
     *
     * \f{eqnarray}{
     *
     *    \kappa_a &=& min(\kappa_h, \kappa_1)\\
     *    S &=& \int_{\kappa_1}^{\kappa_2} r_\kappa d\kappa\\
     *     &=& \ln(\kappa_2 - \kappa_l) - \ln(\kappa_a - \kappa_l)\\
     *     &=& \ln(\frac{\kappa_2 - \kappa_l}{\kappa_a - \kappa_l})\\
     *     \exp(S)(\kappa_a - \kappa_l) &=&\kappa_2 - \kappa_l\\
     *     \kappa_2 &=& \max(\exp(S)(\kappa_a - \kappa_l) + \kappa_l, 0)
     *     \f}
     * Note that stress is negative in this formula.

     */

    void stressMe(double stressRate);

private:
    double lCapacity, lTheta, lKappa, lPsi_d, lPsi_s, lActRate=0, lastHorCond, lastCond, lIce=0, lKappa_low, lKappa_high;
    bool isEmpty, isFull, isMatrix=false,isBoundary=false;
    QString CellName;
    double radius;
};

#endif // WATERCELL_H
