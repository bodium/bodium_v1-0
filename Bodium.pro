#-------------------------------------------------
#
# Project created by QtCreator 2016-10-10T13:42:04
#
#-------------------------------------------------

QT       += core sql

WITH_GUI:DEFINES += WITH_GUI
WITH_GUI: QT +=gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Bodium
#TEMPLATE = lib
#DEFINES += MAKE_LIB

TEMPLATE = app
CONFIG += c++11
#DEFINES += WITH_GUI

SOURCES +=  bodiumrunner.cpp\
    main.cpp\
    boundary.cpp \
    soilnode.cpp \
    plant_stand.cpp \
    soilprofile.cpp \
    upperboundary.cpp \
    wsnode.cpp \
    watercell.cpp \
    conductor.cpp \
    timex.cpp

HEADERS  += bodiumrunner.h\
    boundary.h \
    soilnode.h \
    plant_stand.h \
    timex.h \
    soilprofile.h \
    upperboundary.h \
    wsnode.h \
    watercell.h \
    conductor.h\
    timex.h

contains(DEFINES,  WITH_GUI){
QT += gui

SOURCES += parameterwindow.cpp \
variationwindow.cpp \
bodiumwindow.cpp

HEADERS += bodiumwindow.h \
variationwindow.h \
parameterwindow.h

FORMS    += bodiumwindow.ui \
    parameterwindow.ui \
    variationwindow.ui
}

contains(DEFINES,  MAKE_LIB){
CONFIG += staticlib
SOURCES += landtrans.cpp
HEADERS += landtrans.h
}

DISTFILES += \
    BL_totalweather.csv \
    Konzept_BulkDens.jpg \
    altomuenster_wetter.csv \
    config.json \
    crops/crop1_springwheat.json \
    crops/crop2_winterwheat.json \
    crops/crop3_barley.json \
    crops/crop4_sugarbeet.json \
    crops/crop5_sillagemaize.json \
    crops/crop6_potatoes.json \
    tscherno.json

    param.dox
