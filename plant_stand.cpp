#include "plant_stand.h"
#include "bodiumrunner.h"

#ifdef WITH_GUI
#include <QMessageBox>
#endif
#include "soilprofile.h"
#include <math.h>
#include <iostream>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QtMath>
#include <QDir>



Plant_stand::Plant_stand(QObject *parent) : QObject(parent)
{

}


void Plant_stand::doTimeStep(double timeStep)
{
    double dev_rate=0;
    double rts_length_temp=0;
    double water_stress=0;
    double Cex=400;                         //dummy, need this from weather data
    double water_psi=0;
    double soil_temp=0;
    double photo_tot=0;
    double transact=0;

    QDateTime actTime = static_cast<bodiumrunner*>(parent())->myTimer.currentModelTime;
    double theDay =actTime.date().dayOfYear();
    QString todayStr=actTime.date().toString("yyyy-MM-dd");
    QDate todayDate=QDate::fromString(todayStr, "yyyy-MM-dd");

    QDate testDate=QDate::fromString("2011-07-18","yyyy-MM-dd");

    //if managementfile is active, this checks after eventdates

    if(managefile && todayDate==eventdate){

        QString eventtype=eventObject["Type"].toString();

        if(eventtype=="Seed"){

            QString planttype=eventObject["Plant"].toString();

            QDir tmpCurrDir(cropsFiles);
            QString jsoncrop;

            //jsoncrop=tmpCurrDir.absolutePath();

            if(planttype=="Springbarley"){
              jsoncrop=tmpCurrDir.filePath("crop3_barley.json");
              fert_straw=true;
              harv_stage=2;
              nfix_rate=0.33;
            }
            if(planttype=="Potato"){
              jsoncrop=tmpCurrDir.filePath("crop6_potatoes.json");
              fert_straw=true;
              harv_stage=2;
              nfix_rate=0.27;
            }
            if(planttype=="Winterwheat"){
              jsoncrop=tmpCurrDir.filePath("crop2_winterwheat.json");
              fert_straw=true;
              harv_stage=2;
              nfix_rate=0.24;
            }
            if(planttype=="Sugarbeet"){
              jsoncrop=tmpCurrDir.filePath("crop4_sugarbeet.json");
              fert_straw=false;
              harv_stage=1;        //sugarbeet is already harvested at dev stage 1
              nfix_rate=0.38;
            }
            if(planttype=="Maize"){
              jsoncrop=tmpCurrDir.filePath("crop7_maize.json");
              fert_straw=true;
              harv_stage=2;
              nfix_rate=0.33;
            }
            if(planttype=="Silagemaize"){
              jsoncrop=tmpCurrDir.filePath("crop5_sillagemaize.json");
              fert_straw=true;
              harv_stage=2;
              nfix_rate=0.33;
            }


            QFile loadFile(jsoncrop);
            if (!loadFile.open(QIODevice::ReadOnly)) {
                    qWarning("Couldn't open crop parameter file.");
#ifdef WITH_GUI
                    QMessageBox msgBox;
                    msgBox.setText("Crop parameter file not found, please check path");
                    msgBox.exec();
#endif
                }

            QByteArray saveData = loadFile.readAll();
            QJsonDocument loadCropDoc(QJsonDocument::fromJson(saveData));
            config(loadCropDoc.object());

            setDevstage(-1);
            growth_season=true;
            l_seedday=theDay;
            nvd=0;
        }

        if(eventtype=="Fertilizing"){
            double fertno3=!eventObject.contains("Nitrat_N")?0:eventObject["Nitrat_N"].toDouble();
            double fertnh4=!eventObject.contains("NH4_N")?0:eventObject["NH4_N"].toDouble();

            emit ferti(fertno3*10e-5,fertnh4*10e-5,0.02);           //*10e-5 to calculate from kg/ha to kg/m²

        }

        if(eventtype=="Downunder"){
            double fertno3=!eventObject.contains("Nitrat_N")?0:eventObject["Nitrat_N"].toDouble();
            double fertnh4=!eventObject.contains("NH4_N")?0:eventObject["NH4_N"].toDouble();

            emit ferti(fertno3*10e-5,fertnh4*10e-5,0.2);           //*10e-5 to calculate from kg/ha to kg/m²

        }

        if(eventtype=="org_Fertilizing"){
            double fertorg=!eventObject.contains("FOM")?0:eventObject["FOM"].toDouble();

            emit manure(fertorg*10e-3,0.02);                            //*10e-3 to calculate from dt/ha to kg/m²umrechnung, dry weight
        }
        if(eventtype=="Tillage"){
            double depth=!eventObject.contains("depth")?0:eventObject["depth"].toDouble();

            emit till_event(depth);
        }


        mancount++;
        eventObject=(mancount>=static_cast<bodiumrunner*>(parent())->manageList.count())?eventObject:static_cast<bodiumrunner*>(parent())->manageList[mancount];
        eventdate=QDate::fromString(eventObject["Datum"].toString(), "yyyy-MM-dd");
        eventdate=(eventdate<=todayDate)?todayDate.addDays(1):eventdate;
    }


    if((!managefile && theDay== l_seedday) || (theDay>l_seedday && (theDay - timeStep/DAY)< l_seedday)){
        setDevstage(-1);
        growth_season=true;

        emit ferti(fertilizeN,fertilizeN,0.02);
    }

    if((growth_season) && (getDevstage()<3) && (getDevstage()>-999)){

        water_psi=static_cast<bodiumrunner*>(parent())->theProfile.getPlantPsi(seed_depth);
        soil_temp=static_cast<bodiumrunner*>(parent())->theProfile.getPlantTemp(seed_depth);
        transact=(theDay==l_seedday)?0:static_cast<bodiumrunner*>(parent())->theProfile.getTransAct();
        emit transpi(transact);
        water_stress=(l_trans_pot>0)?transact/l_trans_pot:0;
        dev_rate=calcDevstage(water_psi,theDay,soil_temp, l_temp, l_photoperiod, timeStep);

        if(getDevstage()>=harv_stage) {
            static_cast<bodiumrunner*>(parent())->theProfile.setTransAct(0);
            emit rootharv(0.45*biomass_values[4],root_cn);
            if(fert_straw){
                emit straw(0.45*(biomass_values[1]+biomass_values[2]),0.02,((biomass_values[1]+biomass_values[2])*0.45)/(nitro_values_m[1]+nitro_values_m[2]));
            }

            for(int i=0;i<5;i++){
                biomass_values[i]=0;
                nitro_values[i]=0;
                nitro_values_m[i]=0;
                r_growth[i]=0;
                if(i<3) reserve_values[i]=0;
            }
            static_cast<bodiumrunner*>(parent())->theProfile.rootdepth=0;
            static_cast<bodiumrunner*>(parent())->theProfile.rootVol=0;
            setDevstage(3);
            LAI=0;
            growth_season=false;

            if(!managefile) emit manure(fertilizeOrg,0.02);

            //load next crop parameter if not monocrop and no management file is active
            if(!monocrop && !managefile){
                QDir tmpCurrDir(cropsFiles);

                crop_year=(crop_year>crop_rot.size())?1:crop_year;

                QString jsoncrop;
                jsoncrop = crop_rot.at(crop_year-1);

                jsoncrop=tmpCurrDir.filePath(jsoncrop);

                QFile loadFile(jsoncrop);
                if (!loadFile.open(QIODevice::ReadOnly)) {
                        qWarning("Couldn't open crop parameter file.");
#ifdef WITH_GUI
                        QMessageBox msgBox;
                        msgBox.setText("Crop parameter file not found, please check path");
                        msgBox.exec();
#endif
                    }

                QByteArray saveData = loadFile.readAll();
                QJsonDocument loadCropDoc(QJsonDocument::fromJson(saveData));
                config(loadCropDoc.object());
                crop_year++;
            }
        }

        if(getDevstage()>=0){
            photo_tot=photosynthesis(l_radiation,Cex, l_temp, timeStep,water_stress);
            assimilateDist(water_stress, photo_tot);
        }
        emit rtl_fac();
        rts_length_temp=growth(l_temp,photo_tot,dev_rate, timeStep);

        rts_length=rts_length_temp;
        emit rootdemand(rts_length);

     }
     else {emit transpi(0);}

     double rt_depth=static_cast<bodiumrunner*>(parent())->theProfile.rootdepth;
     double above_bio=biomass_values[1]+biomass_values[2]+biomass_values[3];
     std::cout<<"\t"<<photo_tot<<"\t"<<above_bio<<"\t"<<biomass_values[3]<<"\t"<<nitro_values_m[0]<<"\t"<<exudates<<"\t"<<rt_depth<<"\t"<<getDevstage()<<"\t"<<nitro_values_m[4]<<"\t"<<water_stress<<"\t"<<transact<<"\t"<<static_cast<bodiumrunner*>(parent())->theProfile.leachate()<<"\t"<<static_cast<bodiumrunner*>(parent())->theProfile.getNout();

}

void getETP(double ETP)
{}
void getTemp(double tempera)
{}
void getPAR(double PAR)
{}

double Plant_stand::calcDevstage(double water_psi, double today, double soil_temp, double temp, double exposure, double timeStep){

    double dev_rate=0;
    double temp_func=0;
    double vernal_rate=0;
    double factor_vernil=0;
    double factor_exposure=0;
    exposure=exposure/24;
    double new_devstage=0;

    /**
     * <b>Germination phase:</b>\n
     * After sowing, seeds germinate if water potential in the specific sowing depth is high enough
     * \f$\psi>\psi_{pwp}\f$
     * with \f$\psi\,[Pa]\f$ the water potential in sowing depth and \f$\psi_{pwp}\,[Pa]\f$ the water potential at permanent wilting point.
     * If this does not happen until the 40th day after sowing, we count it as failure.
     */

    if(getDevstage()<-0.5){
        if(water_psi>psi_pwp){// && today-l_seedday<40) {
            new_devstage=-0.5;
            biomass_values[2]=seed_bio;
            reserve_values[1]=biomass_values[2]*0.01;
            nitro_values_m[2]=seed_n*biomass_values[2];
            static_cast<bodiumrunner*>(parent())->theProfile.addRootDepth(seed_depth);
            biomass_values[4]=seed_bio*0.01;
            nitro_values_m[4]=seed_n*biomass_values[4];
            emit setfirstroot(0.005);
            temp_func=tempFunc_Optima(soil_temp, t_min_veg, t_opt_veg, t_max_veg);

            dev_rate=offspring_rate*temp_func*(1/seed_depth)*timeStep;
        }
        else if(water_psi<=psi_pwp){// && today-l_seedday<40){
            new_devstage=-1;
        }

    }

    /**
    * <b>Emergence phase:</b>\n
    * In the next developement stage after germination, the offspring developement rate \f$\mu_{dev} \,[s^{-1}]\f$ is depending on a specific growth rate \f$\mu_{off}\,[m\,s^{-1}]\f$, soil temperature and sowing depth \f$z_{s}\,[m]\f$
    * \f[\mu_{dev}=(\mu_{off}\,f_{T}\,z_{s})\f] \n
    * if offspring has a height > than the sowing depth, emergence phase is reached
    */

    else if(getDevstage()<0){
        temp_func=tempFunc_Optima(soil_temp, t_min_veg, t_opt_veg, t_max_veg);
        dev_rate=offspring_rate*temp_func*(1/seed_depth)*timeStep;

        vernal_rate=tempFunc_Optima(temp, -2,4,15);

        nvd=nvd+vernal_rate;

        new_devstage=getDevstage()+dev_rate;

        if(new_devstage>=0){
            new_devstage=0;
            biomass_values[1]=0.005;
            nitro_values_m[1]=nitro_values_m[2];
            LAI=0.4;
        }

    }

    /**
    * <b>Vegetative phase:</b>\n
    * After emergence, the vegetative phase starts, where the developement rate \f$\mu_{dev, veg}\,[s^{-1}]\f$ is depending on maximum rate \f$\mu_{dev, veg}^{max}\,[s^{-1}]\f$, temperature, vernalisation and exposure time
    * \f[\mu_{dev, veg}=(\mu_{dev, veg}^{max}\,f_{T,veg}\,f_{v}\,f_{ex})\f]
    * with \link  tempFunc_Optima() temperature function \endlink  \f$f_{T,veg}=f_T(T_{day}, T_{min,veg}, T_{opt,veg}, T_{max,veg})\f$ \n
    * using vernalisation rate \f$\mu_{vn}\,[s^{-1}] = f_T(T_{day}, T_{min}=-2, T_{opt}=4, T_{max}=15)/d\f$ (data from ExpertN Doku, p. 213, and daily air temperature \f$T_{day}\f$), amount of vernalisation days \f$n_{vd}\f$ till day n is calculated by integrating from day of germination till day n \n
    * vernalisation factor is then \f[f_V=min\{1;max[0;(n_{vd}-n_{v,min})/(n_{v,max}-n_{v,min})]\}\f] with \f$n_{v,min}\f$ the minimum of vernalisation days and \f$n_{v,max}\f$ the maximum (both crop specific input parameters)\n
    * to account for exposure time, photoperiod factor is calculated
    * \f[f_{ex}=1-\chi\,(ex_{day}-ex_{opp}]\f]
    * with \f$\chi\,[-]\f$ a crop specific sensitivity parameter (positive for short-day crops, negative for long-term crops), \f$ex_{day}\,[d]\f$ is the daily photoperiod, and \f$ex_{opp}\,[d]\f$ the optimum photoperiod.
    */

    else if(getDevstage()<1){
        double vernil_temp=0;

        temp_func=tempFunc_Optima(temp, t_min_veg, t_opt_veg, t_max_veg);

        vernal_rate=tempFunc_Optima(temp, -2,4,15);
        nvd=nvd+vernal_rate;
        vernil_temp=(nvmin>0 && nvmax>0)?(nvd-nvmin)/(nvmax-nvmin):1;
        if(vernil_temp>0 && vernil_temp<1) factor_vernil=vernil_temp;
        else if(vernil_temp>=1) factor_vernil=1;
        else if(vernil_temp<0) factor_vernil=0;

        //simplere Variante aus GECROS
        factor_exposure=1-photo_sens*(exposure-exposure_opp);
        factor_exposure=(factor_exposure<0)?0:factor_exposure;

        dev_rate=veg_max*temp_func*factor_vernil*factor_exposure*timeStep;
        new_devstage=getDevstage()+dev_rate;
    }

    /**
     * <b>Generative phase:</b>\n
    * The developement rate \f$\mu_{dev, gen}\,[s^{-1}]\f$ of the generative phase after flowering is depending on maximum rate \f$\mu_{dev, gen}^{max}\,[s^{-1}]\f$ and temperature
    * \f[\mu_{dev, gen}=(\mu_{dev, gen}^{max}\,f_{T,gen})\f] \n
    * with \link  tempFunc_Optima() temperature function \endlink  \f$f_{T,gen}=f_T(T_{day}, T_{min,gen}, T_{opt,veg}, T_{max,gen})\f$ \n
    */

    else if(getDevstage()<2.5){

        temp_func=tempFunc_Optima(temp, t_min_gen, t_opt_gen, t_max_gen);
        dev_rate=gen_max*temp_func*timeStep;
        new_devstage=getDevstage()+dev_rate;
    }
    setDevstage(new_devstage);
    return dev_rate;
}

/**
 * Assimilate distribution is calculated by interpolating crop specific distribution keys with \link  interpolation() interpolation function \endlink depending on current developement stage \f$s_{dev}\f$. Newly derived assimilates are distributed with proportion \f$f_{ap,off}\f$ to the whole offspring, \f$f_{ap,roots}\f$ to the roots, \f$f_{ap,leaf}\f$ to the leaves, \f$f_{ap,stem}\f$ to the stem, and \f$f_{ap,stor}\f$ to the storage organs:
 * \f[f_{ap,off}=\varsigma_{\theta,ps}*fd_{off,i}(s_{dev}) \f]
 * \f[f_{ap,rts}=1-f_{ap,off} \f]
 * \f[f_{ap,lvs}=f_{ap,off}\,fd_{lvs,i}(s_{dev}) \f]
 * \f[f_{ap,stm}=f_{ap,off}\,fd_{stm,i}(s_{dev}) \f]
 * \f[f_{ap,sto}=f_{ap,off}-f_{ap,lvs}-f_{ap,stm} \f]
 * with \f$\varsigma_{\theta,ps}\f$ a water stress factor
 */

void Plant_stand::assimilateDist(double water_stress, double new_assimi){
    assim_dist[0]=water_stress*interpolList(getDevstage(),assi_off_dev.size(),assi_off_dev, assi_off);
    assim_dist[4]=(1-assim_dist[0]); //roots
    assim_dist[1]=assim_dist[0]*interpolList(getDevstage(),assi_leaf_dev.size(),assi_leaf_dev, assi_leaf); //leaves
    assim_dist[2]=assim_dist[0]*interpolList(getDevstage(),assi_stem_dev.size(),assi_stem_dev, assi_stem); //stem
    assim_dist[3]=assim_dist[0]-assim_dist[1]-assim_dist[2];
    assim_dist[3]=(assim_dist[3]<1e-15)?0:assim_dist[3];

    setAssim(assim_dist,new_assimi);
}

/**
 * The photosynthesis rate is based on the approach of SPASS to calculate the daily photosynthesis rate per leaf unit which is then used with a big-leaf approach to estimate the stock photosynthesis
 */



double Plant_stand::photosynthesis(double radiation, double Cex, double temp, double timeStep,double water_stress){
    double photo_leaf=0;             //photosynthesisrate leaf unit
    double photo_tot=0;             //total photosynthesisrate
    double photo_leafsat=0;          //photosynthesisrate leaf unit at light saturation
    double LUE=0;                     //light use efficency; crop specific -> input data?; here general value for C3 plants at temperatures around 10 C (Penning de Vries)
    double LUE_max=1.67e-5;         //kgC02 kJ-1, maximum light use efficiency, 0.0167 kgCO2 MJ-1 for C3, 0.0139 for C4 (SPASS, p.219)
    double PAR=0.5*radiation;       //absorbed photosynthetically active radiation, 50 % of solar radiation
    double factor_carbon=0;           //reduction factor for CO2
    double factor_nitro=0;            //reduction factor for nitrogen
    double factor_temp=0;             //reduction factor for temperature
    double Gamma_flex=0;              //CO2 compensation point temperature dependend accounting for photorespiration
    double Cintern=0;                 //internal CO2 concentration of leaves [vppm]
    double Cintern_ref=0;             //internal CO2 concentration of leaves [vppm] at external CO2 concentration of 340 vppm

 /**
  * The total photosynthesis \f$P_{tot}\,[kgCO_2\,m^{-2}\,s^{-1}]\f$ is calculated depending on the leaf are index \f$LAI\f$ and a plant-specific extinction coefficient \f$\alpha_{ext}\f$ to account for shadowing of underlying leaves:
  * \f[P_{tot}=P_{leaf}(1-exp(-\alpha_{ext}\,LAI))\f]
  * The daily photosynthesis rate per leaf unit \f$P_{leaf}\,[kgCO_2\,m^{-2}\,s^{-1}]\f$ depends on light use efficiency
  * \f[P_{leaf}=P_{leaf, sat}\,(1-e^{-LUE\,PAR/P_{leaf, sat}})\f]
  * with \f$P_{leaf, sat} \,[kgCO_2\,m^{-2}\,s^{-1}]\f$ the photosynthesis rate per leaf unit at light saturation, \f$LUE\, [kgCO_2\, (MJ)^{-1}]\f$ the light use efficiency, and \f$PAR\, [MJ \,m^{-2}\, s^{-1}]\f$ the absorbed photsynthetically active radiation which is usually assumed to be 50 % of solar radiation. The light use efficiency \f$LUE\f$ is temperature dependend for \f$C_3\f$ plants as photosynthesis is competing with photorespiration
  *\f[LUE=LUE_{max}\,(C_{ex}-\Gamma_f)/(C_{ex}-2\,\Gamma_f)\f]
  * with \f$LUE_{max}\, [kg_{CO_{2}}\,MJ^{-1}]\f$ the maximal light use efficiency in case of low light exposure, low temperature, and low \f$CO_{2}\f$ concentration, \f$C_{ex}\f$ the external \f$CO_2\f$ concentration in the air and \f$\Gamma_f\f$ the temperature dependend \f$CO_2\f$ compensation point
  * \f[\Gamma_f=\Gamma_{f,0}\,Q_{10}^{(T_{day}-T_{ref})/10}\f]
  * with \f$\Gamma_{f,0}\,[vppm]\f$ the \f$CO_2\f$ compensation point at reference temperature \f$T_{ref}\f$, set to 38 \f$vppm\f$ at \f$\SI{20}{\degreeCelsius}\f$, and \f$Q_{10}\f$ to 2. Note that \f$\Gamma_{f,0}\f$ is set to 0 for \f$C_4\f$ plants.<br>
  * The photosynthesis rate at light saturation \f$P_{leaf, sat}\f$ can be modelled by using the maximum photosynthesis rate \f$P_{max, 340}\, [kgCO_2\,m^{-2}\,s^{-1}]\f$ at a \f$CO_2\f$ concentration of \f$340 vppm\f$
  * \f[P_{leaf, sat}=P_{max, 340}\,f_{CO_2}\,f_{T,photo}\,min(f_{N},\varsigma_{\theta,ps})\f]
  *  with reduction function \f$f_{CO_2}\f$ for \f$CO_2\f$
  * \f[f_{CO_2}=P_{CO_2}/P_{max, 340}=min\{P_{max}/P_{max, 340}, (C_{intern}-\Gamma_0)/(C_{intern,340}-\Gamma_0)\}\f]
  * with \f$P_{CO_2}\,[kgCO_2\,m^{-2}\,s^{-1}]\f$ the maximum photosynthesis rate which is only limited by \f$CO_2\f$ concentration, \f$P_{max}\,[kgCO_2\,m^{-2}\,s^{-1}]\f$ the maximum photosynthesis rate at optimal conditions (temperature, N, \f$CO_2\f$, and light), \f$C_{intern}\,[vppm]\f$ the interal \f$CO_2\f$ concentration, and \f$\Gamma_0\,[vppm]\f$ the \f$CO_2\f$ compensation point at optimal temperature set to 50 for\f$C_3\f$ plants and 5 for \f$C_4\f$ plants.
  * The internal \f$CO_2\f$ concentration \f$C_{intern}\f$ depends on stomatic regulation features, and is more or less unchanged for plants with stomatic regulation and linear dependend on external \f$CO_2\f$ concentration \f$C_ex\f$ for plants without:
  * \f[C_{internal}=\begin{cases}
    a\,C_{ex} & \text{for plants w/o stomatic regulation} \\
    C_{intern,340}=a\,340vppm & \text{for plants with stomatic regulation} \\
   \end{cases}\f]
  * with a the ratio between internal and external \f$CO_2\f$ concentration assumed to be between 0.9 and 1.0 for plants without stomatic regulation, and 0.7 for \f$C_3\f$ plants and 0.4 for \f$C_4\f$ plants with stomatic regulation, respectively. \n
  * Below 340 \f$vppm\f$, the maximum photosynthesis rate is assumed to be proportional to the \f$CO_2\f$ concentration, which is still valid up to 700 \f$vppm\,CO_2\f$ in case of \f$C_3\f$ plants, while the photosynthesis rate of \f$C_4\f$ plants do not further increase above this level. The relation between \f$P_{max}\f$ and \f$P_{max,340}\f$ can thus be described as
  * \f[P_{max}/P_{max, 340}=\begin{cases}
    (700\,0.7-50)/(340\,0.7-50)=2.3 & \text{for $C_3$ plants} \\
    1.1 & \text{for $C_4$ plants} \\
   \end{cases}\f]
  * Reduction function \f$f_N\f$ for nitrogen follows the approach of the EU-Rotate_N model
  * \f[f_N=1-exp(N_{leaf}^{min}-(5\,\frac{N_{leaf}^{cur}-N_{leaf}^{min}}{N_{leaf}^{opt}-N_{leaf}^{min}}))\f]
  * with \f$N_{leaf}^{cur}\,[kg\,kg^{-1}]\f$ the current N amount within leaves, \f$N_{leaf}^{min}\, [kg\,kg^{-1}]\f$ the minimum N amount within leaves calculated with \f$N_{leaf}^{min}=a\,(1+b\,exp(-5.26*s_{dev}))\f$ with a and b plant-specific parameters,and \f$N_{leaf}^{opt} \,[kg\,kg^{-1}]\f$ the optimum N amount within leaves based on the plant-specific luxury consumption coefficient\f$r_{lux}\,[-]\f$ with \f$N_{leaf}^{opt}=r_{lux}\,N_{leaf}^{min}\f$.
  * Reduction function \f$f_{T,photo}\f$ for temperature is calculated by using the *\link  tempFunc_Optima() temperature function \endlink with temperature values for photosynthesis \f$f_{T,photo}=f_T(T_{day}, T_{min,photo}, T_{opt,photo}, T_{max,photo})\f$.\n
  * Parameters differ for \f$C_3\f$ and \f$C_4\f$ plants. Default is \f$C_3\f$.
  */

    Cintern_ref=a*340;
    Cintern=(stomReg)?Cintern_ref:a*Cex;

    factor_carbon=(Cintern-Gamma)/(Cintern_ref-Gamma);
    if(c4){
        factor_carbon=(factor_carbon>1.1)?1.1:factor_carbon;
        LUE_max=1.39e-5;
    }
    else{
        factor_carbon=(factor_carbon>2.3)?2.3:factor_carbon;
        LUE_max=1.67e-5;
    }

    double n_fac=(nitro_values[1]>0)?getNfactor(nitro_values[1],nitro_min[1],nitro_opt[1]):0;
    factor_nitro=(isnan(n_fac))?0:n_fac;
    factor_nitro=(factor_nitro>1)?1:factor_nitro;

    factor_nitro=(water_stress<factor_nitro)?water_stress:factor_nitro;

    factor_temp=tempFunc_Optima(temp, t_min_photo, t_opt_photo, t_max_photo);

    photo_leafsat=photo_max*factor_carbon*factor_nitro*factor_temp*timeStep;

    Gamma_flex=Gamma_start*pow(2, (temp-t_opt_photo)/10);

    LUE=LUE_max*(Cex-Gamma_flex)/(Cex+2*Gamma_flex);

    photo_leaf=photo_leafsat*(1-exp(-LUE*PAR/photo_leafsat));

    photo_tot=photo_leaf*(1-exp(-spec_extinct*LAI));

    return photo_tot;


}

double Plant_stand::coverage()
{
    return (1-exp(-spec_extinct*LAI));
}
double Plant_stand::plantAlbedo()
{
    return 0.24;
}

/**
 * Approach of SUCROS extended by nitrogen influence following SPASS
 */

double Plant_stand::growth(double temp, double photo, double dev_rate, double timeStep){
    double maint_cur[5]={0,0,0,0,0};    //maintenance respiration of [whole offspring, leaves, stem, storage organs, roots]
    double maint_metab=0;               //maintenance respiration for metabolic activity
    double temp_func=pow(2,(temp-temp_ref)/10);       //temperature function
    double res_stem=(biomass_values[2]>0)?reserve_values[1]/biomass_values[2]:0;     //proportion of reserve substances of biomass from stem

    //Eu_Rotate / Monica, nmin data from Monica

    double nopt=(getDevstage()<0)?n_a*(1+n_b):n_a*(1+n_b*exp(-5.26*getDevstage()));
    double nmin=n_lux;
    nopt=nopt*n_lux_f;


    //minimum and optimum n amount in the different organs, assumption of homogeneous distribution
    //thus nitro_min and _opt not needed as array, but maybe needed at some point in time
    for(int i=0;i<5;i++){
          nitro_min[i]=nmin*0.01;
          nitro_opt[i]=nopt*0.01;
    }


    /**
    * <b>Maintenance</b>
    * The energy demand for maintenance \f$ \upsilon_{mnt,crp} \,[kg CO_{2}\, m^{-2}\, s^{-1}]\f$ is the sum of energy demand for maintenance of the different plant organs (leaves \f$\upsilon_{mnt,lvs}\f$, stem \f$\upsilon_{mnt,stm}\f$, storage organs \f$\upsilon_{mnt,sto}\f$, and roots \f$\upsilon_{mnt,rts}\f$) and the energy needed for metabolic activity \f$\upsilon_{mnt,metab}\f$:
    * \f[\upsilon_{mnt,crp}=\upsilon_{mnt,lvs}+\upsilon_{mnt,stm}+\upsilon_{mnt,sto}+\upsilon_{mnt,rts}+\upsilon_{mnt,metab}\f]
    * The energy demands of the specific plant organs depend on their current biomass \f$B_{x} [kg]\f$ and increases with temperature
    * \f[\upsilon_{mnt,lvs}=B_{lvs}\upsilon_{mnt,lvs}^{ref}f_T(T_{day})f_{N,lvs}0.75\f]
    * \f[\upsilon_{mnt,stm}=B_{stm}(1-f_{rsv,stm})\upsilon_{mnt,stm}^{ref}f_T(T_{day})f_{N,stm}\f]
    * \f[\upsilon_{mnt,sto}=B_{sto}\upsilon_{mnt,sto}^{ref}f_T(T_{day})f_{N,sto}\f]
    * \f[\upsilon_{mnt,rts}=B_{rts}\upsilon_{mnt,rts}^{ref}f_T(T_{day})f_{N,rts}\f]
    * \f[\upsilon_{mnt,metab}=0.2P_{tot}0.5\f]
    * with \f$\upsilon_{mnt,x}^{ref} \,[kg CO_{2}\, kg^{-1}\, s^{-1}]\f$ is the maintenance respiration at reference temperature \f$T_{ref}\f$, \f$f_T(T_{day})\f$ the temperature function incorporating the current temperature \f$f_T(T_{day})=pow(2,(T_{cur}-T_{red})/10\f$), \f$f_{N, x}\f$ the *\link  getNfactor(double n_act,double n_min, double n_opt) nitrogen reduction function \endlink for the specific plant organ x, \f$f_{rsv,stm}\f$ the proportion of reserve substances in stem biomass, and \f$P_{tot}\f$ the daily stock photosynthesis.
    *
    */

    maint_cur[1]=(biomass_values[1]<=0)?0:biomass_values[1]*maint_ref[1]*temp_func*qMin(getNfactor(nitro_values[1],nitro_min[1],nitro_opt[1]),1.0)*0.75*timeStep;
    maint_cur[2]=(biomass_values[2]<=0)?0:biomass_values[2]*(1-res_stem)*maint_ref[2]*temp_func*qMin(getNfactor(nitro_values[2],nitro_min[2],nitro_opt[2]),1.0)*timeStep;
    maint_cur[3]=(biomass_values[3]<=0)?0:biomass_values[3]*maint_ref[3]*temp_func*qMin(getNfactor(nitro_values[3],nitro_min[3],nitro_opt[3]),1.0)*timeStep;
    maint_cur[4]=(biomass_values[4]<=0)?0:biomass_values[4]*maint_ref[4]*temp_func*qMin(getNfactor(nitro_values[4],nitro_min[4],nitro_opt[4]),1.0)*timeStep;

    maint_metab=0.2*photo*0.5;

    maint_cur[0]=maint_metab+maint_cur[1]+maint_cur[2]+maint_cur[3]+maint_cur[4];

    double res_growth=0;      //growth rate of reserve
    double resp[5]={0,0,0,0,0};       //respiration rate of [whole offspring, leaves, stem, storage organs, roots]
    double c_stm=0.4;           //C proportion of biomass from stem - value for wheat (penning, table 7), to be calculated?
    double transloc=0;        //translocation rate of reserves
    double resp_transloc=0;   //respiration during translocation
    double carbo=0;          //total carbohydrates available for growth
    double conv_1=0.6818182;     //conversion factor from CO2 to carbohydrates (30/44)
    double conv_2=1.111111;     //conversion factor from starch from reserve substances to carbohydrates (30/27)
    double coef_pn=0.44;    //conversion from assimilates to proteins kg*kg-1
    double coef_sc=0.825;    //conversion from assimilates to carbohydrates  kg*kg-1
    double growth_lvs=0;      //growth rate of leaf area
    double weight_lvs=0;      //specific leaf weight (dry mass)
    /**
    * @param conv_eff Conversion efficiency \f$\zeta_{x}\,[kg_{dry}\,kg_{CH_{2}O}^{-1}]\f$ of plant organ \f$x\f$ depends on its protein amount \f$\beta_{pn,x}\f$ and the efficiency coefficients \f$\zeta_{pn}\,[kg\, kg^{-1}]\f$ and \f$\zeta_{carbo}\, [kg\, kg^{-1}]\f$ to convert assimilates to proteins and carbohydrates, respectively
    * \f[\zeta_{x}=\beta_{pn,x}\zeta_{pn}+(1-\beta_{pn,x})\zeta_{carbo}\f]
    * with \f$\zeta_{pn}\f$ set to 0.44, and \f$\zeta_{carbo}\f$ to 0.825, except for grains where \f$\zeta_{pn}\f$ is set to 0.69.
    */
    double conv_eff[5]; //conversion efficiency in [whole offspring, leaves, stem, storage organs, roots], calculation see below


    /**
    * <b>Storage and translocation of reserve substances:</b>
    * The growth rate of the reserve substances \f$\mu_{rsv}\, [kg\, m^{-2}\,s^{-1}]\f$ is calculated depending on the biomass growth rate of the stem \f$\mu_{stm}\, [kg\, m^{-2}\,s^{-1}]\f$, the proportion of reserve substances on the stem biomass \f$f_{rsv,stm}\f$ and the proportion of carbon within stem biomass \f$f_{C,stm}\f$
    * \f[\mu_{rsv}=max(0,\mu_{stm}f_{rsv,stm}f_{C,stm}/0.444)\f]
    * where the division with 0.444 accounts for the ratio of molecular weights of carbon and reserve substances. The total amount of reserve substances \f$B_{rsv} \,[kg \,m^{-2}]\f$ is then the integral over this growth rate
    * \f[B_{rsv}=\int_{t_{0}}^{t} \mu_{rsv} d\tau\f]
    * After flowering, the reserve substances can be translocated depending on the developement rate \f$\mu_{dev, gen}\,[s^{-1}]\f$ during the generative phase
    * \f[\upsilon_{rsv}=\begin{cases}
        \mu_{dev, gen}B_{rsv} & \text{if $f_{ap,stm} < 0.01 $} \\
         0 & \text{if $f_{ap,stm} \geq x$}
         \end{cases}
    * \f]
    * with \f$f_{ap,stm}\f$ the relative proportion of assimilates which are distributed to the stem.
    */

    res_growth=r_growth[2]*res_stem*c_stm/0.444;
    if (res_growth<0) res_growth=0;
    setReserve(res_growth);

    if (getDevstage()>=1){
        if(assim_dist[2]<0.01) transloc=dev_rate*reserve_values[0];
        else transloc=0;
    }

    /**
    * <b>Growth of biomass: </b>
    * The growth rate \f$\mu_{x} \,[kg\, m^{-2}\, s^{-1}]\f$ for the plant organ \f$x\f$ depends on the available carbohydrates \f$C_{asm}\,[kg_{CH_{2}O} \,m^{-2}]\f$ calculated as follows
    * \f[C_{asm}=((P_{tot}-\upsilon_{mnt,crp})\varphi_{con,1} +\upsilon_{rsv}0.95\varphi_{con,2}))*0.95 \f]
    * with \f$P_{tot}\f$ the daily stock photosynthesis, \f$\upsilon_{mnt,crp}\f$ the daily maintenance rate, \f$\varphi_{con,1}\f$ the factor for converting from \f$CO_{2}\f$ to carbohydrates set to 30/44, \f$\upsilon_{rsv}\f$ the reserve translocation, and \f$\varphi_{con,2}\f$ the factor for converting from starch from reserve substances to carbohydrates set to 30/27. Note that \f$5 %\f$ of energy are assumed to be consumed due to intracellular transport, and additional \f$10 %\f$ are extcreted by roots to soil with \f$C_{exu}=C_{asm}*0.1\f$ being the exudates.
    * The different growth rates can then be calculated as
    * \f[\mu_{lvs}=C_{asm}f_{ap,lvs}\zeta_{lvs} \f]
    * \f[\mu_{stm}=C_{asm}f_{ap,stm}\zeta_{stm}-\upsilon_{rsv}(0.444/f_{C,stm}) \f]
    * \f[\mu_{sto}=C_{asm}f_{ap,sto}\zeta_{sto} \f]
    * \f[\mu_{rts}=C_{asm}f_{ap,rts}\zeta_{rts} \f]
    * with \f$f_{ap,x}\f$ the proportion of assimilates distributed to the plant organ \f$x\f$, \f$\zeta_{x}\, [kg_{dry}\,kg_{CH_{2}O}^{-1}]\f$ the conversion efficiency of plant organ \f$x\f$, \f$\upsilon_{rsv}\f$ the transolcation of reserve substances, and \f$f_{C,stm}\f$ the proportion of carbon within stem biomass. The growth rate of the total stock \f$\mu_{crp}^{day}\f$ is then
    * \f[\mu_{crp}=\mu_{lvs}+\mu_{stm}+\mu_{sto}+\mu_{rts} \f]
    */

    carbo=(photo>0)?(photo-maint_cur[0])*conv_1+transloc*0.95*conv_2:0;

        if(carbo!=0.0){
            carbo=carbo+0;
        }
    //if carbo needed for maintenance is higher then photosynthesis outcome - carbon=0

    carbo=(carbo<0)?0:carbo;

    exudates=(carbo>0)?carbo*exud_rate:0;         //assumption: 10 % of assimilates are not used but leave as exudates

    carbo-=exudates;

    double protein[5];
    //protein content of the organ calculated as 6.25 * nitrogen content of the organ, from Groot 1987  p.31
    for(int i=0; i<5; i++){
        protein[i]=nitro_values[i]*6.25;
        protein[i]=(protein[i]>=0.9)?0.9:protein[i];
        conv_eff[i]=protein[i]*coef_pn+(1-protein[i])*coef_sc;
    }


    r_growth[1]=carbo*assim_dist[1]*conv_eff[1];
    r_growth[2]=carbo*assim_dist[2]*conv_eff[2]-transloc*(0.444/c_stm);
    r_growth[3]=carbo*assim_dist[3]*conv_eff[3];
    r_growth[4]=carbo*assim_dist[4]*conv_eff[4];

    r_growth[2]=(r_growth[2]<=0)?0:r_growth[2];
     //if emergence phase not yet reached, set growth of roots and stem to dev_rate
    if(getDevstage()<0){
        r_growth[4]=dev_rate*biomass_values[4];
        r_growth[2]=dev_rate*biomass_values[2];
    }
    setReserve(-transloc);

    r_growth[0]=r_growth[1]+r_growth[2]+r_growth[3]+r_growth[4];

    /**
    *Respiration rates \f$\upsilon_{rsp,x} \,[kg\, m^{-2}\, s^{-1}]\f$ of the different plant organs \f$x\f$, during translocation \f$\upsilon_{rsp,trl} \,[kg\, m^{-2}\, s^{-1}]\f$ and the total plant stand \f$\upsilon_{rsp,tot} \,[kg\, m^{-2}\, s^{-1}]\f$ is calculated as
    *\f[\upsilon_{rsp,x}=\mu_{x}\zeta_{CO_{2},x} \f]
    *\f[\upsilon_{rsp,trl}= \upsilon_{rsv}\varphi_{con,trl}\f]
    *\f[\upsilon_{rsp,tot}=\upsilon_{rsp,lvs}+\upsilon_{rsp,stm}+\upsilon_{rsp,sto}+\upsilon_{rsp,rts}+\upsilon_{rsp,trl} \f]
    *with \f$\mu_{x} \,[kg\, m^{-2}\, s^{-1}]\f$ the biomass growth rates, \f$\zeta_{CO_{2},x}\f$ the \f$CO_{2}\f$ production functions and \f$\varphi_{con,trl}\f$ the conversion factor of the translocation assumed to be \f$0.053\f$.
    */

    resp[1]=r_growth[1]*resp_c[1];
    resp[2]=r_growth[2]*resp_c[2];
    resp[3]=r_growth[3]*resp_c[3];
    resp[4]=r_growth[4]*resp_c[4];
    resp[0]=resp[1]+resp[2]+resp[3]+resp[4];
    resp_transloc=transloc*0.053;               //conversion factor 44/27=0.053

    total_resp=resp_transloc+resp[0];

    if(dev_rate>0){
        biomass_values[1]=biomass_values[1]+r_growth[1];
        biomass_values[2]=biomass_values[2]+r_growth[2];
        biomass_values[3]=biomass_values[3]+r_growth[3];
        biomass_values[4]=biomass_values[4]+r_growth[4]-biomass_values[4]*0.005;
    }
    biomass_values[0]=biomass_values[1]+biomass_values[2]+biomass_values[3]+biomass_values[4];

    for(int i=1; i<5;i++){
        nitro_values[i]=(biomass_values[i]<=0)?0:nitro_values_m[i]/biomass_values[i];
    }
    nitro_values[0]=(biomass_values[0]<=0)?0:(nitro_values_m[1]+nitro_values_m[2]+nitro_values_m[3]+nitro_values_m[4])/biomass_values[0];

    /**
    *<b>Calculation of photosynthetically active plant area: </b>
    * The leaf area index is calculated by integrating the daily growth rate of the leaf area \f$\mu_{LA}\, [d^{-1}]\f$ over time
    * \f[LAI=\int_{t_{0}}^{t_1}\mu_{LA}(\tau)d\tau=\sum_{d=d_{em}}^{d=d_{cur}}\mu_{LA}\f]
    * with \f$d_{em}\f$ the day of emergence, and \f$d_{cur}\f$ the current day. The daily growth rate of the leaf area depends on a reduction factor in case of nitrogen deficiency \f$\varsigma_N\f$, the biomass growth rate of leaves \f$\mu_{lvs}\f$, and the specific leaf weight \f$\omega_{lvs}\, [kg\, ha^{-1}]\f$
    * \f[\mu_{LA}=\varsigma_N \,(\mu_{lvs} / \omega_{lvs}) \f]
    * with
    * \f[\omega_{lvs}=\omega_{lvs,avg}\, f_{\omega,lvs}(s_{dev}) \f]
    * and \f$\omega_{lvs,avg}\, [kg\, m^{-2}]\f$ the mean specific leaf weight and \f$f_{\omega,lvs}(s_{dev})\f$ the relative specific leaf weight as a function of the current developement stage \f$s_{dev}\f$.
    */

    weight_lvs=weight_lvs_avg*interpolList(getDevstage(),lvs_dev_stage.size(),lvs_dev_stage,rel_weight_lvs);
    growth_lvs=(biomass_values[1]<=0)?0:r_growth[1]/weight_lvs;

    LAI=((growth_lvs>0)&&(dev_rate>0))?LAI+growth_lvs:LAI;

    //nitrogen demand calculation needed for calculation of N uptake in soil profile/ documented there

    for(int i=1; i<5;i++){
        nitro_demand[i]=biomass_values[i]*(nitro_opt[i]-nitro_values[i]);
        nitro_demand[i]=(nitro_demand[i]<0)?0:nitro_demand[i];

    }

    nitro_demand[0]=nitro_demand[1]+nitro_demand[2]+nitro_demand[3]+nitro_demand[4];


    root_cn=(nitro_values_m[4]>0)?(biomass_values[4]*0.45)/nitro_values_m[4]:30;  //assumption 45 % of biomass is Carbon (penning table 11)
    //documentation in RootGrowth

    double rts_length=0;
    rts_length=(dev_rate>0)?mean_length*r_growth[4]:0;

    return rts_length; //root length growth rate - m m-2 s-1 bzw. m m-2 d-1

}

/**
 * The current amount of nitrogen \f$N_{x}\,[kg_{N}\,m^{-2}]\f$ in the vegetative plant organs leaves, stem and roots is based on their biomass \f$B_{x}\,[kg\,m^{-2}]\f$ and the specific nitrogen amount \f$\nu_{x}\,[kg_N\,kg^{-1}]\f$, the nitrogen uptake \f$N_{up,x}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$, and nitrogen loss via depletion to storage organs \f$N_{depl,x}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$ and due to decay with rate \f$\sigma_x\,[kg\,m^{-2}]\f$ and minimum specific nitrogen \f$\nu_{x}^{min}\,[kg_N\,kg^{-1}]\f$ needed for maintenance
 * \f[N_x=B_x\,\nu_x+N_{up,x}-N_{depl,x}-\sigma_x\,\nu_{x}^{min}\f]
 * with
 * \f[\nu_x=\frac{N_x}{B_{x,t+1}}\f]
 * and
 * \f[B_{x,t+1}=B_x+\mu_x-\sigma_x\f]
 * with \f$\mu_x\,[kg\,m^{-2}\,s^{-1}\f$] the growth rate of the specific plant organ.\n
 * The uptake of nitrogen \f$N_{up,x}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$ in the different plant organs depends on their nitrogen demand \f$N_{dem,x}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$, the nitrogen demand of the whole plant \f$N_{dem,crp}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$  and the total nitrogen \f$N_{up}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$ which was uptaken over the soil profile
 * \f[N_{up,x}=N_{up}\,(\frac{N_{dem,x}}{N_{dem,crp}})\f]
 * Nitrogen depletion due to translocation \f$N_{depl,x}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$ is calculated based on the total amount of nitrogen translocated to the storage organs \f$N_{trl}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$ and the proportion of nitrogen translocated from each vegetative plant organ \f$N_{trl,x}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$ from the translocated nitrogen of the whole plant \f$N_{trl,crp}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$
 * \f[N_{depl,x}=N_{trl}\,(\frac{N_{trl,x}}{N_{trl,crp}})\f]
 * with
 * \f[N_{trl}=min\{N_{dem,sto}; N_{trl,crp}\}\f]
 * the minimum of the nitrogen demand of the storage organs \f$N_{dem,sto}\,[kg_{N}\,m^{-2}\,s^{-1}]\f$ and the total amount of nitrogen which can be translocated and is summed over the nitrogen which can be translocated from leaves, stem and roots
 * \f[N_{trl,crp}=N_{trl,lvs}+N_{trl,stm}+N_{trl,roots}\f]
 * with
 * \f[N_{trl,x}=B_x\,(\nu_x-\nu_{x}^{min})+N_{up,x}\f]
 */
void Plant_stand::updateN(double total_n_dem){
    double uptake_N[3]={0,0,0};                     //leaves, stem, roots

    double translo_pot, translo_act=0;
    double nitro_translo[3]={0,0,0};
    double nitro_rich[3]={0,0,0};

    if(nitro_demand[0]>0){
        uptake_N[0]=(nitro_demand[1]<=0)?0:total_n_dem*(nitro_demand[1]/nitro_demand[0]);
        uptake_N[1]=(nitro_demand[2]<=0)?0:total_n_dem*(nitro_demand[2]/nitro_demand[0]);
        uptake_N[2]=(nitro_demand[4]<=0)?0:total_n_dem*(nitro_demand[4]/nitro_demand[0]);
    }
    //if no demand, convectivly uptaken N is distributed equally to the plant organs
    else{
        uptake_N[0]=total_n_dem*(biomass_values[1]/biomass_values[0]);
        uptake_N[1]=total_n_dem*(biomass_values[2]/biomass_values[0]);
        uptake_N[2]=total_n_dem*(biomass_values[4]/biomass_values[0]);
    }

    nitro_translo[0]=(nitro_values[1]<nitro_min[1])?uptake_N[0]:biomass_values[1]*(nitro_values[1]-nitro_min[1])+uptake_N[0];
    nitro_translo[1]=(nitro_values[2]<nitro_min[2])?uptake_N[1]:biomass_values[2]*(nitro_values[2]-nitro_min[2])+uptake_N[1];
    nitro_translo[2]=(nitro_values[4]<nitro_min[4])?uptake_N[2]:biomass_values[4]*(nitro_values[4]-nitro_min[4])+uptake_N[2];

    translo_pot=nitro_translo[0]+nitro_translo[1]+nitro_translo[2];
    translo_act=(nitro_demand[3]<translo_pot)?nitro_demand[3]:translo_pot;

    nitro_rich[0]=translo_act*(nitro_translo[0]/translo_pot);
    nitro_rich[1]=translo_act*(nitro_translo[1]/translo_pot);
    nitro_rich[2]=translo_act*(nitro_translo[2]/translo_pot);
    nitro_rich_tot+=nitro_rich[0]+nitro_rich[1]+nitro_rich[2];

    nitro_values_m[1]=biomass_values[1]*nitro_values[1]+uptake_N[0]-nitro_rich[0];
    nitro_values_m[2]=biomass_values[2]*nitro_values[2]+uptake_N[1]-nitro_rich[1];
    nitro_values_m[4]=biomass_values[4]*nitro_values[4]+uptake_N[2]-nitro_rich[2]-(biomass_values[4]*0.005)*nitro_values[4];
    nitro_values_m[3]=biomass_values[3]*nitro_values[3]+translo_act;

    nitro_values_m[0]=nitro_values_m[1]+nitro_values_m[2]+nitro_values_m[3]+nitro_values_m[4];

}

/**
 * Optima temperature function is given by \f[f_T(T, T_{min}, T_{opt}, T_{max})=\frac{2(T-T_{min})^\alpha(T_{opt}-T_{min})^\alpha-(T-T_{min})^{2\alpha}}{(T_{opt}-T_{min})^{2\alpha}}\f]
 * with \f[\alpha=log(2) / log[(T_{max}-T_{min})/(T_{opt}-T_{min})]\f]
 */

double Plant_stand::tempFunc_Optima(double T, double Tmin,double Topt,double Tmax ){
    double func_temp=0;
    double alpha=0;
    if(T>=Tmin && Topt>Tmin && Tmax>Topt){
        alpha=log(2)/log((Tmax-Tmin)/(Topt-Tmin));
        func_temp=(2*pow((T-Tmin),alpha)*pow((Topt-Tmin),alpha)-pow((T-Tmin),2*alpha))/(pow((Topt-Tmin),2*alpha));
       }
    if(T<Tmin){func_temp=0;}
    if(T>Tmax){func_temp=0;}
    return func_temp;
}


/**
 * Function derived from ExpertN documentation (p. 210, formula 775) for n values with \f$d_i=(x_i,y_i)_{1\leq i\leq n}\f$
 *  \f[y={f_d}_{i}(x)=\begin{cases}
    y_1 & \text{if $x \leq x_1 $} \\
    y_i+(x-x_i)\frac{y_{i+1}-y_i}{x_{i+1}-x_i} & \text{if $x_i<x \leq x_{i+1}$} \\
    y_n & \text{if $x_n \leq x$}
   \end{cases}
 * \f]
 */

double Plant_stand::interpolList(double x, int size, QList<double> list_x, QList<double> list_y){

    double y=99;
    if(x<=list_x.at(0)) y=list_y.at(0);
    else if(x>=list_x.at(size-1)) y=list_y.at(size-1);
    else {
        for(int i=0; i<size; ++i){
            if(x>list_x.at(i)&&x<=list_x.at(i+1)){
                y=list_y.at(i)+(x-list_x.at(i))*((list_y.at(i+1)-list_y.at(i))/(list_x.at(i+1)-list_x.at(i)));
                break;
            }
        }
    }
    return y;
}

void Plant_stand::configure(const QJsonObject &theConfig)
{
    crop_year=1;
    QJsonObject manInit = theConfig["ManualManagement"].toObject();

    monocrop=manInit["crop_mono"].toBool();

    QString jsoncrop;
    cropsFiles=!(theConfig.contains("CropFiles"))?cropsFiles:theConfig["CropFiles"].toString();

    if(monocrop){
        jsoncrop=manInit["crop"].toString();
    }
     else{
        QJsonArray jsonArray = QJsonArray(manInit["crop_rot"].toArray());
        QString croppath;

        crop_rot.clear();
        foreach (const QJsonValue & value, jsonArray) {
            crop_rot.append(value["crop"].toString());
            }
        jsoncrop=crop_rot.at(crop_year-1);
        crop_year++;
    }

    QDir tmpCurrDir(cropsFiles);
    jsoncrop=tmpCurrDir.filePath(jsoncrop);

    QFile loadFile(jsoncrop);
    if (!loadFile.open(QIODevice::ReadOnly)) {
            qWarning("Couldn't open crop parameter file.");
#ifdef WITH_GUI
            QMessageBox msgBox;
            msgBox.setText("Crop parameter file not found, please check path");
            msgBox.exec();
#endif
        }

    QByteArray saveData = loadFile.readAll();
    QJsonDocument loadCropDoc(QJsonDocument::fromJson(saveData));
    config(loadCropDoc.object());

    fertilizeN = manInit["ferti"].isUndefined()? fertilizeN:manInit["ferti"].toDouble();
    fertilizeOrg = manInit["org_fert"].isUndefined()? fertilizeOrg:manInit["org_fert"].toDouble();
    cn_var = manInit["cn_var"].isUndefined()? cn_var:manInit["cn_var"].toDouble();

    QJsonObject micInit = theConfig["MicrobeDyn"].toObject();

    exud_rate=micInit["exudation"].isUndefined()? exud_rate:micInit["exudation"].toDouble();

    managefile=theConfig["UseManFile"].toBool();
    mancount=0;

    QJsonObject timeInit = theConfig["timeX"].toObject();

    QDateTime startDateTime = QDateTime::fromString(timeInit["startTime"].toString(), "dd.MM.yyyy HH:mm:ss");
    QString startStr=startDateTime.date().toString("yyyy-MM-dd");
    QDate startDate=QDate::fromString(startStr, "yyyy-MM-dd");

    if(managefile){

        eventObject=static_cast<bodiumrunner*>(parent())->manageList[mancount];

        eventdate=QDate::fromString(eventObject["Datum"].toString(), "yyyy-MM-dd");
        while(eventdate<startDate){
           mancount+=1;
           eventObject=static_cast<bodiumrunner*>(parent())->manageList[mancount];
           eventdate=QDate::fromString(eventObject["Datum"].toString(), "yyyy-MM-dd");
        }
    }


}

void Plant_stand::config(const QJsonObject &plantInit){


     rts_length=0;
     LAI=0;

     for(int i=0; i<5; i++){
         nitro_demand[i]=0;
         biomass_values[i]=0;
         assim_values[i]=0;
         assim_dist[i]=0;
         nitro_values[i]=0;
         nitro_values_m[i]=0;
         r_growth[i]=0;
         reserve_values[i]=0;

     }


      seed_depth = plantInit["seeddepth"].isUndefined()? seed_depth:plantInit["seeddepth"].toDouble();
      psi_pwp=plantInit["psipwp"].isUndefined()? psi_pwp:plantInit["psipwp"].toDouble();
      veg_max=plantInit["vegmax"].isUndefined()? veg_max:plantInit["vegmax"].toDouble();
      gen_max=plantInit["genmax"].isUndefined()? gen_max:plantInit["genmax"].toDouble();
      t_min_veg=plantInit["tminveg"].isUndefined()? t_min_veg:plantInit["tminveg"].toDouble();
      t_max_veg=plantInit["tmaxveg"].isUndefined()? t_max_veg:plantInit["tmaxveg"].toDouble();
      t_opt_veg=plantInit["toptveg"].isUndefined()? t_opt_veg:plantInit["toptveg"].toDouble();
      t_min_gen=plantInit["tmingen"].isUndefined()? t_min_gen:plantInit["tmingen"].toDouble();
      t_max_gen=plantInit["tmaxgen"].isUndefined()? t_max_gen:plantInit["tmaxgen"].toDouble();
      t_opt_gen=plantInit["toptgen"].isUndefined()? t_opt_gen:plantInit["toptgen"].toDouble();
      t_min_photo=plantInit["tminphoto"].isUndefined()? t_min_photo:plantInit["tminphoto"].toDouble();
      t_max_photo=plantInit["tmaxphoto"].isUndefined()? t_max_photo:plantInit["tmaxphoto"].toDouble();
      t_opt_photo=plantInit["toptphoto"].isUndefined()? t_opt_photo:plantInit["toptphoto"].toDouble();
      nvmin=plantInit["nvmin"].isUndefined()? nvmin:plantInit["nvmin"].toDouble();
      nvmax=plantInit["nvmax"].isUndefined()? nvmax:plantInit["nvmax"].toDouble();
      weight_lvs_avg=plantInit["weight_lvs"].isUndefined()? weight_lvs_avg:plantInit["weight_lvs"].toDouble();
      mean_length=plantInit["length_root"].isUndefined()? mean_length:plantInit["length_root"].toDouble();
      offspring_rate=plantInit["offspring_rate"].isUndefined()? offspring_rate:plantInit["offspring_rate"].toDouble();
      daytype=plantInit["daytype"].isUndefined()? daytype:plantInit["daytype"].toDouble();
      exposure_opp=plantInit["expopp"].isUndefined()? exposure_opp:plantInit["expopp"].toDouble();
      photo_max=plantInit["photomax"].isUndefined()? photo_max:plantInit["photomax"].toDouble();
      stomReg=plantInit["stomreg"].isUndefined()? stomReg:plantInit["stomreg"].toBool();
      a=plantInit["relCO2"].isUndefined()? a:plantInit["relCO2"].toDouble();
      spec_extinct=plantInit["coef_ex"].isUndefined()? spec_extinct:plantInit["coef_ex"].toDouble();
      Gamma=plantInit["gamma"].isUndefined()? Gamma:plantInit["gamma"].toDouble();
      Gamma_start=plantInit["gamma0"].isUndefined()? Gamma_start:plantInit["gamma0"].toDouble();
      mean_root_dia=plantInit["root_dia"].isUndefined()? mean_root_dia:plantInit["root_dia"].toDouble();
      depth_gro_max=plantInit["depth_gro_max"].isUndefined()? depth_gro_max:plantInit["depth_gro_max"].toDouble();
      pen_depth=plantInit["pen_depth"].isUndefined()? pen_depth:plantInit["pen_depth"].toDouble();
      l_seedday = plantInit["seedday"].isUndefined()? l_seedday:plantInit["seedday"].toDouble();
      c4=plantInit["c4"].isUndefined()? c4:plantInit["c4"].toBool();
      photo_sens = plantInit["photo_sens"].isUndefined()? photo_sens:plantInit["photo_sens"].toDouble();
      seed_bio = plantInit["seed_bio"].isUndefined()? seed_bio:plantInit["seed_bio"].toDouble();
      seed_n = plantInit["seed_n"].isUndefined()? seed_n:plantInit["seed_n"].toDouble();
      temp_ref = plantInit["ref_temp"].isUndefined()? temp_ref:plantInit["ref_temp"].toDouble();
      n_lim =! plantInit.contains("n_lim")?n_lim:plantInit["n_lim"].toDouble();
      n_lux_f=!plantInit.contains("nitro_luxury")?1:plantInit["nitro_luxury"].toDouble();

      QJsonArray jsonArray = QJsonArray(plantInit["maint_ref"].toArray());
      int num=0;

      foreach (const QJsonValue & value, jsonArray) {
          maint_ref[num]=value["value"].isUndefined()? maint_ref[num]:value["value"].toDouble();
          num++;
          }

      assi_off.clear();
      jsonArray=QJsonArray(plantInit["assi_off"].toArray());
      foreach (const QJsonValue & value, jsonArray) {
          assi_off.append(value["value"].toDouble());
          }
      assi_off_dev.clear();
      jsonArray=QJsonArray(plantInit["assi_off_dev"].toArray());
      foreach (const QJsonValue & value, jsonArray) {
          assi_off_dev.append(value["value"].toDouble());
          }
      assi_leaf.clear();
      jsonArray=QJsonArray(plantInit["assi_leaf"].toArray());
      foreach (const QJsonValue & value, jsonArray) {
          assi_leaf.append(value["value"].toDouble());
          }
      assi_leaf_dev.clear();
      jsonArray=QJsonArray(plantInit["assi_leaf_dev"].toArray());
      foreach (const QJsonValue & value, jsonArray) {
          assi_leaf_dev.append(value["value"].toDouble());
          }
      assi_stem.clear();
      jsonArray=QJsonArray(plantInit["assi_stem"].toArray());
      foreach (const QJsonValue & value, jsonArray) {
          assi_stem.append(value["value"].toDouble());
          }
      assi_stem_dev.clear();
      jsonArray=QJsonArray(plantInit["assi_stem_dev"].toArray());
      foreach (const QJsonValue & value, jsonArray) {
          assi_stem_dev.append(value["value"].toDouble());
          }
      rel_weight_lvs.clear();
      jsonArray=QJsonArray(plantInit["rel_weight_lvs"].toArray());
      foreach (const QJsonValue & value, jsonArray) {
          rel_weight_lvs.append(value["value"].toDouble());
          }
      lvs_dev_stage.clear();
      jsonArray=QJsonArray(plantInit["lvs_dev_stage"].toArray());
      foreach (const QJsonValue & value, jsonArray) {
          lvs_dev_stage.append(value["value"].toDouble());
          }

      n_a=plantInit["nitro_a"].isUndefined()? n_a:plantInit["nitro_a"].toDouble();
      n_b=plantInit["nitro_b"].isUndefined()? n_b:plantInit["nitro_b"].toDouble();
      n_lux=plantInit["nitro_lux"].isUndefined()? n_lux:plantInit["nitro_lux"].toDouble();

      growth_season=false;
}
