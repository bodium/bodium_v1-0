#include "soilnode.h"
#include "soilprofile.h"
#include "bodiumrunner.h"
#include "timex.h"
#include "plant_stand.h"

#include <math.h>
#include <iostream>
#include <QJsonObject>
#include <QJsonArray>
#include <QString>
#include <QtMath>


timeX::timeX(QObject *parent, double timeStep) : QObject(parent)
{
    currentTimeStepSize = timeStep;
    currentModelTime = startModelTime = QDateTime::fromString("01.03.2017 00:00:00", "dd.MM.yyyy HH:mm:ss");
    endModelTime = QDateTime::fromString("01.03.2037 00:00:00", "dd.MM.yyyy HH:mm:ss");

}
timeX::timeX(const QJsonObject &theConf, QObject *parent) : QObject(parent)
{
    currentTimeStepSize = theConf["timeStep"].toDouble();
    currentModelTime = startModelTime = QDateTime::fromString(theConf["startTime"].toString(), "dd.MM.yyyy HH:mm:ss");
    endModelTime = QDateTime::fromString(theConf["endTime"].toString(), "dd.MM.yyyy HH:mm:ss");
    total_secs = startModelTime.secsTo(endModelTime);

}
void timeX::init(const QJsonObject &theConf)
{
    currentTimeStepSize = theConf["timeStep"].toDouble();
    currentModelTime = startModelTime = QDateTime::fromString(theConf["startTime"].toString(), "dd.MM.yyyy HH:mm:ss");
    endModelTime = QDateTime::fromString(theConf["endTime"].toString(), "dd.MM.yyyy HH:mm:ss");
    total_secs = startModelTime.secsTo(endModelTime);

}
void timeX::doTimeSteps(void)
{
    while(currentModelTime <= endModelTime){
        QString current=currentModelTime.toString("dd.MM.yyyy HH:mm:ss").toLatin1().data();
        std::cout<<"\n"<<currentModelTime.toString("dd.MM.yyyy HH:mm:ss").toLatin1().data();
        emit timeStepped(currentTimeStepSize);
        emit(update());
        emit(write());
        currentModelTime = currentModelTime.addSecs(currentTimeStepSize);
        double prog_secs=currentModelTime.secsTo(endModelTime);
        emit finished((int)((total_secs-prog_secs)/total_secs*100));
    }
    exit(0);
}

void timeX::step_py(void)
{
    QString current=currentModelTime.toString("dd.MM.yyyy HH:mm:ss").toLatin1().data();
    std::cout<<"\n"<<currentModelTime.toString("dd.MM.yyyy HH:mm:ss").toLatin1().data();
    emit timeStepped(currentTimeStepSize);
    emit update();
    emit write();
    currentModelTime = currentModelTime.addSecs(currentTimeStepSize);
    double prog_secs=currentModelTime.secsTo(endModelTime);
    emit finished((int)((total_secs-prog_secs)/total_secs*100));

}

